%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BertBlueEyeMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bert
    m_Weight: 0
  - m_Path: Bert/Face
    m_Weight: 0
  - m_Path: Bert/RootBone
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEndSpine
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeLookAt.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeController.L
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidDown1.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidDown2.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidDown3.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidLeft.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidRight.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidUp1.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidUp2.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/EyeSocket.L/EyeLidUp3.M.001
    m_Weight: 1
  - m_Path: Bert/RootBone/IKEyeSpineEnd.L/PoleEyeSpine.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeLookAt.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeController.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidDown1.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidDown2.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidDown3.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidLeft.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidRight.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidUp1.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidUp2.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/EyeSocket.M/EyeLidUp3.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.M/PoleEyeSpine.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeLookAt.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeController.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidDown1.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidDown2.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidDown3.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidLeft.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidRight.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidUp1.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidUp2.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/EyeSocket.R/EyeLidUp3.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKEyeSpineEnd.R/PoleEyeSpine.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKMiddleSpine
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftController.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftController.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftController.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.L/EyeShaftSpine2.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.L/EyeShaftSpine2.L/EyeShaftSpine3.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.L/EyeShaftSpine2.L/EyeShaftSpine3.L/EyeShaftSpine4.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.L/EyeShaftSpine2.L/EyeShaftSpine3.L/EyeShaftSpine4.L/EyeShaftSpine5.L
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.M/EyeShaftSpine2.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.M/EyeShaftSpine2.M/EyeShaftSpine3.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.M/EyeShaftSpine2.M/EyeShaftSpine3.M/EyeShaftSpine4.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.M/EyeShaftSpine2.M/EyeShaftSpine3.M/EyeShaftSpine4.M/EyeShaftSpine5.M
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.R/EyeShaftSpine2.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.R/EyeShaftSpine2.R/EyeShaftSpine3.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.R/EyeShaftSpine2.R/EyeShaftSpine3.R/EyeShaftSpine4.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/EyeShaftSpine1.R/EyeShaftSpine2.R/EyeShaftSpine3.R/EyeShaftSpine4.R/EyeShaftSpine5.R
    m_Weight: 0
  - m_Path: Bert/RootBone/IKStartSpine/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Spine7/Head
    m_Weight: 0
  - m_Path: Bert/RootBone/PoleEndSpine
    m_Weight: 0
  - m_Path: Bert/RootBone/PoleStartSpine
    m_Weight: 0
  - m_Path: Bert/RootBone/Shell
    m_Weight: 0
  - m_Path: BertBody
    m_Weight: 0
