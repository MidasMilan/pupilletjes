%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BradRightEyeMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body
    m_Weight: 0
  - m_Path: Body/Eye.L
    m_Weight: 0
  - m_Path: Body/Eye.R
    m_Weight: 0
  - m_Path: Body/Mask
    m_Weight: 0
  - m_Path: Body/Mask/Mouth
    m_Weight: 0
  - m_Path: Brad
    m_Weight: 0
  - m_Path: Brad/Root
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.L/Thigh.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.L/Thigh.L/LowerLeg.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.L/Thigh.L/LowerLeg.L/Foot.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.L/Thigh.L/LowerLeg.L/Foot.L/Foot.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.R/Thigh.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.R/Thigh.R/LowerLeg.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.R/Thigh.R/LowerLeg.R/Foot.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Pelvis.R/Thigh.R/LowerLeg.R/Foot.R/Foot.R_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Eye_Def.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Eye_Def.L/Eye_Def.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Eye_Def.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Eye_Def.R/Eye_Def.R_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidBottom_Def.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidBottom_Def.L/EyelidBottom_Def.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidBottom_Def.R
    m_Weight: 1
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidBottom_Def.R/EyelidBottom_Def.R_end
    m_Weight: 1
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidTop_Def.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidTop_Def.L/EyelidTop_Def.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidTop_Def.R
    m_Weight: 1
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/EyelidTop_Def.R/EyelidTop_Def.R_end
    m_Weight: 1
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Main_eye_con
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Main_eye_con/Eye_Con.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Main_eye_con/Eye_Con.L/Eye_Con.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Main_eye_con/Eye_Con.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Main_eye_con/Eye_Con.R/Eye_Con.R_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Mouth 1
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Mouth 1/Mouth_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Mouth_con
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Head/Mouth_con/Mouth_con_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Hand.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Hand.L/Hand.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Thumb.L
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Thumb.L/Thumb.L_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Hand.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Hand.R/Hand.R_end
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Thumb.R
    m_Weight: 0
  - m_Path: Brad/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Thumb.R/Thumb.R_end
    m_Weight: 0
