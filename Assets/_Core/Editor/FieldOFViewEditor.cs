﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.TerrainAPI;
using TMPro;

[CustomEditor(typeof(FieldOfView))]
public class FieldOFViewEditor : Editor
{
    void OnSceneGUI()
    {
        FieldOfView fov = (FieldOfView)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.GetViewRadius());

        Vector3 viewAngleA = fov.DirfromAngle(-fov.GetViewAngle() / 2, false);
        Vector3 viewAngleB = fov.DirfromAngle(fov.GetViewAngle() / 2, false);
        Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleA * fov.GetViewRadius());
        Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleB * fov.GetViewRadius());
    }   
}
