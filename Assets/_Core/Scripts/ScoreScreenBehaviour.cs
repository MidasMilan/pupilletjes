﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreScreenBehaviour : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreCanvasText;
    [SerializeField] private ProgressionBuildingsManager progressionBuildingsManager;
    [SerializeField] private GameObject container;

    public void SetScoreText(int score) {
        if (!scoreCanvasText)
        {
            Debug.LogError("Score canvas text not found! cannot set score.");
            return;
        }

        scoreCanvasText.text = score.ToString();
    }

    private void OnEnable()
    {
        ProgressionBuildingsManager.OnTotalAliensHelped += UpdateScore;
        EndingCutsceneBehaviour.OnEndingCutscene += OnShowScore;
    }

    private void OnDisable()
    {
        ProgressionBuildingsManager.OnTotalAliensHelped -= UpdateScore;
        EndingCutsceneBehaviour.OnEndingCutscene -= OnShowScore;

    }

    public void OnShowScore()
    {
        // Display the amount of aliens helped
        int totalAliensHelped = progressionBuildingsManager.GetTotalAliensHelped();
        SetScoreText(totalAliensHelped);
        container.SetActive(true);
    }

    private void UpdateScore()
    {
        int totalAliensHelped = progressionBuildingsManager.GetTotalAliensHelped();
        SetScoreText(totalAliensHelped);
    }
}
