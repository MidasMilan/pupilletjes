﻿using System.Collections;
using UnityEngine.AI;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class BuildableBehaviour : MonoBehaviour
{
    [SerializeField] private Transform[] buildTransforms;
    [SerializeField] private ParticleSystem buildParticleSystem;
    [SerializeField] private float totalBuildTime = 4.0f;
    [SerializeField] private GameObject finishedBuilding;
    [SerializeField] private SkinnedMeshRenderer blendShapeRenderer;
    [SerializeField] private float buildSpawnLength = 3.0f;
    [SerializeField] private float buildOvershootLength = 0.5f;
    [SerializeField] private float blendShapeAnimLength = 1.0f;
    [SerializeField] private float blendShapeOvershootLength = 1.0f;
    [SerializeField] private float spawnAnimDelayLength = 1.0f;
    [SerializeField] private AudioClip buildingAudioClip;

    private bool building;
    private float buildCounter;
    //TODO: make this more dynamic (dixi has three navmeshobstacles and not on the finishedbuilding GameObject
    private NavMeshObstacle navMeshObstacle;
    private AudioSource audioSource;

    public delegate void BuildableCompleteHandler(BuildableBehaviour buildable);
    public static event BuildableCompleteHandler OnBuildableComplete;

    private void Start()
    {
        building = false;
        buildCounter = 0.0f;
        audioSource = GetComponent<AudioSource>();
        if(blendShapeOvershootLength > blendShapeAnimLength)
        {
            Debug.LogError("Overshoot should never be higher than linear. Resetting to 20% of total");
            blendShapeOvershootLength = blendShapeAnimLength * 0.2f;
        }
        navMeshObstacle = finishedBuilding.GetComponent<NavMeshObstacle>();
        if (navMeshObstacle != null)
        {
            navMeshObstacle.enabled = false;
        }
        finishedBuilding.SetActive(false);
    }

    private void Update()
    {
        if(building)
        {
            buildCounter += Time.deltaTime;
            if(buildCounter >= totalBuildTime)
            {
                StopBuilding();
            }
        }
    }

    public Transform[] GetBuildTransforms()
    {
        return buildTransforms;
    }    

    public void StartBuilding()
    {
        finishedBuilding.transform.localScale = Vector3.zero;
        finishedBuilding.SetActive(true);
        building = true;
        buildParticleSystem.Play();
        StartCoroutine(SpawnAnimation(buildSpawnLength, buildOvershootLength, buildOvershootLength * 0.5f));

        audioSource.PlayOneShot(buildingAudioClip);
    }

    private void StopBuilding()
    {
        OnBuildableComplete?.Invoke(this);
        building = false;
        buildParticleSystem.Stop(false, ParticleSystemStopBehavior.StopEmitting);
        navMeshObstacle.enabled = true;

        if (blendShapeRenderer != null)
        {
            StartCoroutine(DoBlendShapeAnim(blendShapeAnimLength, blendShapeOvershootLength, spawnAnimDelayLength));
        }
    }

    IEnumerator SpawnAnimation(float totalDuration, float overshootDuration, float waitTillStartLength)
    {
        float time = 0;
        float delayTime = 0;
        float beforeOvershootDuration = totalDuration - overshootDuration;
        float halfOvershootDuration = overshootDuration * 0.5f;
        Vector3 overshootScale = new Vector3(1.2f, 1.2f, 1.2f);

        while (delayTime < waitTillStartLength)
        {
            delayTime += Time.deltaTime;
            yield return null;
        }

        while (time < totalDuration)
        {
            if (time < beforeOvershootDuration)
            {
                finishedBuilding.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, Mathf.Cos(time / beforeOvershootDuration * Mathf.PI * 0.5f));
            }
            else if ((time - beforeOvershootDuration) < halfOvershootDuration)
            {
                finishedBuilding.transform.localScale = Vector3.Lerp(Vector3.one, overshootScale, Mathf.Sin((time - beforeOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            else
            {
                finishedBuilding.transform.localScale = Vector3.Lerp(Vector3.one, overshootScale, Mathf.Cos((time - beforeOvershootDuration - halfOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            time += Time.deltaTime;
            yield return null;
        }

        finishedBuilding.transform.localScale = Vector3.one;
    }


    IEnumerator DoBlendShapeAnim(float totalDuration, float overshootDuration, float waitTillStartLength)
    {
        float time = 0;
        float delayTime = 0;
        float beforeOvershootDuration = totalDuration - overshootDuration;
        float halfOvershootDuration = overshootDuration * 0.5f;

        while (delayTime < waitTillStartLength)
        {
            delayTime += Time.deltaTime;
            yield return null;
        }

        while (time < totalDuration)
        {
            if(time < beforeOvershootDuration)
            {
                blendShapeRenderer.SetBlendShapeWeight(0, Mathf.Lerp(100.0f, 0.0f, Mathf.Cos(time / beforeOvershootDuration * Mathf.PI * 0.5f)));
            }
            else if ((time - beforeOvershootDuration) < halfOvershootDuration)
            {
                blendShapeRenderer.SetBlendShapeWeight(0, Mathf.Lerp(100.0f, 120.0f, Mathf.Sin((time - beforeOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f)));
            }
            else
            {
                blendShapeRenderer.SetBlendShapeWeight(0, Mathf.Lerp(100.0f, 120.0f, Mathf.Cos((time - beforeOvershootDuration - halfOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f)));
            }
            time += Time.deltaTime;
            yield return null;
        }

        blendShapeRenderer.SetBlendShapeWeight(0, 100.0f);
    }
}
