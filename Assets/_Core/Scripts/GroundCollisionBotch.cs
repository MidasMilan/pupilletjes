﻿using UnityEngine;
using UnityEngine.AI;

public class GroundCollisionBotch : MonoBehaviour
{
    [SerializeField] private NavMeshSurface navMeshPointer;

    private void Start()
    {
        navMeshPointer.enabled = false;
    }

    public void SetNavmeshActive(bool value)
    {
        navMeshPointer.enabled = value;
    }
}
