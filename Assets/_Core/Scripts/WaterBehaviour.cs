﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBehaviour : MonoBehaviour
{
    //MAKE SURE DOBBER IS TAGGED WITH:"Dobber"!
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dobber"))
        {
            DobberBehaviour dobber = other.GetComponent<DobberBehaviour>();
            if (dobber.GetDobberState() == DobberBehaviour.DobberState.BeingThrown)
            {
                dobber.LandDobber(transform.position.y);
            }
        }
    }
}
