﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingCutsceneBehaviour : MonoBehaviour
{
    public delegate void OnEndingCutsceneHandler();
    public static event OnEndingCutsceneHandler OnEndingCutscene;

    [SerializeField] private bool disableEndingCutscene = false;
    [SerializeField] private float minutesBeforeEndingCutscene = 15.0f;
    [SerializeField] private AudioSource musicAudioSource;
    [SerializeField] private AudioClip music;
    [SerializeField] private ProgressionBuildingsManager progressionBuildingsManager;

    private void Start()
    {
        if (disableEndingCutscene)
        {
            Debug.Log("Ending cutscene disabled, game will run indefinetely!");
        }
        else
        {
            StartCoroutine(startCutsceneInXMinutes(minutesBeforeEndingCutscene * 60));
        }
    }

    IEnumerator startCutsceneInXMinutes(float mins)
    {
        yield return new WaitForSeconds(mins);
        InitializeEndingCutscene();
    }

    public void InitializeEndingCutscene()
    {
        OnEndingCutscene?.Invoke();

        // Start the music
        musicAudioSource.clip = music;
        musicAudioSource.loop = true;
        musicAudioSource.Play();

        // Build all buildings
        progressionBuildingsManager.BuildAllBuildings();
    }
}
