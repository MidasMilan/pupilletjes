﻿using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof (PlayableDirector))]
public class UFOBehaviour : MonoBehaviour
{

    private PlayableDirector UFOAnimation;
    private void OnEnable()
    {
        UFOAnimation = GetComponent<PlayableDirector>();
        GameManager.StartGameEvent += OnStartCutscene;
        GameManager.StopGameEvent += OnStopCutscene;
    }

    private void OnDisable()
    {
        GameManager.StartGameEvent -= OnStartCutscene;
        GameManager.StopGameEvent -= OnStopCutscene;
    }

    private void OnStartCutscene()
    {
        UFOAnimation.Play();
    }

    private void OnStopCutscene()
    {
        UFOAnimation.Stop();
    }
}
