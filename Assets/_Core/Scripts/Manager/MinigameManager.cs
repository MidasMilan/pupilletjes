﻿using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    public enum Minigames
    {
        EyeDrop,
        AnalyseEye,
        ERGScrub,
        PlaceERGWires,
        PlaceERGStickers,
        VEPScrub,
        PlaceVEPMarker,
        PlaceVEPWires,
        None
    }

    [SerializeField] private Minigames[] assignableMinigames;

    [SerializeField] private TouchScreenBehaviour touchScreen;
    [SerializeField] private HologramRoomBehaviour hologramRoomPointer;
    [SerializeField] private IRISVoiceBehaviour IRISVoice;
    private Alien currentAlienTakingTest;

    [SerializeField] private float timeTillHelpDialogueTrigger;
    [SerializeField] private Dialogue[] EyedropHelpDialogues;
    [SerializeField] private Dialogue[] ScrubHelpDialogues;
    [SerializeField] private Dialogue[] WiresHelpDialogues;
    [SerializeField] private Dialogue[] StickersHelpDialogues;
    [SerializeField] private Dialogue[] MarkerHelpDialogues;

    [SerializeField] private Dialogue[] MinigameCompleteDialogues;
    [SerializeField] private Dialogue[] MinigameStepCompleteDialogues;

    private float helpQuoteCounter;
    private int givenMinigameCounter;

    public delegate void MinigameManageHandler(Alien helpedAlien, Minigames nextTask);
    public static event MinigameManageHandler OnMinigameCompleted;

    public void InvokeCompleteMG(Alien helpedAlien, Minigames nextTask)
    {
        OnMinigameCompleted?.Invoke(helpedAlien, nextTask);

        if (nextTask != Minigames.None)
        {
            IRISVoice.AddRandomVoiceLineToQueue(MinigameStepCompleteDialogues);
        }
    }

    private void Start()
    {
        helpQuoteCounter = 0.0f;
        givenMinigameCounter = 0;
    }

    private void Update()
    {
        if (helpQuoteCounter > 0.0f)
        {
            helpQuoteCounter -= Time.deltaTime;
            if(helpQuoteCounter <= 0.0f)
            {
                Alien benchedAlien = GetCurrentTestingAlien();
                if (benchedAlien != null)
                {
                    print("giving tip from minigame manager");
                    switch (benchedAlien.GetAssignedMinigame())
                    {
                        case Minigames.EyeDrop:
                            IRISVoice.AddRandomVoiceLineToQueue(EyedropHelpDialogues);
                            break;
                        case Minigames.ERGScrub:
                            IRISVoice.AddRandomVoiceLineToQueue(ScrubHelpDialogues);
                            break;
                        case Minigames.PlaceERGWires:
                            IRISVoice.AddRandomVoiceLineToQueue(WiresHelpDialogues);
                            break;
                        case Minigames.PlaceERGStickers:
                            IRISVoice.AddRandomVoiceLineToQueue(StickersHelpDialogues);
                            break;
                        case Minigames.PlaceVEPMarker:
                            IRISVoice.AddRandomVoiceLineToQueue(MarkerHelpDialogues);
                            break;
                        case Minigames.VEPScrub:
                            IRISVoice.AddRandomVoiceLineToQueue(ScrubHelpDialogues);
                            break;
                        case Minigames.PlaceVEPWires:
                            IRISVoice.AddRandomVoiceLineToQueue(WiresHelpDialogues);
                            break;
                        default:
                            break;
                    }
                }
                helpQuoteCounter = timeTillHelpDialogueTrigger;
            }
        }
    }

    public Minigames getRandomMinigame()
    {
        int randomGameID = Random.Range(0, assignableMinigames.Length);
        if (randomGameID >= assignableMinigames.Length) randomGameID = assignableMinigames.Length - 1;
        return assignableMinigames[randomGameID];
    }

    public Minigames getMinigame()
    {
        Minigames output = assignableMinigames[givenMinigameCounter];
        givenMinigameCounter++;
        if (givenMinigameCounter >= assignableMinigames.Length) givenMinigameCounter = 0;
        return output;
    }

    public void SetTestingAlien(Alien alien)
    {
        currentAlienTakingTest = alien;

        if (alien == null)
        {
            touchScreen.TransitionToLayout(TouchScreenBehaviour.Layout.MainMenu);
            helpQuoteCounter = 0.0f;
            return;
        }

        hologramRoomPointer.SetHologram(alien.GetAlienType());
        if (alien._IsHelped)
        {
            touchScreen.TransitionToLayout(TouchScreenBehaviour.Layout.MinigameComplete);
            helpQuoteCounter = 0.0f;
            IRISVoice.AddRandomVoiceLineToQueue(MinigameCompleteDialogues);
        }
        else
        {
            touchScreen.TransitionToLayout(alien.GetAssignedMinigame());
            helpQuoteCounter = timeTillHelpDialogueTrigger;
        }
    }

    public Alien GetCurrentTestingAlien()
    {
        return currentAlienTakingTest;
    }
}
