﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionBuildingsManager : MonoBehaviour
{
    [SerializeField] private BuildableBehaviour[] buildables;
    [SerializeField] private Transform supplyPointTrans;
    [SerializeField] private BuildMatsSpawner buildMatsSpawnerPointer;
    [SerializeField] private Transform returnToPlayerTrans;
    [SerializeField] private EndingCutsceneBehaviour endingCutscenePointer;
    private int buildingProgression;

    private List<Alien> aliensReadyForTask;
    private int totalAliensHelped;
    public int GetTotalAliensHelped() { return totalAliensHelped; }
    public int GetBuildableLength() { return buildables.Length; }
    public int GetBuildingProgression() { return buildingProgression; }
    public Transform GetSupplyPoint() { return supplyPointTrans; }
    public Transform GetReturnToPlayerTrans() { return returnToPlayerTrans; }

    public delegate void AlienArriveAtPOIHandler(Alien arrivedAlien);
    public static event AlienArriveAtPOIHandler OnAlienArrivalAtPOI;

    public delegate void TotalAliensHelpedHandler();
    public static event TotalAliensHelpedHandler OnTotalAliensHelped;

    private void OnEnable()
    {
        totalAliensHelped = 0;
        buildingProgression = 0;
        aliensReadyForTask = new List<Alien>();
        MinigameManager.OnMinigameCompleted += CheckAlienHelpProgression;
        BuildableBehaviour.OnBuildableComplete += BuildableBehaviour_OnBuildableComplete;
    }

    private void BuildableBehaviour_OnBuildableComplete(BuildableBehaviour buildable)
    {
        if (buildable == buildables[buildables.Length-1])
        {
            endingCutscenePointer.InitializeEndingCutscene();
        }
    }

    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= CheckAlienHelpProgression;
    }

    private void CheckAlienHelpProgression(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        if (nextTask != MinigameManager.Minigames.None) return;

        totalAliensHelped++;
        aliensReadyForTask.Add(helpedAlien);
        OnTotalAliensHelped.Invoke();
    }

    public void BuildAllBuildings()
    {
        for (int i = buildingProgression; i < buildables.Length; i++)
        {
            buildables[i].StartBuilding();
        }
    }

    public BuildableBehaviour GetNextBuildable()
    {
        if (buildingProgression >= buildables.Length)
        {
            Debug.LogError($"No buildings left to build in {gameObject.name} buildables array");
            return null;
        }

        BuildableBehaviour newBuildTask = buildables[buildingProgression];
        buildingProgression++;
        buildMatsSpawnerPointer.AddBuildMatNeeded();

        print($"new build task: {newBuildTask.gameObject.name}, building progression: {buildingProgression}");
        return newBuildTask;
    }

    public int GetNumberInLine()
    {
        return buildMatsSpawnerPointer.GetTotalBuildMatsNeeded();
    }
}
