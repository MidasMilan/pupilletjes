﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public delegate void TurnOnContrast();
    public static event TurnOnContrast TurnContrast;

    public delegate void ChangeColor();
    public static event ChangeColor _ChangeColor;

    public delegate void StartGameHandler();
    public static event StartGameHandler StartGameEvent;
    public static event StartGameHandler StopGameEvent;

    private bool gameStarted;

    public void ToggleContrastMode() => TurnContrast();    public void ChangeContrastCollorMode() => _ChangeColor();

    private void Start()
    {
        gameStarted = false;
    }

    public void RestartScene()
    {
        gameStarted = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartGame()
    {
        if(!gameStarted)
        {
            StartGameEvent.Invoke();
            gameStarted = true;
        }
    }

    public void StopGame()
    {
        StopGameEvent.Invoke();
    }
}
