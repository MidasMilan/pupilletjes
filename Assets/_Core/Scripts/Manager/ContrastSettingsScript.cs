﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContrastSettingsScript : MonoBehaviour
{
    [SerializeField]
    Material _Material;
    void Start()
    {
        if(_Material == null)
        {
          _Material = GetComponent<Renderer>().material;
        }
    }

    void OnEnable() => GameManager.TurnContrast += ToggleContrast;

    void OnDisable() => GameManager.TurnContrast -= ToggleContrast;

    void OnDestroy() => GameManager.TurnContrast -= ToggleContrast;

    void ToggleContrast() => _Material.SetFloat("_ContrastMode", _Material.GetFloat("_ContrastMode") == 1 ? 0 : 1);    
}
