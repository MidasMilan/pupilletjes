﻿using Valve.VR.InteractionSystem;
using UnityEngine;

[RequireComponent(typeof(CustomInteractable))]
public class CustomThrowable : Throwable
{
    [SerializeField] private ToolWheelController leftToolWheelPointer;
    [SerializeField] private ToolWheelController rightToolWheelPointer;

    protected override void OnAttachedToHand(Hand hand)
    {
        base.OnAttachedToHand(hand);

        if (hand.handType == Valve.VR.SteamVR_Input_Sources.RightHand)
        {
            if (rightToolWheelPointer == null) rightToolWheelPointer = hand.GetComponentInChildren<ToolWheelController>();
            if (rightToolWheelPointer.GetCurrentToolType() != ToolWheelController.Tools.Hand)
            {
                hand.DetachObject(gameObject, true);
                return;
            }
        }
        else if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand)
        {
            if (leftToolWheelPointer == null) leftToolWheelPointer = hand.GetComponentInChildren<ToolWheelController>();
            if (leftToolWheelPointer.GetCurrentToolType() != ToolWheelController.Tools.Hand)
            {
                hand.DetachObject(gameObject, true);
                return;
            }
        }
    }

    public void SetToolWheelControllerPointers(ToolWheelController leftTWC, ToolWheelController rightTWC)
    {
        leftToolWheelPointer = leftTWC;
        rightToolWheelPointer = rightTWC;
    }
}
