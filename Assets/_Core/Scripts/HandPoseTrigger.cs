﻿using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class HandPoseTrigger : MonoBehaviour
{
    [SerializeField] private Hand leftHand;
    [SerializeField] private SteamVR_Skeleton_Poser leftHandPoser;
    [SerializeField] private Hand rightHand;
    [SerializeField] private SteamVR_Skeleton_Poser rightHandPoser;
    
    [SerializeField] private bool debugMode = false;

    private SteamVR_Skeleton_Poser prevLeftHandPoser;
    private SteamVR_Skeleton_Poser prevRightHandPoser;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LeftHand"))
        {
            if (debugMode) print($"left hand detected");

            if (leftHand.AttachedObjects.Count <= 0)
            {
                leftHand.skeleton.BlendToPoser(leftHandPoser);
            }
            else
            {
                prevLeftHandPoser = leftHand.AttachedObjects[0].attachedObject.GetComponent<SteamVR_Skeleton_Poser>();
                if (debugMode) print($"object in hand: {leftHand.AttachedObjects[0].attachedObject.name}");
            }
        }

        if (other.CompareTag("RightHand"))
        {
            if (debugMode) print($"right hand detected");

            if (rightHand.AttachedObjects.Count <= 0)
            {
                rightHand.skeleton.BlendToPoser(rightHandPoser);
            }
            else
            {
                prevRightHandPoser = rightHand.AttachedObjects[0].attachedObject.GetComponent<SteamVR_Skeleton_Poser>();
                if (debugMode) print($"object in hand: {rightHand.AttachedObjects[0].attachedObject.name}");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LeftHand"))
        {
            if(prevLeftHandPoser != null)
            {
                leftHand.skeleton.BlendToPoser(prevLeftHandPoser);
                prevLeftHandPoser = null;
            }
            else
            {
                leftHand.skeleton.BlendToSkeleton();
            }
        }

        if (other.CompareTag("RightHand"))
        {
            if (prevRightHandPoser != null)
            {
                rightHand.skeleton.BlendToPoser(prevRightHandPoser);
                prevRightHandPoser = null;
            }
            else
            {
                rightHand.skeleton.BlendToSkeleton();
            }
        }
    }
}
