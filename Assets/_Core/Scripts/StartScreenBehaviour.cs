﻿using UnityEngine;

public class StartScreenBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject IRISPointer;
    [SerializeField] private GameObject BridgePointer;

    private void Start()
    {
        SetStartScreenActive(true);
    }

    private void OnEnable()
    {
        GameManager.StopGameEvent += OnDisableStartScreen;
    }

    private void OnDisable()
    {
        GameManager.StopGameEvent -= OnDisableStartScreen;
    }

    private void OnDisableStartScreen()
    {
        SetStartScreenActive(false);
    }

    public void SetStartScreenActive(bool value)
    {
        gameObject.SetActive(value);
        IRISPointer.SetActive(!value);
        BridgePointer.SetActive(!value);
    }
}
