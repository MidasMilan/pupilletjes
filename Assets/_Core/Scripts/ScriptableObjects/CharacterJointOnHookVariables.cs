﻿using UnityEngine;

[CreateAssetMenu(fileName = "BertCharacterJointAttributes", menuName = "ScriptableObjects/CharacterJointAttributes")]
public class CharacterJointOnHookVariables : ScriptableObject
{
    [Header("Basic settings")]
    public Rigidbody connectedBody;
    public Vector3 anchor;
    public Vector3 axis;
    public bool autoConfigureConnectedAnchor;
    public Vector3 connectedAnchor = new Vector3(0.0f, -0.05f, 0.0f);
    public Vector3 swingAxis = new Vector3(0.0f, 1.0f, 0.0f);

    [Header("Twist Limit Spring")]
    public float twistLimitSpring;
    public float twistLimitDamper;
    [Header("Low Twist Limit")]
    public float lowLimit = -177.0f;
    public float lowBounciness;
    public float lowContactDistance;
    [Header("High Twist Limit")]
    public float highLimit = 177.0f;
    public float highBounciness;
    public float highContactDistance;

    [Header("Swing Limit Spring")]
    public float swingLimitSpring;
    public float swingLimitDamper;
    [Header("Swing 1 Limit")]
    public float swing1Limit = 177.0f;
    public float swing1Bounciness;
    public float swing1ContactDistance;
    [Header("Swing 2 Limit")]
    public float swing2Limit = 177.0f;
    public float swing2Bounciness;
    public float swing2ContactDistance;

    [Header("Other settings")]
    public bool enableProjection;
    public float projectionDistance = 0.1f;
    public float projectionAngle = 180.0f;
    public float breakForce;//You can (and should) assign:"infinity" in the inspector here
    public float breakTorque;//You can (and should) assign:"infinity" in the inspector here
    public bool enableCollision;
    public bool enablePreprocessing = true;
    public float massScale = 1.0f;
    public float connectedMassScale = 1.0f;
}
