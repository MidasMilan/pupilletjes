﻿using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "ScriptableObjects/Dialogue")]
public class Dialogue : ScriptableObject
{
    public string clipName;
    public AudioClip voiceClip;
    public string animatorTrigger;
    public string faceAnimatorTrigger;
    public float volume = 1.0f;

    public bool canOnlyPlayOnce = false;
    public bool hasBeenPlayed = false;
}
