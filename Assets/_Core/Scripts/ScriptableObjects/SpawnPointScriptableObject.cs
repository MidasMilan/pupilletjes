﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpawnPoint", menuName = "ScriptableObjects/SpawnPoint")]
public class SpawnPointScriptableObject : ScriptableObject
{
    [SerializeField] [Tooltip("List of Units you want to Spawn from This SpawnPoint")]
    GameObject[] _GameObjectsToSpawn;

    public GameObject[] GetObjectsToSpawn => _GameObjectsToSpawn;
}
