﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(ParticleSystem))]
public class FishBehaviour : MonoBehaviour
{
    [SerializeField] private float swimStrokeInterval;
    [SerializeField] private float swimStrokeDistance;
    [SerializeField] private float swimStrokeDuration;
    [SerializeField] private float tryNibbleInterval;
    [SerializeField] private float tryNibbleDuration;
    [SerializeField] private float recoverNibbleDistance;
    [SerializeField] private int caughtOnHookStartPercent;
    [SerializeField] private int caughtOnHookPercentIncrement;
    [SerializeField] private float timeTillCollisionActivate;

    [SerializeField] private Transform hookAnchorPoint;
    [SerializeField] private Transform fishContainer;

    [SerializeField] private bool swimWhileIdle = true;

    [SerializeField] private AudioClip fishCaughtSound;
    [SerializeField] private AudioClip[] nibbleSounds;
    [SerializeField] private AudioClip[] pickupSounds;
    [SerializeField] private AudioClip[] releaseSounds;
    [SerializeField ]Animator _Animator;

    private float strokeIntervalCounter;
    private float strokeDurationCounter;
    private Vector3 targetSwimPos;
    private Vector3 strokeStartPos;
    private int timesNibbled;

    private Rigidbody thisRB;
    private Transform followTarget;
    private AudioSource thisAudioSource;

    private CapsuleCollider thisCollisionCollider;

    public enum FishState
    {
        Idle,
        FollowingTarget,
        TryingNibble,
        RecoverFromFailedNible,
        CaughtOnHook,
        FloppingOnLand
    }
    private FishState currentState;

    public void SetFollowTarget(Transform target) 
    { 
        followTarget = target;
        if(followTarget != null)
        {
            StartNibblingPhase();
        }
    }

    void Start()
    {
        targetSwimPos = transform.position;
        strokeStartPos = transform.position;
        currentState = FishState.Idle;
        strokeIntervalCounter = 0.0f;
        strokeDurationCounter = 0.0f;
        timesNibbled = 0;
        thisRB = GetComponent<Rigidbody>();
        thisRB.isKinematic = true;
        thisCollisionCollider = GetComponent<CapsuleCollider>();
        thisAudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        switch (currentState)
        {
            case FishState.Idle:
                if(!swimWhileIdle) { break; }
                strokeIntervalCounter -= Time.deltaTime;
                if(strokeIntervalCounter <= 0.0f)
                {
                    SetNewSwimDirection();
                    strokeIntervalCounter = swimStrokeInterval;
                }
                if(strokeDurationCounter < swimStrokeDuration)
                {
                    strokeDurationCounter += Time.deltaTime;
                    transform.position = Vector3.Lerp(strokeStartPos, targetSwimPos, Mathf.Sin((strokeDurationCounter / swimStrokeDuration) * Mathf.PI * 0.5f));
                }
                break;

            case FishState.FollowingTarget:
                strokeIntervalCounter -= Time.deltaTime;
                if (strokeIntervalCounter <= 0.0f)
                {
                    TryNibble();
                }
                break;

            case FishState.TryingNibble:
                transform.position = Vector3.Lerp(strokeStartPos, targetSwimPos, strokeDurationCounter / tryNibbleDuration);
                if (strokeDurationCounter >= tryNibbleDuration)
                {
                    Nibble();
                }
                strokeDurationCounter += Time.deltaTime;
                break;

            case FishState.RecoverFromFailedNible:
                transform.position = Vector3.Lerp(targetSwimPos, strokeStartPos, Mathf.Sin((strokeDurationCounter / swimStrokeDuration) * Mathf.PI * 0.5f));
                if(strokeDurationCounter >= swimStrokeDuration)
                {
                    currentState = FishState.FollowingTarget;
                }
                strokeIntervalCounter -= Time.deltaTime;
                strokeDurationCounter += Time.deltaTime;
                break;

            case FishState.CaughtOnHook:
                thisRB.velocity = new Vector3();
                transform.rotation = Quaternion.LookRotation(followTarget.up, followTarget.forward * -1);
                transform.position = followTarget.position + (transform.position - hookAnchorPoint.position);
                break;
            default:
                break;
        }
    }

    private void SetNewSwimDirection()
    {
        Vector3 direction = new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f)).normalized;
        transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
        targetSwimPos = transform.position + (direction * swimStrokeDistance);
        strokeStartPos = transform.position;
        strokeDurationCounter = 0.0f;
    }

    private void StartNibblingPhase()
    {
        lookAtTarget(followTarget);
        strokeIntervalCounter = tryNibbleInterval;
        strokeDurationCounter = 0.0f;
        currentState = FishState.FollowingTarget;
    }

    private void TryNibble()
    {
        strokeStartPos = transform.position;
        Vector3 toTarget = followTarget.position - hookAnchorPoint.position;
        toTarget = new Vector3(toTarget.x, 0.0f, toTarget.z);//Fish swim at bobbing center, instead of random point in bob motion
        targetSwimPos = strokeStartPos + toTarget;
        strokeDurationCounter = 0.0f;
        currentState = FishState.TryingNibble;
    }

    private void Nibble()
    {
        int nibbleTryPercentage = Random.Range(0, 100);
        if (nibbleTryPercentage < (caughtOnHookStartPercent + timesNibbled * caughtOnHookPercentIncrement))
        {
            currentState = FishState.CaughtOnHook;
            thisAudioSource.PlayOneShot(fishCaughtSound);
            var emission = GetComponent<ParticleSystem>().emission;
            emission.enabled = true;
            GetComponent<ParticleSystem>().Play();
        } 
        else
        {
            timesNibbled++;
            strokeDurationCounter = 0.0f;
            strokeIntervalCounter = tryNibbleInterval;
            //move StrokeStartPos as close as recoverNibbleDistance
            strokeStartPos = targetSwimPos + ((strokeStartPos - targetSwimPos).normalized * recoverNibbleDistance);
            currentState = FishState.RecoverFromFailedNible;
            thisAudioSource.PlayOneShot(nibbleSounds[Random.Range(0, nibbleSounds.Length)]);
        }
    }

    private void lookAtTarget(Transform target)
    {
        Vector3 toTarget = target.position - transform.position;
        toTarget = new Vector3(toTarget.x, 0.0f, toTarget.z);//Fish look at bobbing centre, instead of random point in bob motion
        transform.rotation = Quaternion.LookRotation(toTarget, Vector3.up);
    }

    public void TryStopTargeting()
    {
        if(currentState != FishState.CaughtOnHook)
        {
            currentState = FishState.Idle;
            strokeDurationCounter = 0.0f;
            strokeIntervalCounter = 0.0f;
            timesNibbled = 0;
            SetFollowTarget(null);
        }
    }

    public void LetGoOfHook(bool grabbedByPlayer = false)
    {
        if(!grabbedByPlayer)
        {
            thisCollisionCollider.enabled = false;
            StartCoroutine(activeColliderAfterInterval(timeTillCollisionActivate));
            thisRB.isKinematic = false;
        } 
        else
        {
            thisAudioSource.PlayOneShot(pickupSounds[Random.Range(0, pickupSounds.Length)]);
        }
        var emission = GetComponent<ParticleSystem>().emission;
        emission.enabled = false;
        currentState = FishState.FloppingOnLand;
        SetFollowTarget(null);
    }

    public void ResetParentToFishContainer()
    {
        transform.SetParent(fishContainer);
        thisRB.isKinematic = false;
        thisAudioSource.PlayOneShot(releaseSounds[Random.Range(0, releaseSounds.Length)]);
    }

    //prevent dobber colliding with fish
    IEnumerator activeColliderAfterInterval(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        thisCollisionCollider.enabled = true;
    }
}
