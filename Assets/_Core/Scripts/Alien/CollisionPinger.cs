﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CollisionPinger : MonoBehaviour
{
    [SerializeField] private Alien alien;

    private void Start()
    {
        if(alien == null)
        {
            Debug.LogError(gameObject.name + ", Alien not set! Will cause bugs!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        alien.TryAddCollision(other.tag);
    }

    private void OnTriggerExit(Collider other)
    {
        alien.TryRemoveCollision(other.tag);
    }
}