﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), (typeof(Collider)))]
public class EyeController : MonoBehaviour
{
    [SerializeField] private float followSpeed;
    [SerializeField] private float maxLookAtAngle;
    [SerializeField] private Transform eyeSocketBone;
    [SerializeField] private Transform eyeDirectionBone;
    [SerializeField] private Transform eyeContainerTransform;
    [SerializeField] private bool useTransUpForAngle = false;

    [SerializeField] private bool debugMode = false;

    private Transform lookTarget;
    private Vector3 currentEyeForward;

    void Start()
    {
        lookTarget = null;
        currentEyeForward = eyeDirectionBone.up;
    }

    void Update()
    {
        if (lookTarget != null)
        {
            Vector3 toHead = lookTarget.position - eyeContainerTransform.position;
            float socketToHeadAngle = Vector3.Angle(useTransUpForAngle?eyeSocketBone.up:(eyeSocketBone.forward * -1), toHead);

            if (socketToHeadAngle >= maxLookAtAngle)
            {
                ReturnToNeutralTick();
                return;
            }

            currentEyeForward = Vector3.RotateTowards(currentEyeForward, toHead, followSpeed * Time.deltaTime, 0.0f);
            Quaternion lookRotation = Quaternion.LookRotation(currentEyeForward);
            eyeContainerTransform.rotation = lookRotation;

            if (debugMode)
            {
                print($"following object {lookTarget.name}, angle: {socketToHeadAngle}, currentEyeForward: {currentEyeForward}");
                Debug.DrawRay(eyeContainerTransform.position, currentEyeForward, Color.red);
            }
        }
        else if(currentEyeForward != (useTransUpForAngle ? eyeSocketBone.up : eyeDirectionBone.up))
        {
            ReturnToNeutralTick();
        }
    }

    private void ReturnToNeutralTick()
    {
        currentEyeForward = Vector3.RotateTowards(currentEyeForward, (useTransUpForAngle ? eyeSocketBone.up : eyeDirectionBone.up), followSpeed * Time.deltaTime, 0.0f);
        Quaternion lookRotation = Quaternion.LookRotation(currentEyeForward);
        eyeContainerTransform.rotation = lookRotation;
        if (debugMode)
        {
            print($"returning to neutral: {currentEyeForward}");
            Debug.DrawRay(eyeContainerTransform.position, currentEyeForward, Color.blue);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("VRHead"))
        {
            currentEyeForward = useTransUpForAngle ? eyeSocketBone.up : eyeDirectionBone.up;
            lookTarget = other.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("VRHead"))
        {
            lookTarget = null;
        }
    }
}
