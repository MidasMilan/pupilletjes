﻿using Boo.Lang;
using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(ParticleSystem), typeof(AudioSource))]
public class Alien : MonoBehaviour
{
    public enum AlienType
    {
        Brad,
        Bert
    }

    [Header("Alien Traits")]
    [SerializeField] private float _WalkSpeed;
    [SerializeField] private float _SwimmingSpeed;
    [SerializeField] private float _WanderMaxRadius;
    [SerializeField] [Range(0.0f, 100.0f)] private float _NewBuildTaskPercentage;
    [SerializeField] private float _PickUpAnimLength;

    [Header("Assign from Scene Hierarchy")]
    [SerializeField]
    public DobberBehaviour _Dobber;

    [SerializeField]
    public GameObject _Table;

    [SerializeField]
    MinigameManager _MinigameManagerPointer;

    [SerializeField]
    ProgressionBuildingsManager _ProgressionManagerPointer;

    [Header("Assign From Prefab")]
    [SerializeField] [Tooltip("Animator for Creature Animation")]
    Animator _Animator;

    [SerializeField] [Tooltip("Assign NevMesh Agent on this Object Here")]
    NavMeshAgent _NavMeshAgent;

    [SerializeField] [Tooltip("Assign Particle System on this Object Here")]
    ParticleSystem _ParticleSystem;

    [SerializeField] [Tooltip("Assign Collider on this Object Here")]
    Collider _TriggerCollider;

    [SerializeField] [Tooltip("Assign ALL this alien's collision colliders here")]
    Collider[] _CollisionColliders;

    [SerializeField] [Tooltip("Assign Field of View Script Here")]
    FieldOfView _FOVScript;

    [SerializeField] [Tooltip("Assign Rigidbody Script Here")]
    Rigidbody _Rigidbody;

    [SerializeField] [Tooltip("Assign refference to ScriptableObject for onhook physics variables here")]
    CharacterJointOnHookVariables _CharacterJointAttributes;

    [SerializeField] [Tooltip("Assign refference to Arrow object here")]
    GameObject _ArrowPointer;

    [SerializeField] [Tooltip("Assign refference to Build material IK Transform here")]
    Transform BuildMatsIKPointer;

    [SerializeField] [Tooltip("Select this alien type")]
    AlienType _AlienType;

    //(should be) Private Variables
    private bool _IsHeld;
    public bool _IsHelped;
    private CharacterJoint _CharacterJoint;
    [SerializeField] private MinigameManager.Minigames _AssignedMinigame;
    private ERGAttachmentCreature _ERGAttachmentCreature;
    private VEPAttachmentCreature _VEPAttachmentCreature;
    [SerializeField] private Transform _SupplyPoint;
    [SerializeField] private BuildableBehaviour _BuildableTarget;
    [SerializeField] private Transform _ReturnToPlayerTrans;
    [SerializeField] private AlienSound _AlienSound;
    private BuildMaterialBehaviour targetPickup;
    private int numberInLine;
    private AudioSource _AudioSource;

    StateMachine _StateMachine;  
    
    List<string> _Collisions;  

    public Animator GetAnimator() => _Animator;
    public NavMeshAgent GetNavMeshAgent() => _NavMeshAgent;
    public ParticleSystem GetParticleSystem() => _ParticleSystem;
    public Collider GetTriggerCollider() => _TriggerCollider;
    public Rigidbody GetRigidbody() => _Rigidbody;
    public CharacterJoint GetCharacterJoint()
    {
        if (_CharacterJoint == null) AddCharacterJoint();
        return _CharacterJoint;
    }
    public MinigameManager GetMinigameManager() => _MinigameManagerPointer;
    public ProgressionBuildingsManager GetProgressionManager() => _ProgressionManagerPointer;
    public MinigameManager.Minigames GetAssignedMinigame() => _AssignedMinigame;
    public ERGAttachmentCreature GetERGBehaviour() => _ERGAttachmentCreature;
    public VEPAttachmentCreature GetVEPBehaviour() => _VEPAttachmentCreature;
    public AlienType GetAlienType() => _AlienType;
    public FieldOfView GetFieldOfView() => _FOVScript;
    public DobberBehaviour GetDobberRefference() => _Dobber;  
    public bool IsAlienHeld() => _IsHeld;
    public bool IsAlienHelped() => _IsHelped;
    public float GetWalkSpeed() => _WalkSpeed;
    public float GetWanderRadius() => _WanderMaxRadius;
    public float GetSwimSpeed() => _SwimmingSpeed;
    public Transform GetSupplyPoint() => _SupplyPoint;
    public BuildableBehaviour GetBuildableTarget() => _BuildableTarget;
    public GameObject GetArrow() => _ArrowPointer;
    public float GetNewBuildTaskPercentage() => _NewBuildTaskPercentage;
    public float GetPickUpAnimLength() => _PickUpAnimLength;
    public BuildMaterialBehaviour GetTargetPickup() => targetPickup;
    public int GetNumberInLine() => numberInLine;
    public Transform GetReturnToPlayerTrans() => _ReturnToPlayerTrans;

    public AudioSource GetAudioSource() => _AudioSource;
    public AlienSound GetAlienSound() => _AlienSound;

    public void setNonPrefabReferences(MinigameManager minigameManager, ProgressionBuildingsManager progressionManager, Transform supplyPoint, DobberBehaviour dobberBehaviour, GameObject table, bool isHelped)
    {
        _Dobber = dobberBehaviour;
        _MinigameManagerPointer = minigameManager;
        _ProgressionManagerPointer = progressionManager;
        _SupplyPoint = supplyPoint;
        _ReturnToPlayerTrans = progressionManager.GetReturnToPlayerTrans();
        _Table = table;
        _IsHelped = isHelped;
        _AssignedMinigame = _MinigameManagerPointer.getMinigame();

        //Turn on specific minigame script (So only this minigame can be completed)
        SetMinigameScripts();
    }

    [SerializeField] public bool debugMode = false;

    private void OnEnable()
    {
        BuildableBehaviour.OnBuildableComplete += BuildableBehaviour_OnBuildableComplete;
        BuildMatsSpawner.OnRemoveBuildMatNeeded += BuildMatsSpawner_OnRemoveBuildMatNeeded;
    }

    private void OnDisable()
    {
        BuildableBehaviour.OnBuildableComplete -= BuildableBehaviour_OnBuildableComplete;
        BuildMatsSpawner.OnRemoveBuildMatNeeded -= BuildMatsSpawner_OnRemoveBuildMatNeeded;
    }

    private void BuildableBehaviour_OnBuildableComplete(BuildableBehaviour buildable)
    {
        if (_BuildableTarget == null) return;
        if (_BuildableTarget == buildable) _BuildableTarget = null;
    }

    private void BuildMatsSpawner_OnRemoveBuildMatNeeded()
    {
        if (_BuildableTarget == null) return;
        if (numberInLine > 0) numberInLine--;
    }

    private void Start()
    {
        _StateMachine = new StateMachine(debugMode);
        _NavMeshAgent.speed = _WalkSpeed;
        _NavMeshAgent.avoidancePriority = UnityEngine.Random.Range(1, 99);//makes creatures push eachother out of the way instead of walk into eachother forever
        _Collisions = new List<string>();

        _AudioSource = GetComponent<AudioSource>();
       
        if(_Dobber == null) { Debug.LogError("Dobber not Assigned, Please Assign Dobber"); }

        if (_Animator == null || _NavMeshAgent == null || _ParticleSystem == null) UnityEngine.Debug.LogError("Animator, NavMesh Agent or ParticleSystem not assigned ");

        //Create all States a creature can have
        var _Swimming = new Swimming(this);
        var _Hooked = new Hooked(this);
        var _Hold = new Hold(this);
        var _Airborne = new Airborne(this); 
        var _TakingTests = new TakingTests(this);
        var _Walking = new Walking(this);
        var _Building = new Building(this);
        var _Despawn = new Despawn(this);

        void At(IState to, IState from, Func<bool> condition) => _StateMachine.AddTransition(to, from, condition);

        //State Transitions with Conditions for Transitions
        At(_Swimming, _Hooked, HasDobber());

        At(_Hooked, _Hold, IsHeldByPlayer());
        At(_TakingTests, _Hold, IsHeldByPlayer());
        At(_Airborne, _Hold, IsHeldByPlayer());
        At(_Walking, _Hold, IsHeldByPlayer());
        //At(_Building, _Hold, IsHeldByPlayer()); would be nice for immersion, but this will probably break the build system

        At(_Hold, _TakingTests, IsPlacedOnTable());
        At(_Airborne, _TakingTests, IsThrownOnTable());
        At(_Hooked, _TakingTests, IsThrownOnTable());
        At(_Hold, _Airborne, LetGoByPlayer());
        At(_Airborne, _Swimming, IsDroppedInWater());
        At(_Airborne, _Walking, HasReachedGround());
        At(_Walking, _Building, StartBuilding());        
        At(_Building, _Walking, StopBuilding());        

        //Assigns Tagg to _Collision to check for Transition Condition
        Func<bool> HasDobber() => () => _Collisions.Contains("Dobber") && _Dobber.GetDobberTarget() == null; // Make sure Dobber has Dobber Tag and POI layer.
        Func<bool> IsHeldByPlayer() => () => _IsHeld == true;
        Func<bool> IsDroppedInWater() => () => _Collisions.Contains("Water");
        Func<bool> IsPlacedOnTable() => () => _Collisions.Contains("Table") && !_IsHeld && _MinigameManagerPointer.GetCurrentTestingAlien() == null;
        Func<bool> IsThrownOnTable() => () => _Collisions.Contains("Table") && _MinigameManagerPointer.GetCurrentTestingAlien() == null;
        Func<bool> LetGoByPlayer() => () => !_Collisions.Contains("Table") && _IsHeld == false;
        Func<bool> HasReachedGround() => () => _Collisions.Contains("Ground");
        Func<bool> StartBuilding() => () => _BuildableTarget != null && _IsHelped == true;
        Func<bool> StopBuilding() => () => _BuildableTarget == null;
        
        //Set Current State
        _StateMachine.SetState(_Swimming);

        //Put ignore collision for dobberOnRod collider
        Collider onRodCollider = _Dobber.GetDobberOnRodCollider();
        foreach (Collider coll in _CollisionColliders)
        {
            Physics.IgnoreCollision(coll, onRodCollider, true);
        }

        //TODO: Remove this when building app. This is only necessary when creatures aren't spawned but placed in the scene by hand
        SetMinigameScripts();

        //TODO: set toolwheelcontroller referrences here
    }

    private void SetMinigameScripts()
    {
        EyeDropCreature eyeDropCreature = GetComponent<EyeDropCreature>();
        if (GetComponent<ERGAttachmentCreature>() != null)
        {
            _ERGAttachmentCreature = GetComponent<ERGAttachmentCreature>();
            _ERGAttachmentCreature.DisableAllERGComponents();
        }
        if (GetComponent<VEPAttachmentCreature>() != null)
        {
            _VEPAttachmentCreature = GetComponent<VEPAttachmentCreature>();
            _VEPAttachmentCreature.DisableAllVEPComponents();
        }

        eyeDropCreature.enabled = false;
        _ERGAttachmentCreature.enabled = false;
        _VEPAttachmentCreature.enabled = false;

        switch (_AssignedMinigame)
        {
            case MinigameManager.Minigames.EyeDrop:
                eyeDropCreature.enabled = true;
                break;
            case MinigameManager.Minigames.ERGScrub:
                _ERGAttachmentCreature.enabled = true;
                break;
            case MinigameManager.Minigames.PlaceVEPMarker:
                _VEPAttachmentCreature.enabled = true;
                break;
            default:
                break;
        }
    }

    private void Update() => _StateMachine.Tick();

    //Midas notes: OnTriggerEnter is called in root bone collider instead of this script so it syncs better with animations
    public void TryAddCollision(string collisionName)
    {
        //if (debugMode) print($"{gameObject.name}, TRY ADD: {collisionName}, current list: {_Collisions}");
        
        if (collisionName == "Untagged" || collisionName == null) return;

        if(!_Collisions.Contains(collisionName))
        { 
            _Collisions.Add(collisionName);
        }
    }

    //Midas notes: OnTriggerExit is called in root bone collider instead of this script so it syncs better with animations
    public void TryRemoveCollision(string collisionName)
    {
        if (_Collisions.Contains(collisionName)) _Collisions.Remove(collisionName);
    }

    public void ResetCollision()
    {
        _Collisions = new List<string>();
    }

    public void SetTargetPickup(BuildMaterialBehaviour target)
    {
        print($"{gameObject.name}, setting targetpickup: {target}");
        targetPickup = target;
    }

    public bool TryPickup()
    {
        if (targetPickup != null)
        {
            targetPickup.OnCreaturePickup();
            targetPickup.transform.parent = BuildMatsIKPointer;
            targetPickup.transform.localRotation = Quaternion.identity;
            targetPickup.transform.localPosition = Vector3.zero;
            return true;
        }
        return false;
    }

    public void RemoveHeldObject()
    {
        if (targetPickup != null)
        {
            Destroy(targetPickup.gameObject, 0.05f);
        }
        targetPickup = null;
    }

    public void OnPlayerGrab()
    {
        _IsHeld = true;
    }

    public void ReleaseFromHand()
    {
        _IsHeld = false;
    }

    public void GetNewBuildTask()
    {
        numberInLine = _ProgressionManagerPointer.GetNumberInLine();
        _BuildableTarget = _ProgressionManagerPointer.GetNextBuildable();
        if (_BuildableTarget == null)
        {
            _Animator.SetBool("WantsToDance", true);
        }
    }

    public void CompleteTask(MinigameManager.Minigames nextTask)
    {
        if (_IsHelped) return;

        if (nextTask == MinigameManager.Minigames.None)
        {
            _IsHelped = true;
        }

        _AssignedMinigame = nextTask;
        _MinigameManagerPointer.InvokeCompleteMG(this, nextTask);
    }

    public void SetRBKinematic(bool value)
    {
        _Rigidbody.isKinematic = value;
    }

    public void DestroyCharacterJoint()
    {
        Destroy(_CharacterJoint);
    }

    //Midas notes: can't disable a joint, so disabling is done by deleting and adding the component...
    private void AddCharacterJoint()
    {
        if (_CharacterJoint != null)
        {
            Destroy(_CharacterJoint);
        }
        _CharacterJoint = gameObject.AddComponent<CharacterJoint>();

        _CharacterJoint.connectedBody = _CharacterJointAttributes.connectedBody;
        _CharacterJoint.anchor = _CharacterJointAttributes.anchor;
        _CharacterJoint.axis = _CharacterJointAttributes.axis;
        _CharacterJoint.autoConfigureConnectedAnchor = _CharacterJointAttributes.autoConfigureConnectedAnchor;
        _CharacterJoint.connectedAnchor = _CharacterJointAttributes.connectedAnchor;
        _CharacterJoint.swingAxis = _CharacterJointAttributes.swingAxis;

        SoftJointLimitSpring twistLimitSpring = new SoftJointLimitSpring();
        twistLimitSpring.spring = _CharacterJointAttributes.twistLimitSpring;
        twistLimitSpring.damper = _CharacterJointAttributes.twistLimitDamper;
        _CharacterJoint.twistLimitSpring = twistLimitSpring;

        SoftJointLimit lowTwistLimit = new SoftJointLimit();
        lowTwistLimit.limit = _CharacterJointAttributes.lowLimit;
        lowTwistLimit.bounciness = _CharacterJointAttributes.lowBounciness;
        lowTwistLimit.contactDistance = _CharacterJointAttributes.lowContactDistance;
        _CharacterJoint.lowTwistLimit = lowTwistLimit;

        SoftJointLimit highTwistLimit = new SoftJointLimit();
        highTwistLimit.limit = _CharacterJointAttributes.highLimit;
        highTwistLimit.bounciness = _CharacterJointAttributes.highBounciness;
        highTwistLimit.contactDistance = _CharacterJointAttributes.highContactDistance;
        _CharacterJoint.highTwistLimit = highTwistLimit;

        SoftJointLimitSpring swingLimitSpring = new SoftJointLimitSpring();
        swingLimitSpring.spring = _CharacterJointAttributes.swingLimitSpring;
        swingLimitSpring.damper = _CharacterJointAttributes.swingLimitDamper;
        _CharacterJoint.swingLimitSpring = swingLimitSpring;

        SoftJointLimit swing1Limit = new SoftJointLimit();
        swing1Limit.limit = _CharacterJointAttributes.swing1Limit;
        swing1Limit.bounciness = _CharacterJointAttributes.swing1Bounciness;
        swing1Limit.contactDistance = _CharacterJointAttributes.swing1ContactDistance;
        _CharacterJoint.swing1Limit = swing1Limit;

        SoftJointLimit swing2Limit = new SoftJointLimit();
        swing2Limit.limit = _CharacterJointAttributes.swing2Limit;
        swing2Limit.bounciness = _CharacterJointAttributes.swing2Bounciness;
        swing2Limit.contactDistance = _CharacterJointAttributes.swing2ContactDistance;
        _CharacterJoint.swing2Limit = swing2Limit;

        _CharacterJoint.enableProjection = _CharacterJointAttributes.enableProjection;
        _CharacterJoint.projectionDistance = _CharacterJointAttributes.projectionDistance;
        _CharacterJoint.projectionAngle = _CharacterJointAttributes.projectionAngle;
        _CharacterJoint.breakForce = _CharacterJointAttributes.breakForce;
        _CharacterJoint.breakTorque = _CharacterJointAttributes.breakTorque;
        _CharacterJoint.enableCollision = _CharacterJointAttributes.enableCollision;
        _CharacterJoint.enablePreprocessing = _CharacterJointAttributes.enablePreprocessing;
        _CharacterJoint.massScale = _CharacterJointAttributes.massScale;
        _CharacterJoint.connectedMassScale = _CharacterJointAttributes.connectedMassScale;
    }
}
