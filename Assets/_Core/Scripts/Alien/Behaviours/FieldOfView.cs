﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    [SerializeField]
    float viewRadius;
    [SerializeField] [Range(0, 360)]
    float viewAngle;

    [SerializeField]
    LayerMask _POIMask;
    [SerializeField]
    LayerMask _ObstacleMask;

    public float GetViewRadius() => viewRadius;
    public float GetViewAngle() => viewAngle;

    public Transform GetClosestVisibleTarget()
    {
        Collider[] _TargetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, _POIMask);

        for(int i = 0; i < _TargetsInViewRadius.Length; i++)
        {
            Transform _Target = _TargetsInViewRadius[i].transform;
            Vector3 _DirTarget = (_Target.position - transform.position).normalized;

            if(Vector3.Angle(transform.forward, _DirTarget) < viewAngle / 2)
            {
                float _DistanceToTarget = Vector3.Distance(transform.position, _Target.position);
                
                if(!Physics.Raycast(transform.position, _DirTarget, _DistanceToTarget, _ObstacleMask))
                {
                    return _Target;
                }                
            }            
        }

        return null;
    }

    public Vector3 DirfromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}
