﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Swimming : MonoBehaviour, IState
{
    enum SwimmingStates
    {
        SwimmingState,
        IdleSwimmingState
    }

    Alien _Alien;
    Animator _Animator; 

    Vector3 _FinalPosition;
    float _Offset = .5f;
    float _SpaceTimer = 8f;
    float _SwimmingSpeed;
    float _DebugCounter;
    private int _SoundProbabilityPercentage = 15;

    SwimmingStates _CurrentState;

    public Swimming(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
        _SwimmingSpeed = alien.GetSwimSpeed();
    }

    public void Tick()
    {   
        switch (_CurrentState)
        {
            case SwimmingStates.SwimmingState:
                _Animator.SetFloat("MoveSpeed", _SwimmingSpeed);

                Vector3 distanceToPOI = _Alien.transform.position - _FinalPosition;
                distanceToPOI = new Vector3(distanceToPOI.x, 0.0f, distanceToPOI.z);
                if (distanceToPOI.magnitude <= _Offset)
                {
                    _DebugCounter += 1;
                    _CurrentState = SwimmingStates.IdleSwimmingState;
                    PlaySwimmingSoundWithProbability();
                }

                //quick fix
                _SpaceTimer -= Time.deltaTime;
                if (_SpaceTimer <= 0f)
                {
                    _SpaceTimer = 10.0f;
                    _Alien.GetNavMeshAgent().SetDestination(RandomPointInBounds());
                    _CurrentState = SwimmingStates.SwimmingState;
                    PlaySwimmingSoundWithProbability();
                }

                break;

            case SwimmingStates.IdleSwimmingState:
                _SpaceTimer -= Time.deltaTime;
                _Animator.SetFloat("MoveSpeed", 0);

                Transform _PositionPOI = _Alien.GetFieldOfView().GetClosestVisibleTarget();

                if(_PositionPOI != null)
                {
                    if (_PositionPOI.tag == "Dobber")
                    {
                        _FinalPosition = _PositionPOI.position;
                        _Alien.GetNavMeshAgent().SetDestination(_FinalPosition);
                        _CurrentState = SwimmingStates.SwimmingState;
                        PlaySwimmingSoundWithProbability();
                        return;
                    }
                }

                if (_SpaceTimer <= 0f)
                {
                    _SpaceTimer = 8f;
                    _Alien.GetNavMeshAgent().SetDestination(RandomPointInBounds());
                    _CurrentState = SwimmingStates.SwimmingState;
                    PlaySwimmingSoundWithProbability();
                }

            break;

            default:
                Debug.LogFormat("==SwimmingBehaviour== State not found: {0}", _CurrentState);
            break;
        }           
    }

    //Brute Force RandomPositionCalculation due to NavMesh Bug
    public Vector3 RandomPointInBounds()
    {
        _FinalPosition = new Vector3(Random.Range(-7.45f, 4.0f), -0.65f, Random.Range(-57.6f , -67.5f));        
        return _FinalPosition;
    }    

    //Calls Set Destination when State is Entered. TODO: Check if Method call doesn't look weird when creatures are in dropped by player
    public void OnEnter()
    {
        _Alien.ResetCollision();
        _Animator.SetFloat("MoveSpeed", 0);
        _Alien.GetNavMeshAgent().speed = _SwimmingSpeed;
        _Animator.SetBool("IsWalking", false);
        _Animator.SetTrigger("LandInWater");
        _Alien.GetRigidbody().isKinematic = true;
        _CurrentState = SwimmingStates.IdleSwimmingState;
        _Alien.GetNavMeshAgent().enabled = true;
        _Alien.GetNavMeshAgent().SetDestination(RandomPointInBounds()); 
        _SpaceTimer = 8f;
    }

    public void OnExit()
    {
    }    

    private void PlaySwimmingSoundWithProbability()
    {
        if (Random.Range(0, 100) <= _SoundProbabilityPercentage)
        {
            _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.InWater);
        }
    }
}
