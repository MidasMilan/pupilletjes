﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakingTests : IState
{
    Alien _Alien;
    Animator _Animator;
    MinigameManager _MinigameManager;
    public TakingTests(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
        _MinigameManager = alien.GetMinigameManager();
    }

    public void Tick()
    {
        _Alien.transform.position = _Alien.GetAlienType() == Alien.AlienType.Brad ? _Alien._Table.transform.position : _Alien._Table.transform.position + _Alien._Table.transform.forward * 0.25f;
    }

    public void OnEnter()
    {
        _MinigameManager.SetTestingAlien(_Alien);
        _Alien.SetRBKinematic(true);
        _Alien.transform.position = _Alien.GetAlienType() == Alien.AlienType.Brad ? _Alien._Table.transform.position : _Alien._Table.transform.position + _Alien._Table.transform.forward * 0.25f;
        _Alien.transform.rotation = Quaternion.LookRotation(_Alien._Table.transform.forward);
        _Animator.SetTrigger("PlacedOnTable");
        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.OnTable);
    }

    public void OnExit()
    {
        _Alien.SetRBKinematic(false);
        _MinigameManager.SetTestingAlien(null);
        if (_Alien.GetERGBehaviour() != null)
        {
            _Alien.GetERGBehaviour().DisableAllERGComponents();
        }

        if (_Alien.GetVEPBehaviour() != null)
        {
            _Alien.GetVEPBehaviour().DisableAllVEPComponents();
        }
    }
}
