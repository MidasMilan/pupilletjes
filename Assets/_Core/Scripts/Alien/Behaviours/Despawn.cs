﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : IState
{
    Alien _Alien;
    Animator _Animator;
    Material _Material;

    public Despawn(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
    }

    public void Tick()
    {
        throw new System.NotImplementedException();
    }
    public void OnEnter()
    {
        throw new System.NotImplementedException();
    }

    public void OnExit()
    {
        throw new System.NotImplementedException();
    }  
}
