﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : IState
{
    Alien _Alien;
    Animator _Animator;

    Transform _SupplyTransform;
    Transform _POITransform;

    int currentLineNumber;
    Vector3 currentWalkingDest;

    float _Offset = .25f;
    float _WalkingSpeed;
    float animCounter;
    float _BuildMatsLineDistance = 1.25f;

    enum BuildStates
    {
        GetInLine,
        WaitForBuildMat,
        PickUpBuildMats,
        GetOutTheLine,
        GoToPOI,
        Building
    }
    BuildStates _CurrentState;

    public Building(Alien alien)
    {
        _Alien = alien;
        _Animator = _Alien.GetAnimator();
        _WalkingSpeed = _Alien.GetWalkSpeed();
        animCounter = 0.0f;
    }

    public void Tick()
    {
        if (!_Alien.GetNavMeshAgent().updateRotation)
        {
            Vector3 currentForward = Vector3.RotateTowards(_Alien.transform.forward, _SupplyTransform.right, Mathf.Deg2Rad * _Alien.GetNavMeshAgent().angularSpeed * Time.deltaTime, 0.0f);
            Quaternion lookRotation = Quaternion.LookRotation(currentForward);
            _Alien.transform.rotation = lookRotation;
        }
        
        switch (_CurrentState)
        {
            case BuildStates.GetInLine:
                if(currentLineNumber != _Alien.GetNumberInLine())
                {
                    currentLineNumber = _Alien.GetNumberInLine();
                    currentWalkingDest = _SupplyTransform.position + (_SupplyTransform.right * -1 * _BuildMatsLineDistance * _Alien.GetNumberInLine());
                    _Alien.GetNavMeshAgent().SetDestination(currentWalkingDest);
                    _Animator.SetFloat("MoveSpeed", _WalkingSpeed);
                    Debug.Log($"{_Alien.gameObject.name}, updating building walk target: {currentWalkingDest}, line number: {currentLineNumber}");
                }

                Vector3 distanceToWalkDest = _Alien.transform.position - currentWalkingDest;
                distanceToWalkDest = new Vector3(distanceToWalkDest.x, 0.0f, distanceToWalkDest.z);
                if (distanceToWalkDest.magnitude <= _Offset)
                {
                    _Animator.SetFloat("MoveSpeed", 0.0f);
                }

                Vector3 distanceToSupply = _Alien.transform.position - _SupplyTransform.position;
                distanceToSupply = new Vector3(distanceToSupply.x, 0.0f, distanceToSupply.z);
                if (distanceToSupply.magnitude <= _Offset)
                {
                    _Alien.GetNavMeshAgent().updateRotation = false;
                    SetState(BuildStates.WaitForBuildMat, 0.0f);
                }
                if (_Alien.debugMode) Debug.Log($"{_Alien.gameObject.name}, walking to computer: {distanceToSupply.magnitude}");
                break;

            case BuildStates.WaitForBuildMat:
                if (_Alien.debugMode) Debug.Log($"{_Alien.gameObject.name}, waiting for build mat: {_Alien.GetTargetPickup()}");
                if (_Alien.GetTargetPickup() != null)
                {
                    SetState(BuildStates.PickUpBuildMats, 0.0f);
                }
                break;

            case BuildStates.PickUpBuildMats:
                float pickupLength = _Alien.GetPickUpAnimLength();
                if (animCounter < pickupLength * 0.8f && animCounter + Time.deltaTime >= pickupLength * 0.8f)
                {
                    _Alien.TryPickup();
                }

                animCounter += Time.deltaTime;
                if (animCounter >= pickupLength)
                {
                    _Alien.GetNavMeshAgent().updateRotation = true;
                    animCounter = 0.0f;
                    float walkRightModifier = _SupplyTransform.position.x < _POITransform.position.x ? 1.0f : -1.0f;
                    currentWalkingDest = _SupplyTransform.position + (_SupplyTransform.up * walkRightModifier * _BuildMatsLineDistance);
                    _Alien.GetNavMeshAgent().SetDestination(currentWalkingDest);
                    SetState(BuildStates.GetOutTheLine, _WalkingSpeed);
                }
                break;

            case BuildStates.GetOutTheLine:
                Vector3 outOfLineDest = _Alien.transform.position - currentWalkingDest;
                outOfLineDest = new Vector3(outOfLineDest.x, 0.0f, outOfLineDest.z);
                if (outOfLineDest.magnitude <= _Offset)
                {
                    _Alien.GetNavMeshAgent().SetDestination(_POITransform.position);
                    SetState(BuildStates.GoToPOI, _WalkingSpeed);
                }
                break;

            case BuildStates.GoToPOI:
                Vector3 distanceToPOI = _Alien.transform.position - _POITransform.position;
                distanceToPOI = new Vector3(distanceToPOI.x, 0.0f, distanceToPOI.z);
                if (distanceToPOI.magnitude <= _Offset)
                {
                    SetState(BuildStates.Building, 0.0f);
                }
                break;

            case BuildStates.Building:
                break;

            default:
                Debug.LogFormat("==BuildingBehaviour == State not found: {0}", _CurrentState);
                break;
        }
    }

    private void SetState(BuildStates newState, float movespeed)
    {
        _CurrentState = newState;
        _Animator.SetFloat("MoveSpeed", movespeed);

        if (newState == BuildStates.PickUpBuildMats)
        {
            _Alien.GetArrow().SetActive(false);
            _Alien.GetAnimator().SetTrigger("GrabMaterials");
            _Alien.GetAnimator().SetBool("CarryingStuff", true);
        }
        else if (newState == BuildStates.Building)
        {
            _Alien.RemoveHeldObject();
            _Alien.GetAnimator().SetBool("CarryingStuff", false);
            _Alien.GetBuildableTarget().StartBuilding();
            _Alien.GetAnimator().SetTrigger("StartBuilding");
        }

        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.Building);
    }

    public void OnEnter()
    {
        _SupplyTransform = _Alien.GetSupplyPoint();
        _POITransform = _Alien.GetBuildableTarget().GetBuildTransforms()[0];
        _Alien.GetNavMeshAgent().enabled = true;
        _Alien.GetRigidbody().isKinematic = true;

        _Animator.SetTrigger("LandOnGround");
        _Animator.SetBool("IsWalking", true);

        currentWalkingDest = _SupplyTransform.position + (_SupplyTransform.right * -1 * _BuildMatsLineDistance * _Alien.GetNumberInLine());
        currentLineNumber = _Alien.GetNumberInLine();
        _Alien.GetNavMeshAgent().SetDestination(currentWalkingDest);
        Debug.Log($"{_Alien.gameObject.name}, building walking target: {currentWalkingDest}, Line number: {currentLineNumber}");
        SetState(BuildStates.GetInLine, _WalkingSpeed);
    }

    public void OnExit()
    {

    }
}
