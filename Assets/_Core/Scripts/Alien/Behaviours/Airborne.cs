﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airborne : IState
{
    Alien _Alien;
    Animator _Animator;

    public Airborne(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
    }

    public void Tick()
    {
    }

    public void OnEnter()
    {
        _Animator.SetTrigger("IsLetGo");
        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.OnHooked);
    }

    public void OnExit()
    {
    }
}
