﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hold : IState
{
    Alien _Alien;
    Animator _Animator;
    public Hold(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
    }

    public void Tick()
    {
    }

    public void OnEnter()
    {
        _Animator.SetTrigger("IsHeld");
        _Alien.GetNavMeshAgent().enabled = false;
        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.Walking);
    }

    public void OnExit()
    {
    }   
}
