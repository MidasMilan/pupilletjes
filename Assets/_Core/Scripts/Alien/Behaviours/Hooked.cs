﻿using UnityEngine;

public class Hooked : IState
{
    Alien _Alien;
    Animator _Animator;

    public Hooked(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
    }

    public void Tick()
    {
    }

    public void OnEnter()
    {
        _Animator.SetTrigger("IsHooked");
        _Alien.GetDobberRefference().tag = "Untagged";
        _Alien.GetNavMeshAgent().enabled = false;
        _Alien._Dobber.SetDobberTarget(_Alien);
        _Alien.GetCharacterJoint().connectedBody = _Alien._Dobber.GetRigidbody();
        _Alien.SetRBKinematic(false);
        _Alien.GetArrow().SetActive(false);

        //If Check for Object Tag otherwise Alien might set Position to Water Origin
        _Alien.ResetCollision();

        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.OnHooked);
    }

    public void OnExit()
    {
        _Alien.DestroyCharacterJoint();
        _Alien.GetDobberRefference().tag = "Dobber";
        _Alien._Dobber.SetDobberTarget(null);
    }    
}
