﻿using UnityEngine.AI;
using UnityEngine;

public class Walking : IState
{
    enum RoamingStates
    {
        RandomWalkingState,
        LookingAroundState,
        WalkToPOI,
        WalkToPlayer,
        StayPut
    }

    Alien _Alien;
    Animator _Animator;

    Vector3 _FinalPosition;
    Vector3 _FinalForward;

    Transform _POITransform;

    float _DistanceToClosestObject;
    float _AngleToObject;

    float _Offset = .1f;    
    float _SpaceTimer = 4f;
    float _WalkingSpeed;
    float _WanderMaxRadius;
    int _RoamingCounter;

    RoamingStates _CurrentState;

    public Walking(Alien alien)
    {
        _Alien = alien;
        _Animator = alien.GetAnimator();
        _WalkingSpeed = alien.GetWalkSpeed();
        _WanderMaxRadius = alien.GetWanderRadius();
        _RoamingCounter = 0;
    }

    public void Tick()
    {
        if (!_Alien.GetNavMeshAgent().updateRotation)
        {
            Vector3 currentForward = Vector3.RotateTowards(_Alien.transform.forward, _FinalForward, Mathf.Deg2Rad * _Alien.GetNavMeshAgent().angularSpeed * Time.deltaTime, 0.0f);
            Quaternion lookRotation = Quaternion.LookRotation(currentForward);
            _Alien.transform.rotation = lookRotation;
        }

        switch (_CurrentState)
        {
            case RoamingStates.RandomWalkingState:
                //quick fix
                _SpaceTimer -= Time.deltaTime;
                if (_SpaceTimer <= 0f)
                {
                    _SpaceTimer = 15.0f;
                    SetWalkingState(RoamingStates.LookingAroundState);
                }

                Vector3 distanceToRandomPoint = _Alien.transform.position - _FinalPosition;
                distanceToRandomPoint = new Vector3(distanceToRandomPoint.x, 0.0f, distanceToRandomPoint.z);
                if (distanceToRandomPoint.magnitude <= _Offset)
                {
                    SetWalkingState(RoamingStates.LookingAroundState);
                }
                break;

            case RoamingStates.LookingAroundState:
                _SpaceTimer -= Time.deltaTime;

                Transform _PositionPOI = _Alien.GetFieldOfView().GetClosestVisibleTarget();
                if (_PositionPOI != null)
                {
                    _Alien.GetNavMeshAgent().SetDestination(_PositionPOI.position);
                    SetWalkingState(RoamingStates.WalkToPOI);
                    return;
                }

                if (_SpaceTimer <= 0f)
                {
                    _SpaceTimer = Random.Range(2.0f, 5.0f);
                    SetWalkingState(RoamingStates.RandomWalkingState);
                }
            break;

            case RoamingStates.WalkToPOI:
                Vector3 distanceToPOI = _Alien.transform.position - _FinalPosition;
                distanceToPOI = new Vector3(distanceToPOI.x, 0.0f, distanceToPOI.z);
                if (distanceToPOI.magnitude <= _Offset)
                {
                    SetWalkingState(RoamingStates.LookingAroundState);
                }
                break;


            case RoamingStates.WalkToPlayer:
                Vector3 distanceToPlayer = _Alien.transform.position - _FinalPosition;
                distanceToPlayer = new Vector3(distanceToPlayer.x, 0.0f, distanceToPlayer.z);
                if (distanceToPlayer.magnitude <= _Offset)
                {
                    SetWalkingState(RoamingStates.StayPut);
                }
                break;

            case RoamingStates.StayPut:
                break;

            default:
            break;
        }        
    }

    private void SetWalkingState(RoamingStates newState)
    {
        switch (newState)
        {
            case RoamingStates.RandomWalkingState:
                _Alien.GetNavMeshAgent().updateRotation = true;
                CheckForNewBuildable();
                _Animator.SetFloat("MoveSpeed", _WalkingSpeed);
                if (false)//_RoamingCounter >= Random.Range(0, 0))
                {
                    _RoamingCounter++;
                    _Alien.GetNavMeshAgent().SetDestination(RandomPointAroundCurrentLoc());
                }
                else
                {
                    _RoamingCounter = 0;
                    _Alien.GetNavMeshAgent().SetDestination(RandomNavmeshLocation());
                }
                break;

            case RoamingStates.LookingAroundState:
                _Animator.SetFloat("MoveSpeed", 0.0f);
                break;

            case RoamingStates.WalkToPOI:
                _Animator.SetFloat("MoveSpeed", _WalkingSpeed);
                break;

            case RoamingStates.WalkToPlayer:
                _Animator.SetFloat("MoveSpeed", _WalkingSpeed);
                _Alien.GetNavMeshAgent().updateRotation = true;
                _FinalPosition = _Alien.GetReturnToPlayerTrans().position;
                _Alien.GetNavMeshAgent().SetDestination(_FinalPosition);
                break;

            case RoamingStates.StayPut:
                _Animator.SetFloat("MoveSpeed", 0.0f);
                _FinalForward = _Alien.GetReturnToPlayerTrans().forward;
                _Alien.GetNavMeshAgent().updateRotation = false;
                break;

            default:
                Debug.LogFormat("==WalkingBehaviour== State not found: {0}", _CurrentState);
                break;
        }
        _CurrentState = newState;

        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.Walking);
    }

    private void CheckForNewBuildable()
    {
        if (Random.Range(0.0f, 100.0f) <= _Alien.GetNewBuildTaskPercentage())
        {
            _Alien.GetNewBuildTask();
        }
    }

    private Vector3 RandomPointAroundCurrentLoc()
    {
        Vector3 wanderDirection = new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f));
        wanderDirection = wanderDirection.normalized * Random.Range(1.0f, _WanderMaxRadius);
        wanderDirection = _Alien.transform.position - wanderDirection;
        _FinalPosition = wanderDirection;
        return wanderDirection;
    }

    private Vector3 RandomNavmeshLocation()
    {
        NavMeshTriangulation navMeshData = NavMesh.CalculateTriangulation();

        int t = Random.Range(0, navMeshData.indices.Length - 3);

        _FinalPosition = Vector3.Lerp(navMeshData.vertices[navMeshData.indices[t]], navMeshData.vertices[navMeshData.indices[t + 1]], Random.value);
        _FinalPosition = Vector3.Lerp(_FinalPosition, navMeshData.vertices[navMeshData.indices[t + 2]], Random.value);
        return _FinalPosition;
    }

    public void OnEnter()
    {
        _Alien.GetNavMeshAgent().enabled = true;
        _Alien.GetNavMeshAgent().speed = _WalkingSpeed;
        _Alien.GetRigidbody().isKinematic = true;

        _Animator.SetTrigger("LandOnGround");        
        _Animator.SetBool("IsWalking", true);
        SetWalkingState(_Alien._IsHelped?RoamingStates.RandomWalkingState:RoamingStates.WalkToPlayer);
        _Alien.GetAlienSound().Play(_Alien.GetAudioSource(), AlienSound.SoundType.Walking);
    }

    public void OnExit()
    {
        _Alien.GetRigidbody().isKinematic = false;
    }  
}
