﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PickUpCollPinger : MonoBehaviour
{
    [SerializeField] private Alien alien;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BuildMat"))
        {
            BuildMaterialBehaviour targetPickup = other.GetComponent<BuildMaterialBehaviour>();
            alien.SetTargetPickup(targetPickup);
            return;
        }
    }
}
