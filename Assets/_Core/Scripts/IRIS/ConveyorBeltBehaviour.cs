﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBeltBehaviour : MonoBehaviour
{
    [SerializeField] private Transform endpoint;
    [SerializeField] private float speed;
    [SerializeField] private float defaultConveyorDuration;
    [SerializeField] private AudioClip beltSound;
    [SerializeField] private AudioSource audioSource;
    private bool currentlyOn;

    public void EnableConveyor(bool value, float conveyorDuration = 0.0f)
    {
        currentlyOn = value;
        if (value) StartCoroutine(ShutDownConveyor(conveyorDuration == 0.0f ? defaultConveyorDuration : conveyorDuration));
    }

    private void Start()
    {
        currentlyOn = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!currentlyOn || other.CompareTag("IgnoreBelt")) return;
        other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, speed * Time.deltaTime);
        if (audioSource != null) { audioSource.PlayOneShot(beltSound); }
    }

    IEnumerator ShutDownConveyor(float totalTimeTillShutdown)
    {
        float time = 0.0f;

        while (time < totalTimeTillShutdown)
        {
            time += Time.deltaTime;
            yield return null;
        }

        currentlyOn = false;
    }
}
