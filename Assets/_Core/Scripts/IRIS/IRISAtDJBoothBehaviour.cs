﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]

public class IRISAtDJBoothBehaviour : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        EndingCutsceneBehaviour.OnEndingCutscene += OnShowIRISAtDJBooth;
    }

    private void OnDisable()
    {
        EndingCutsceneBehaviour.OnEndingCutscene -= OnShowIRISAtDJBooth;
    }

    public void OnShowIRISAtDJBooth()
    {
        animator.SetTrigger("TurnOn");
    }
}
