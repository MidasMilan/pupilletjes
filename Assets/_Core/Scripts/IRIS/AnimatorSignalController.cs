﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorSignalController : MonoBehaviour
{
    [SerializeField] private string defaultBoolParamName;
    private Animator thisAnimator;

    private void Start()
    {
        thisAnimator = GetComponent<Animator>();
    }

    public void SetTrigger(string paramName, bool value)
    {
        thisAnimator.SetBool(defaultBoolParamName, value);
        if (value) thisAnimator.SetTrigger(paramName);
    }

    public void SetDefaultBool(bool value)
    {
        SetTrigger("StartTalking", value);
    }

    public void SetHappyBool(bool value)
    {
        SetTrigger("StartHappyTalking", value);
    }

    public void SetProudBool(bool value)
    {
        SetTrigger("StartProudTalking", value);
    }
}
