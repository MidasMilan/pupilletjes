﻿using UnityEngine;

public class TouchScreenBehaviour : MonoBehaviour
{
    [SerializeField] private TSUIElement hologramUIElement;
    [SerializeField] private Layout startingLayout;
    [SerializeField] private bool debugMode = false;

    public delegate void TouchScreenLayoutHandler(Layout newLayout);
    public static event TouchScreenLayoutHandler OnLayoutTransition;

    public enum Layout
    {
        None,
        StartScreen,
        MainMenu,
        EyeDropInstructions,
        ERGScrubInstructions,
        ERGWireInstructions,
        ERGElectrolyteInstructions,
        VEPMarkerInstructions,
        VEPScrubInstructions,
        VEPWireInstructions,
        EyeAnalysesInstructions,
        MinigameComplete
    }
    private Layout currentLayout;

    private void OnEnable()
    {
        MinigameManager.OnMinigameCompleted += MinigameManager_OnMinigameCompleted;
    }

    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= MinigameManager_OnMinigameCompleted;
    }

    private void MinigameManager_OnMinigameCompleted(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        TransitionToLayout(nextTask);
    }

    private void Start()
    {
        currentLayout = Layout.StartScreen;
        TransitionToLayout(currentLayout);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            TransitionToLayout(Layout.MainMenu);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            TransitionToLayout(Layout.VEPWireInstructions);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            TransitionToLayout(Layout.VEPMarkerInstructions);
        }
    }

    public void TransitionToLayout(Layout newLayout)
    {
        if (currentLayout == newLayout) return;

        bool currentLayoutIsMinigame = currentLayout != Layout.StartScreen && currentLayout != Layout.MainMenu;
        bool newLayoutIsMinigame = newLayout != Layout.StartScreen && newLayout != Layout.MainMenu;

        if(newLayoutIsMinigame && !currentLayoutIsMinigame)
        {
            hologramUIElement.DoScreenTransition(true);
        }
        else if(!newLayoutIsMinigame && currentLayoutIsMinigame)
        {
            hologramUIElement.DoScreenTransition(false);
        }

        if (debugMode) print($"transition starting to layout: {newLayout}");
        currentLayout = newLayout;
        OnLayoutTransition.Invoke(newLayout);
    }

    public void TransitionToLayout(MinigameManager.Minigames minigame)
    {
        Layout minigameLayout;
        switch (minigame)
        {
            case MinigameManager.Minigames.EyeDrop:
                minigameLayout = Layout.EyeDropInstructions;
                break;
            case MinigameManager.Minigames.AnalyseEye:
                minigameLayout = Layout.EyeAnalysesInstructions;
                break;
            case MinigameManager.Minigames.ERGScrub:
                minigameLayout = Layout.ERGScrubInstructions;
                break;
            case MinigameManager.Minigames.PlaceERGWires:
                minigameLayout = Layout.ERGWireInstructions;
                break;
            case MinigameManager.Minigames.PlaceERGStickers:
                minigameLayout = Layout.ERGElectrolyteInstructions;
                break;
            case MinigameManager.Minigames.PlaceVEPMarker:
                minigameLayout = Layout.VEPMarkerInstructions;
                break;
            case MinigameManager.Minigames.VEPScrub:
                minigameLayout = Layout.VEPScrubInstructions;
                break;
            case MinigameManager.Minigames.PlaceVEPWires:
                minigameLayout = Layout.VEPWireInstructions;
                break;
            case MinigameManager.Minigames.None:
                minigameLayout = Layout.MinigameComplete;
                break;
            default:
                Debug.LogError("Couldn't find layout of minigame. Did you add one in the TransitionToLayout switch case?");
                minigameLayout = Layout.None;
                break;
        }
        TransitionToLayout(minigameLayout);
    }
}
