﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TSUIElement), typeof(RectTransform))]
public class IRISFaceBehaviour : MonoBehaviour
{
    [SerializeField] private float fullscreenSize;
    [SerializeField] private float smallSize;
    [SerializeField] private float scaleAnimDuration;

    [SerializeField] private Vector2 fullscreenPos;
    [SerializeField] private Vector2 smallscreenPos;

    private TSUIElement thisUIElement;
    private RectTransform thisRectTrans;

    private void OnEnable()
    {
        TouchScreenBehaviour.OnLayoutTransition += onTransition;
    }
    private void OnDisable()
    {
        TouchScreenBehaviour.OnLayoutTransition -= onTransition;
    }

    private void onTransition(TouchScreenBehaviour.Layout newLayout)
    {
        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.None:
                break;
            case TouchScreenBehaviour.Layout.StartScreen:
                thisUIElement.DoScreenTransition(false);
                StartCoroutine(LerpWidthSin(smallSize, scaleAnimDuration));
                break;
            case TouchScreenBehaviour.Layout.MainMenu:
                thisUIElement.GoToPosition(fullscreenPos);
                StartCoroutine(LerpWidthSin(fullscreenSize, scaleAnimDuration));
                break;
            case TouchScreenBehaviour.Layout.MinigameComplete:
                thisUIElement.GoToPosition(fullscreenPos);
                StartCoroutine(LerpWidthSin(smallSize, scaleAnimDuration));
                break;
            default:
                thisUIElement.GoToPosition(smallscreenPos);
                StartCoroutine(LerpWidthSin(smallSize, scaleAnimDuration));
                break;
        }
    }

    private void Start()
    {
        thisUIElement = GetComponent<TSUIElement>();
        thisRectTrans = GetComponent<RectTransform>();
    }

    IEnumerator LerpWidthSin(float targetWidth, float duration)
    {
        float time = 0;
        float startWidth = thisRectTrans.sizeDelta.x;
        float endWidth = targetWidth;
        float currentWidth = startWidth;

        while (time < duration)
        {
            currentWidth = Mathf.Lerp(startWidth, endWidth, Mathf.Sin(time / duration * Mathf.PI * 0.5f));
            thisRectTrans.sizeDelta = new Vector2(currentWidth, currentWidth);
            time += Time.deltaTime;
            yield return null;
        }

        thisRectTrans.sizeDelta = new Vector2(targetWidth, targetWidth);
    }
}
