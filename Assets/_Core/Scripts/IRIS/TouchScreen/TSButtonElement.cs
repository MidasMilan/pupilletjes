﻿using UnityEngine;

[RequireComponent(typeof(TSUIElement), typeof(Collider), typeof(Rigidbody))]
public class TSButtonElement : MonoBehaviour
{
    [SerializeField] private bool activateOnRelease = true;
    [SerializeField] private TouchScreenBehaviour touchScreen;
    [SerializeField] private TouchScreenBehaviour.Layout transitionsToLayout = TouchScreenBehaviour.Layout.None;
    private Collider thisCollider;
    private TSUIElement uiElement;

    private void Start()
    {
        thisCollider = GetComponent<Collider>();
        uiElement = GetComponent<TSUIElement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("IndexFinger"))
        {
            uiElement.InvokePressDelegate();
            if (!activateOnRelease) OnButtonActivate();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("IndexFinger"))
        {
            if (activateOnRelease) OnButtonActivate();
        }
    }

    private void OnButtonActivate()
    {
        uiElement.InvokeActivateDelegate();
        if (transitionsToLayout != TouchScreenBehaviour.Layout.None) touchScreen.TransitionToLayout(transitionsToLayout);
        SetButtonEnabled(false);
    }

    private void SetButtonEnabled(bool value)
    {
        thisCollider.enabled = value;
    }
}
