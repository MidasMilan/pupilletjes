﻿using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(TSUIElement), typeof(Image))]
public class TSImageElement : MonoBehaviour
{
    private Image thisImage;
    private Sprite defaultSprite;
    [SerializeField] private Sprite pressedSprite;
    private TSUIElement uiElement;

    void Start()
    {
        thisImage = GetComponent<Image>();
        defaultSprite = thisImage.sprite;
        if (pressedSprite == null) pressedSprite = defaultSprite;
    }

    private void OnEnable()
    {
        uiElement = GetComponent<TSUIElement>();
        uiElement.OnPressed += TSImageElement_OnPressed;
    }

    private void OnDisable()
    {
        uiElement.OnPressed -= TSImageElement_OnPressed;
    }

    private void TSImageElement_OnPressed()
    {
        thisImage.sprite = pressedSprite;
    }

    public void DoScreenTransition(bool value)
    {
        if (value)
        {
            thisImage.sprite = defaultSprite;
        }
    }
}
