﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class ConfettiBehaviour : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] confettiParticleSystems;
    [SerializeField] private AudioClip confettiCannonSound;

    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    private void OnEnable()
    {
        MinigameManager.OnMinigameCompleted += SpawnConfetti;
    }

    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= SpawnConfetti;
    }

    private void SpawnConfetti(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        if (nextTask != MinigameManager.Minigames.None) return;

        audioSource.PlayOneShot(confettiCannonSound);

        foreach (ParticleSystem particleSystem in confettiParticleSystems)
        {
            particleSystem.Play();
        }
    }
}
