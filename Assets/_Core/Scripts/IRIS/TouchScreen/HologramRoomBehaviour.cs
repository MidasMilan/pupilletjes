﻿using UnityEngine;

public class HologramRoomBehaviour : MonoBehaviour
{
    [SerializeField] private HologramData[] holograms;

    public void SetHologram(Alien.AlienType hologramType)
    {
        foreach (HologramData hologram in holograms)
        {
            bool ifSetActive = hologram.GetAlienType() == hologramType;
            hologram.gameObject.SetActive(hologram.GetAlienType() == hologramType);
        }
    }
}
