﻿using System.Collections;
using UnityEngine;

public class TSUIElement : MonoBehaviour
{
    [SerializeField] private Vector2 onscreenPos;
    [SerializeField] private Vector2 offscreenPos;
    [SerializeField] private float transitionDuration = 0.25f;
    [SerializeField] private TouchScreenBehaviour.Layout isPartOffLayout;

    public delegate void UIElementEventHandler();
    public event UIElementEventHandler OnActivate;
    public event UIElementEventHandler OnPressed;

    public void InvokeActivateDelegate()
    {
        OnActivate?.Invoke();
    }
    public void InvokePressDelegate()
    {
        OnPressed?.Invoke();
    }

    private void OnEnable()
    {
        if (isPartOffLayout != TouchScreenBehaviour.Layout.None)
        {
            TouchScreenBehaviour.OnLayoutTransition += TouchScreenBehaviour_OnLayoutTransition;
        }
    }

    private void OnDisable()
    {
        if (isPartOffLayout != TouchScreenBehaviour.Layout.None)
        {
            TouchScreenBehaviour.OnLayoutTransition -= TouchScreenBehaviour_OnLayoutTransition;
        }
    }

    private void TouchScreenBehaviour_OnLayoutTransition(TouchScreenBehaviour.Layout newLayout)
    {
        if (newLayout == isPartOffLayout) DoScreenTransition(true);
        else DoScreenTransition(false);
    }

    public virtual void DoScreenTransition(bool onScreen)
    {
        if (onScreen)
        {
            StartCoroutine(LerpToPosSin(onscreenPos, transitionDuration));
        }
        else
        {
            StartCoroutine(LerpToPosCos(offscreenPos, transitionDuration));
        }
    }

    public void GoToPosition(Vector2 targetPos)
    {
        StartCoroutine(LerpToPosSin(targetPos, transitionDuration));
    }

    //Start slow, end quick
    IEnumerator LerpToPosCos(Vector2 targetPos, float duration)
    {
        float time = 0;
        Vector2 startPos = transform.localPosition;
        Vector2 currentPos = transform.localPosition;

        while (time < duration)
        {
            currentPos = Vector2.Lerp(targetPos, startPos, Mathf.Cos(time / duration * Mathf.PI * 0.5f));
            transform.localPosition = currentPos;
            time += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = targetPos;
    }

    //Start quick, end slow
    IEnumerator LerpToPosSin(Vector2 targetPos, float duration)
    {
        float time = 0;
        Vector2 startPos = transform.localPosition;
        Vector2 currentPos = transform.localPosition;

        while (time < duration)
        {
            currentPos = Vector2.Lerp(startPos, targetPos, Mathf.Sin(time / duration * Mathf.PI * 0.5f));
            transform.localPosition = currentPos;
            time += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = targetPos;
    }

    //Start slow, middle quick, end slow
    IEnumerator LerpToPosSinCos(Vector2 targetPos, float duration)
    {
        float time = 0;
        Vector3 startPos = transform.localPosition;
        Vector3 halfPos = Vector2.Lerp(startPos, targetPos, 0.5f);
        float halfDuration = duration * 0.5f;

        while (time < duration)
        {
            if (time < halfDuration)
            {
                transform.localPosition = Vector2.Lerp(startPos, halfPos, Mathf.Sin(time / halfDuration * Mathf.PI * 0.5f));
            }
            else
            {
                transform.localPosition = Vector2.Lerp(targetPos, halfPos, Mathf.Cos((time - halfDuration) / halfDuration * Mathf.PI * 0.5f));
            }
            time += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = targetPos;
    }
}
