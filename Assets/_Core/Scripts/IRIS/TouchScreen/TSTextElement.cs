﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TSUIElement))]
public class TSTextElement : MonoBehaviour
{
    [SerializeField] private TMP_Text buttonTextPointer;
    [SerializeField] private string changeTextOnPress;
    private string defaultText;
    private TSUIElement uiElement;

    private void OnEnable()
    {
        uiElement = GetComponent<TSUIElement>();
        uiElement.OnPressed += TSTextElement_OnPressed;
    }

    private void OnDisable()
    {
        uiElement.OnPressed -= TSTextElement_OnPressed;
    }

    private void Start()
    {
        defaultText = buttonTextPointer.text;
    }

    private void TSTextElement_OnPressed()
    {
        buttonTextPointer.text = changeTextOnPress;
    }

    public void ResetText()
    {
        buttonTextPointer.text = defaultText;
    }
}
