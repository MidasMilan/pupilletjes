﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Collections;

[RequireComponent(typeof(Slider), typeof(TSUIElement))]
public class ProgressionBarBehaviour : MonoBehaviour
{
    [SerializeField] private TMP_Text sliderTextPointer;
    [SerializeField] private float barFillTransitionLength;
    private Slider thisSlider;
    private TSUIElement thisUIElement;
    private int aliensHelped;
    private int aliensNeeded;

    private void OnEnable()
    {
        MinigameManager.OnMinigameCompleted += MinigameManager_OnMinigameCompleted;
    }
    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= MinigameManager_OnMinigameCompleted;
    }

    private void ProgressionBuildingsManager_OnNewBuildingTask(int aliensNeededForTask, int buildingID)
    {
        aliensNeeded = aliensNeededForTask;
        aliensHelped = 0;
        UpdateSlider(aliensHelped, aliensNeeded);
        thisUIElement.DoScreenTransition(true);
    }

    private void Start()
    {
        aliensHelped = 0;
        aliensNeeded = 0;
        thisSlider = GetComponent<Slider>();
        thisUIElement = GetComponent<TSUIElement>();
    }

    private void MinigameManager_OnMinigameCompleted(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        if (nextTask != MinigameManager.Minigames.None) return;

        aliensHelped++;
        if (aliensNeeded < 1)
        {
            Debug.LogError($"Aliens needed in {gameObject.name} is lower than 1, setting to 1");
            aliensNeeded = 1;
        }
        UpdateSlider(aliensHelped, aliensNeeded);
    }

    public void UpdateSlider(int aliensHelped, int aliensNeeded)
    {
        sliderTextPointer.text = $"{aliensHelped} / {aliensNeeded}";
        StartCoroutine(LerpToFillValue((float)aliensHelped / aliensNeeded, barFillTransitionLength));
    }

    IEnumerator LerpToFillValue(float targetValue, float duration)
    {
        float time = 0;
        float startValue = thisSlider.value;
        float currentValue = startValue;

        while (time < duration)
        {
            currentValue = Mathf.Lerp(startValue, targetValue, Mathf.Sin(time / duration * Mathf.PI * 0.5f));
            thisSlider.value = currentValue;
            time += Time.deltaTime;
            yield return null;
        }

        thisSlider.value = targetValue;
    }
}
