﻿using UnityEngine;

public class HologramData : MonoBehaviour
{
    [SerializeField] private Alien.AlienType alienType;
    public Alien.AlienType GetAlienType()
    {
        return alienType;
    }
}
