﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class SlidingPanelBehaviour : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] float panelSlidingSpeed;
    [SerializeField] float panelOpenYPosition;
    [SerializeField] float panelClosedYPosition;
    [SerializeField] AudioClip openSound;
    [SerializeField] AudioClip closeSound;

    [SerializeField] GameObject scrubTub;
    [SerializeField] GameObject wireControlPanel;
    [SerializeField] GameObject objectToOpenAtStart;

    private bool lidOpen;
    private GameObject objectToSpawn;

    private AudioSource audioSource;

    private void OnEnable()
    {
        TouchScreenBehaviour.OnLayoutTransition += OnTransition;

        lidOpen = false;
        objectToSpawn = null;
        audioSource = gameObject.GetComponent<AudioSource>();
        OpenLidWithObject(objectToOpenAtStart);
    }

    private void OnDisable()
    {
        TouchScreenBehaviour.OnLayoutTransition -= OnTransition;
    }

    private void OnTransition(TouchScreenBehaviour.Layout newLayout)
    {
        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.ERGScrubInstructions:
                OpenLidWithObject(scrubTub);
                break;
            case TouchScreenBehaviour.Layout.ERGWireInstructions:
                OpenLidWithObject(wireControlPanel);
                break;
            case TouchScreenBehaviour.Layout.VEPScrubInstructions:
                OpenLidWithObject(scrubTub);
                break;
            case TouchScreenBehaviour.Layout.VEPWireInstructions:
                OpenLidWithObject(wireControlPanel);
                break;
            case TouchScreenBehaviour.Layout.MainMenu:
                CloseLid();
                break;
            default:
                break;
        }
    }

    public void OpenLidWithObject(GameObject spawnObject)
    {
        objectToSpawn = spawnObject;
        if (lidOpen) CloseLid();
        else CheckSpawnConditions();
    }

    private void OpenLid()
    {
        StartCoroutine(MoveLid(panel.transform, panelOpenYPosition, panelSlidingSpeed));
        audioSource.PlayOneShot(openSound);
        lidOpen = true;
    }

    public void CloseLid()
    {
        StartCoroutine(MoveLid(panel.transform, panelClosedYPosition, panelSlidingSpeed));
        audioSource.PlayOneShot(closeSound);
        lidOpen = false;
    }

    private IEnumerator MoveLid(Transform target, float targetPos, float duration)
    {
        float time = 0;
        float startPos = target.localPosition.y;
        float currentPos = target.localPosition.y;

        while (time < duration)
        {
            currentPos = Mathf.Lerp(startPos, targetPos, Mathf.Sin(time / duration * Mathf.PI * 0.5f));
            target.localPosition = new Vector3(target.localPosition.x, currentPos, target.localPosition.z);
            time += Time.deltaTime;
            yield return null;
        }

        target.localPosition = new Vector3(target.localPosition.x, targetPos, target.localPosition.z);
        CheckSpawnConditions();
    }

    private void CheckSpawnConditions()
    {
        if (!lidOpen && objectToSpawn != null)
        {
            objectToOpenAtStart.SetActive(objectToSpawn == objectToOpenAtStart);
            scrubTub.SetActive(objectToSpawn == scrubTub);
            wireControlPanel.SetActive(objectToSpawn == wireControlPanel);
            objectToSpawn = null;
            OpenLid();
        }
    }
}
