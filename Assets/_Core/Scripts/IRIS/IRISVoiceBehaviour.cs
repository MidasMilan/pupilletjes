﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class IRISVoiceBehaviour : MonoBehaviour
{
    [SerializeField] private float betweenQueuePlayOffset = 0.1f;
    [SerializeField] private Dialogue[] getAttentionClips;
    [SerializeField] private Dialogue[] rudeInterruptClips;
    
    [SerializeField] private Animator modelAnimatorPointer;
    [SerializeField] private Animator faceAnimatorPointer;

    [SerializeField] private bool debugMode = false;


    private Queue<Dialogue> queuedVoices;
    private AudioSource thisAS;
    private Dialogue currentlyPlaying;

    private void Start()
    {
        queuedVoices = new Queue<Dialogue>();
        thisAS = GetComponent<AudioSource>();
    }

    private void PlayVoiceLine(Dialogue playingDialogue)
    {
        if (playingDialogue.canOnlyPlayOnce && playingDialogue.hasBeenPlayed)
        {
            CheckQueue();
        }
        else
        {
            currentlyPlaying = playingDialogue;
            thisAS.PlayOneShot(playingDialogue.voiceClip, playingDialogue.volume);
            playingDialogue.hasBeenPlayed = true;

            modelAnimatorPointer.SetFloat("RandomIdleValue", Random.Range(0.0f, 1.0f));
            if(playingDialogue.animatorTrigger != null) modelAnimatorPointer.SetTrigger(playingDialogue.animatorTrigger);
            if(playingDialogue.faceAnimatorTrigger != null) faceAnimatorPointer.SetTrigger(playingDialogue.faceAnimatorTrigger);
            faceAnimatorPointer.SetBool("IsTalking", true);

            if (queuedVoices.Count <= 1)
            {
                StartCoroutine(CheckQueueAfterSeconds(playingDialogue.voiceClip.length));
            }

            if (debugMode) print($"playing voiceline: {playingDialogue.clipName}");
        }
    }

    public void ForceVoiceLine(Dialogue newDialogue, bool clearQueue = true)
    {
        if(thisAS.isPlaying)
        {
            thisAS.Stop();
            if (clearQueue) queuedVoices.Clear();
        }
        PlayVoiceLine(newDialogue);
    }

    public void AddVoicelineToQueue(Dialogue newDialogue)
    {
        if (debugMode) print($"Adding to queue: {newDialogue.clipName}");
        queuedVoices.Enqueue(newDialogue);
        if (queuedVoices.Count <= 1)
        {
            PlayVoiceLine(newDialogue);
        }
    }

    public void AddVoiceLineToQueue(Dialogue[] newDialogues)
    {
        if (newDialogues.Length > 0) {
            if (queuedVoices.Count == 0)
            {
                if (newDialogues.Length > 1)
                {
                    for (int i = 1; i < newDialogues.Length; i++)
                    {
                        queuedVoices.Enqueue(newDialogues[i]);
                    }
                }
                PlayVoiceLine(newDialogues[0]);
            }
        }
    }

    public void AddRandomVoiceLineToQueue(Dialogue[] newDialogues)
    {
        if (newDialogues.Length > 0)
        {
            int randomInt = Random.Range(0, newDialogues.Length);
            if (debugMode) print($"Adding to queue: {newDialogues[randomInt].clipName}");
            queuedVoices.Enqueue(newDialogues[randomInt]);
            if (queuedVoices.Count <= 1)
            {
                PlayVoiceLine(newDialogues[randomInt]);
            }
        }
    }

    IEnumerator CheckQueueAfterSeconds(float totalTimeTillCheck)
    {
        float time = 0.0f;
        float duration = totalTimeTillCheck + betweenQueuePlayOffset;

        while(time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        CheckQueue();
    }

    private void CheckQueue()
    {
        if(thisAS.isPlaying)
        {
            if (debugMode) print("I am kind of playing right now my guy");
            CheckQueueAfterSeconds(currentlyPlaying.voiceClip.length);
            return;
        }

        if (queuedVoices.Count >= 2)
        {
            Dialogue nextDialogue = queuedVoices.Dequeue();
            PlayVoiceLine(nextDialogue);
            CheckQueueAfterSeconds(nextDialogue.voiceClip.length);
        }
        else if (queuedVoices.Count == 1)
        {
            queuedVoices.Dequeue();
        }
        else
        {
            faceAnimatorPointer.SetBool("IsTalking", false);

            if(debugMode) print("queue empty");
        }
    }
}
