﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]

public class IRISTableBehaviour : MonoBehaviour
{
    [SerializeField] GameObject lever;
    [SerializeField] GameObject tableMovingPart;
    [SerializeField] float maxContraction = -0.465f;
    [SerializeField] float maxExtension = 0.235f;
    [SerializeField] float slowMoveAngle = 10.0f;
    [SerializeField] float fastMoveAngle = 40.0f;
    [SerializeField] float moveSpeed = 0.01f;

    [SerializeField] private bool debugMode = false;

    private bool stoppingSoundEffect;

    [SerializeField] private AudioSource effectAudioSource;

    private Animator animator;
    private HingeJoint leverHinge;
    private float springForce;

    private void Start()
    {
        stoppingSoundEffect = false;
        animator = GetComponent<Animator>();
        leverHinge = lever.GetComponent<HingeJoint>();
    }

    private void Update()
    {
        float leverAngle = Quaternion.Angle(lever.transform.rotation, tableMovingPart.transform.rotation);
        float dot = Quaternion.Dot(lever.transform.localRotation, Quaternion.LookRotation(Vector3.down, Vector3.right));
        bool shouldMoveUp = dot <= 0.5;
        if (debugMode) print($"dot: {dot}, angle: {leverAngle}");

        if (leverAngle >= fastMoveAngle)
        {
            changePosition(shouldMoveUp);
            PlaySound(1.2f);
        }
        //do slow movement
        else if (leverAngle >= slowMoveAngle)
        {
            changePosition(shouldMoveUp, 0.5f);
            PlaySound();
        }
        else if (effectAudioSource.isPlaying && !stoppingSoundEffect)
        {
            StartCoroutine(TurnOffSoundEffect());
            stoppingSoundEffect = true;
        }

        checkPosition();
    }

    private void changePosition(bool shouldMoveUp, float multiplier = 1.0f)
    {
        if (shouldMoveUp)
        {
            tableMovingPart.transform.localPosition += Vector3.up * moveSpeed * multiplier * Time.deltaTime;
        }
        else
        {
            tableMovingPart.transform.localPosition -= Vector3.up * moveSpeed * multiplier * Time.deltaTime;
        }
    }
    private void checkPosition()
    {
        if (tableMovingPart.transform.localPosition.y < maxContraction)
        {
            tableMovingPart.transform.localPosition = new Vector3(tableMovingPart.transform.localPosition.x, maxContraction, tableMovingPart.transform.localPosition.z);
        }
        else if (tableMovingPart.transform.localPosition.y > maxExtension)
        {
            tableMovingPart.transform.localPosition = new Vector3(tableMovingPart.transform.localPosition.x, maxExtension, tableMovingPart.transform.localPosition.z);
        }
    }

    private void PlaySound(float pitch = 1.0f)
    {
        StopAllCoroutines();
        stoppingSoundEffect = false;
        effectAudioSource.volume = 1.0f;
        effectAudioSource.pitch = pitch;
        if (!effectAudioSource.isPlaying) effectAudioSource.Play();
    }

    IEnumerator TurnOffSoundEffect()
    {
        float duration = 0.1f;
        float time = duration;

        while (time < 0.0f)
        {
            time -= Time.deltaTime;
            effectAudioSource.volume = time / duration;
            yield return null;
        }

        effectAudioSource.volume = 0.0f;
        effectAudioSource.Stop();
        stoppingSoundEffect = false;
    }

    private void OnEnable()
    {
        EndingCutsceneBehaviour.OnEndingCutscene += OnHideIRISAtTable;
    }

    private void OnDisable()
    {
        EndingCutsceneBehaviour.OnEndingCutscene -= OnHideIRISAtTable;
    }

    public void OnHideIRISAtTable()
    {
        animator.SetTrigger("TurnOff");
    }

    public void OnGrabLever()
    {
        springForce = leverHinge.spring.spring;
        JointSpring spring = leverHinge.spring;
        spring.spring = 0.0f;
        leverHinge.spring = spring;
    }

    public void OnReleaseLever()
    {
        JointSpring spring = leverHinge.spring;
        spring.spring = springForce;
        leverHinge.spring = spring;
    }
}
