﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMatsSpawner : MonoBehaviour
{
    [Header("PROPERTIES")]
    [SerializeField] private float spawnDuration;
    [SerializeField] private float spawnScaleOvershoot;
    [SerializeField] private float spawnOvershootDuration;
    [SerializeField] private float pickupTillBuildMatEventBuffer = 1.0f;

    [SerializeField] private IRISVoiceBehaviour IRISVoice;
    [SerializeField] private Dialogue[] MaterialSpawnDialogues;

    [Header("SFX")]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip buildMaterialsSpawnSound;
    [SerializeField] private AudioClip buildMaterialsPickupSound;
    [SerializeField] private float buildMaterialVolume = 0.5f;

    [Header("SET IN PREFAB")]
    [SerializeField] private GameObject[] buildMatPrefabs;
    [SerializeField] private Transform spawnEndPoint;
    [SerializeField] private ConveyorBeltBehaviour conveyorPointer;

    public delegate void BuildMatsLineHandler();
    public static event BuildMatsLineHandler OnRemoveBuildMatNeeded;


    private int totalBuildMatsNeeded;
    public int GetTotalBuildMatsNeeded() => totalBuildMatsNeeded;

    public void AddBuildMatNeeded() 
    {
        if (totalBuildMatsNeeded == 0) SpawnBuildMat();
        totalBuildMatsNeeded++; 
    }
    public void RemoveBuildMatNeeded() 
    {
        totalBuildMatsNeeded--;
        if (totalBuildMatsNeeded > 0)
        {
            SpawnBuildMat();
        }
        StartCoroutine(EventInvokeBuffer());
    }

    private void OnEnable()
    {
        totalBuildMatsNeeded = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            SpawnBuildMat();
        }
    }

    public GameObject SpawnBuildMat()
    {
        GameObject newBuildMats = Instantiate(buildMatPrefabs[Random.Range(0, buildMatPrefabs.Length - 1)]);
        newBuildMats.transform.localScale = Vector3.zero;
        newBuildMats.transform.position = transform.position;
        newBuildMats.GetComponent<BuildMaterialBehaviour>().SetSpawner(this);
        StartCoroutine(SpawnAnimation(newBuildMats, spawnDuration, spawnOvershootDuration, spawnScaleOvershoot));
        IRISVoice.AddRandomVoiceLineToQueue(MaterialSpawnDialogues);
        return newBuildMats;
    }

    public void PlayBuildMatPickupSound()
    {
        audioSource.PlayOneShot(buildMaterialsPickupSound, buildMaterialVolume);
    }

    public void PlayBuildMatSpawnSound()
    {
        audioSource.PlayOneShot(buildMaterialsSpawnSound, buildMaterialVolume);
    }

    IEnumerator EventInvokeBuffer()
    {
        float time = 0;

        while (time < pickupTillBuildMatEventBuffer)
        {
            time += Time.deltaTime;
            yield return null;
        }

        OnRemoveBuildMatNeeded?.Invoke();
    }

    IEnumerator SpawnAnimation(GameObject buildMatObj, float totalDuration, float overshootDuration, float overshootScaleFloat)
    {
        float time = 0;
        float beforeOvershootDuration = totalDuration - overshootDuration;
        float halfOvershootDuration = overshootDuration * 0.5f;
        Vector3 startPos = transform.position;
        Vector3 endPos = spawnEndPoint.position;
        Vector3 overshootScale = new Vector3(overshootScaleFloat, overshootScaleFloat, overshootScaleFloat);

        while (time < totalDuration)
        {
            if (time < beforeOvershootDuration)
            {
                buildMatObj.transform.position = Vector3.Lerp(startPos, endPos, Mathf.Sin(time / beforeOvershootDuration * Mathf.PI * 0.5f));
                buildMatObj.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, Mathf.Cos(time / beforeOvershootDuration * Mathf.PI * 0.5f));
            }
            else if ((time - beforeOvershootDuration) < halfOvershootDuration)
            {
                buildMatObj.transform.position = endPos;
                buildMatObj.transform.localScale = Vector3.Lerp(Vector3.one, overshootScale, Mathf.Sin((time - beforeOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            else
            {
                buildMatObj.transform.localScale = Vector3.Lerp(Vector3.one, overshootScale, Mathf.Cos((time - beforeOvershootDuration - halfOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            time += Time.deltaTime;
            yield return null;
        }

        buildMatObj.tag = "BuildMat";
        buildMatObj.transform.localScale = Vector3.one;
        buildMatObj.GetComponent<Rigidbody>().isKinematic = false;
        conveyorPointer.EnableConveyor(true);
    }
}
