﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeatingHighlightBehaviour : MonoBehaviour
{

    [SerializeField] float AppearAnimationDuration;
    [SerializeField] float HideAnimationDuration;

    private Vector3 scaledDownVector = new Vector3(0.0f, 0.0f, 1.0f);
    private Vector3 scaledUpVector = new Vector3(1.0f, 1.0f, 1.0f);

    private void OnEnable()
    {
        DobberBehaviour.OnHooked += OnShowHighlight;
        DobberBehaviour.OnUnhooked += OnHideHighlight;
    }

    private void OnDisable()
    {
        DobberBehaviour.OnHooked -= OnShowHighlight;
        DobberBehaviour.OnUnhooked -= OnHideHighlight;
    }
    public void OnShowHighlight()
    {
        StartCoroutine(ScaleUpAnimation(AppearAnimationDuration));
    }

    public void OnHideHighlight()
    {
        StartCoroutine(ScaleDownAnimation(HideAnimationDuration));
    }

    IEnumerator ScaleUpAnimation(float totalDuration)
    {
        float time = 0;
        float overshootDuration = totalDuration / 3;
        float beforeOvershootDuration = totalDuration - overshootDuration;
        float halfOvershootDuration = overshootDuration * 0.5f;
        Vector3 overshootScale = new Vector3(1.1f, 1.1f, 1.0f);

        while (time < totalDuration)
        {
            if (time < beforeOvershootDuration)
            {
                gameObject.transform.localScale = Vector3.Lerp(scaledUpVector, scaledDownVector, Mathf.Cos(time / beforeOvershootDuration * Mathf.PI * 0.5f));
            }
            else if ((time - beforeOvershootDuration) < halfOvershootDuration)
            {
                gameObject.transform.localScale = Vector3.Lerp(scaledUpVector, overshootScale, Mathf.Sin((time - beforeOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            else
            {
                gameObject.transform.localScale = Vector3.Lerp(scaledUpVector, overshootScale, Mathf.Cos((time - beforeOvershootDuration - halfOvershootDuration) / halfOvershootDuration * Mathf.PI * 0.5f));
            }
            time += Time.deltaTime;
            yield return null;
        }

        gameObject.transform.localScale = scaledUpVector;
    }

    IEnumerator ScaleDownAnimation(float totalDuration)
    {
        float time = 0;
        while (time < totalDuration)
        {
            gameObject.transform.localScale = Vector3.Lerp(scaledDownVector, scaledUpVector, Mathf.Cos(time / totalDuration * Mathf.PI * 0.5f));
            time += Time.deltaTime;
            yield return null;
        }

        gameObject.transform.localScale = scaledDownVector;
    }
}
