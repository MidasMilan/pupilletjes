﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class BuildMaterialBehaviour : MonoBehaviour
{
    [SerializeField] private float despawnTime = 10.0f;
    [SerializeField] private float despawnAnimDuration = 0.5f;
    [SerializeField] private float forceDespawnAfterNoPickupLength = 60.0f;

    public bool despawning;

    private Collider thisColl;
    private Rigidbody thisRB;
    private BuildMatsSpawner spawner;

    private float pickupCounter;

    public void SetSpawner(BuildMatsSpawner spawner)
    {
        this.spawner = spawner;
    }

    private void Start()
    {
        despawning = false;
        thisColl = GetComponent<Collider>();
        thisRB = GetComponent<Rigidbody>();
        pickupCounter = 0.0f;
        spawner.PlayBuildMatSpawnSound();
    }

    private void Update()
    {
        pickupCounter += Time.deltaTime;
        if (pickupCounter >= forceDespawnAfterNoPickupLength && !despawning)
        {
            despawning = true;
            spawner.SpawnBuildMat();
            StartCoroutine(DespawnAnim(despawnAnimDuration, despawnAnimDuration - 0.1f));
            Destroy(gameObject, despawnTime);
        }
    }

    public void OnCreaturePickup()
    {
        spawner.RemoveBuildMatNeeded();
        thisColl.enabled = false;
        thisRB.isKinematic = true;
        gameObject.tag = "Untagged";
        spawner.PlayBuildMatPickupSound();
    }

    public void OnPlayerPickup()
    {
        if (despawning) return;
        despawning = true;
        spawner.SpawnBuildMat();
        spawner.PlayBuildMatPickupSound();
    }

    public void OnThrow()
    {
        StartCoroutine(DespawnAnim(despawnAnimDuration, despawnTime - despawnAnimDuration - 0.1f));
        Destroy(gameObject, despawnTime);

    }

    IEnumerator DespawnAnim(float duration, float timeTillStart)
    {
        float time = 0;
        float totalDuration = duration + timeTillStart;

        while (time < totalDuration)
        {
            time += Time.deltaTime;

            if (time > timeTillStart)
            {
                transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, Mathf.Cos((time - timeTillStart) / duration * Mathf.PI * 0.5f));
            }

            yield return null;
        }
    }
}
