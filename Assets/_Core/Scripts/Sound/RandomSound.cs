﻿using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [SerializeField] private AudioClip[] clips;
    [SerializeField] AudioSource audioSource;

    private void Start()
    {
        if (audioSource == null)
        {
            Debug.LogError("An audio source is required!");
        }
    }

    public void PlayRandomSound(float volume = 1.0f)
    {
        int randomInt = Random.Range(0, clips.Length);
        audioSource.PlayOneShot(clips[randomInt], volume);
    }
}
