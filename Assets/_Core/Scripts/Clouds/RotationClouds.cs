﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationClouds : MonoBehaviour
{
    public float rotationSpeed;
    public GameObject target;

    void Update()
    {
        transform.RotateAround(target.transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
    