﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [Header("Assign Before Use")]
    [SerializeField] [Tooltip("Scriptable Object that contains list of Units to Spawn")]
    SpawnPointScriptableObject ObjectsToSpawn;
    [SerializeField] private int spawnRadius = 1;

    [Header("References of spawned creature")]
    [SerializeField] private MinigameManager minigameManager;
    [SerializeField] private ProgressionBuildingsManager progressionManager;
    [SerializeField] private DobberBehaviour dobberBehaviour;
    [SerializeField] private GameObject table;

    [Header("Attributes of spawned creature")]
    [SerializeField] private bool isHelped = false;
    public void SpawnEntity()
    {
        int _RandomCreature = Random.Range(0, ObjectsToSpawn.GetObjectsToSpawn.Length);
        Vector2 randomUnitCircle = Random.insideUnitCircle * spawnRadius;
        Vector3 _RandomSpawnPosition = new Vector3 (randomUnitCircle.x, 0.0f, randomUnitCircle.y);
        _RandomSpawnPosition += transform.position;

        GameObject entity = Instantiate(ObjectsToSpawn.GetObjectsToSpawn[_RandomCreature], _RandomSpawnPosition, Quaternion.identity);
        entity.GetComponent<Alien>().setNonPrefabReferences(minigameManager, progressionManager, progressionManager.GetSupplyPoint(), dobberBehaviour, table, isHelped);
    }

    public void SpawnMultipleEntities(int amountToSpawn)
    {
        for (int i = 0; i < amountToSpawn; i++)
        {
            SpawnEntity();
        }
    }
}
