﻿using UnityEngine;

public class WireAttachmentController : MonoBehaviour
{
    [SerializeField] private MeshRenderer wireAttachmentLightRenderer;
    public void SetLightToActive(bool active = true)
    {
        wireAttachmentLightRenderer.material.SetInt("_IsActive", active ? 1 : 0);
    }
}
