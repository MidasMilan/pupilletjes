﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ScrubBehaviour : MonoBehaviour
{
    [SerializeField] AudioClip scrubAudioClip;
    [SerializeField] private MeshRenderer highlightAttachmentRenderer;
    [SerializeField] private Material disableHighlightMaterial;

    private ERGAttachmentCreature ERGCreaturePointer;
    private VEPAttachmentCreature VEPCreaturePointer;
    private AudioSource audioSource;

    public bool isScrubbed = false;

    void Start()
    {
        if (GetComponentInParent<ERGAttachmentCreature>() == null)
        {
            Debug.LogError(gameObject.name + " couldn't find ERGAttachmentCreature script in parent! Attachment point will not contribute to minigame completion!");
        }
        ERGCreaturePointer = GetComponentInParent<ERGAttachmentCreature>();
        VEPCreaturePointer = GetComponentInParent<VEPAttachmentCreature>();
        audioSource = ERGCreaturePointer.GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ScrubTool"))
        {
            if (isScrubbed) return;
            
            ScrubToolBehaviour scrubToolBehaviour = other.GetComponent<ScrubToolBehaviour>();
            if (scrubToolBehaviour.GetScrubApplied())
            {
                audioSource.PlayOneShot(scrubAudioClip);
                SetHighlightAttachmentToActive();
                scrubToolBehaviour.SetScrubApplied(false);
                if (!isScrubbed) isScrubbed = true;
                if (ERGCreaturePointer.enabled) ERGCreaturePointer.CheckAllAttachmentPointsScrubbed();
                if (VEPCreaturePointer.enabled) VEPCreaturePointer.CheckAllAttachmentPointsScrubbed();
            }
        }
    }

    private void SetHighlightAttachmentToActive(bool active = true)
    {
        List<Material> m = new List<Material>();
        highlightAttachmentRenderer.GetMaterials(m);
        m.RemoveAt(0);
        highlightAttachmentRenderer.materials = m.ToArray();

        highlightAttachmentRenderer.material = disableHighlightMaterial;
        highlightAttachmentRenderer.material.SetInt("_IsActive", active ? 1 : 0);
    }
}
