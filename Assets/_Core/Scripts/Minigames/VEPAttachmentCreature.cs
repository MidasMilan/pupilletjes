﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(Alien))]
public class VEPAttachmentCreature : MonoBehaviour
{
    [SerializeField] private MarkerBehaviour[] MarkerPoints;
    [SerializeField] private ScrubBehaviour[] ScrubPoints;
    [SerializeField] private WireBehaviour[] WirePoints;
    [SerializeField] private AudioClip pointsMarkedAudioClip;
    [SerializeField] private AudioClip pointsScrubbedAudioClip;
    [SerializeField] private AudioClip wiresAttachedAudioClip;

    private Alien thisAlien;
    private List<WireAttachmentController> attachmentControllers;
    private AudioSource audioSource;

    private enum AttachmentPointAction
    {
        Marked,
        Scrubbed,
        Wired
    }
    AttachmentPointAction currentStep;

    private void OnEnable()
    {
        attachmentControllers = new List<WireAttachmentController>();
        audioSource = gameObject.GetComponent<AudioSource>();
        currentStep = AttachmentPointAction.Marked;
        thisAlien = GetComponent<Alien>();

        TouchScreenBehaviour.OnLayoutTransition += TouchScreenBehaviour_OnLayoutTransition;
    }

    private void OnDisable()
    {
        TouchScreenBehaviour.OnLayoutTransition -= TouchScreenBehaviour_OnLayoutTransition;
    }

    private void TouchScreenBehaviour_OnLayoutTransition(TouchScreenBehaviour.Layout newLayout)
    {
        if (thisAlien.GetMinigameManager().GetCurrentTestingAlien() != thisAlien) return;

        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.VEPMarkerInstructions:
                for (int i = 0; i < MarkerPoints.Length; i++)
                {
                    MarkerPoints[i].gameObject.SetActive(true);
                }
                break;
            case TouchScreenBehaviour.Layout.VEPScrubInstructions:
                for (int i = 0; i < MarkerPoints.Length; i++)
                {
                    MarkerPoints[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < ScrubPoints.Length; i++)
                {
                    ScrubPoints[i].gameObject.SetActive(true);
                }
                break;
            
            case TouchScreenBehaviour.Layout.VEPWireInstructions:
                for (int i = 0; i <ScrubPoints.Length; i++)
                {
                   ScrubPoints[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < WirePoints.Length; i++)
                {
                    WirePoints[i].gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
    }

    public void DisableAllVEPComponents()
    {
        if (MarkerPoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < MarkerPoints.Length; i++)
            {
                MarkerPoints[i].gameObject.SetActive(false);
            }
        }

        if (ScrubPoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < ScrubPoints.Length; i++)
            {
                ScrubPoints[i].gameObject.SetActive(false);
            }
        }

        if (WirePoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < WirePoints.Length; i++)
            {
                WirePoints[i].gameObject.SetActive(false);
            }
        }        
    }

    public void CheckAllAttachmentPointsMarked()
    {
        if (currentStep != AttachmentPointAction.Marked) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Marked);
        if (stepComplete)
        {
            currentStep = AttachmentPointAction.Scrubbed;
            thisAlien.CompleteTask(MinigameManager.Minigames.VEPScrub);
            audioSource.PlayOneShot(pointsMarkedAudioClip);
        }
    }

    public void AddAttachmentControllerToList(WireAttachmentController controller)
    {
        attachmentControllers.Add(controller);
    }

    public void CheckAllAttachmentPointsScrubbed()
    {
        if (currentStep != AttachmentPointAction.Scrubbed) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Scrubbed);
        if (stepComplete)
        {
            currentStep = AttachmentPointAction.Wired;
            thisAlien.CompleteTask(MinigameManager.Minigames.PlaceVEPWires);
            audioSource.PlayOneShot(pointsScrubbedAudioClip);
        }
    }

    public void CheckAllAttachmentPointsWired()
    {
        if (currentStep != AttachmentPointAction.Wired) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Wired);
        if (stepComplete)
        {
            ActivateWires();
            thisAlien.CompleteTask(MinigameManager.Minigames.None);
            audioSource.PlayOneShot(wiresAttachedAudioClip, 0.2f);
            StartCoroutine(DisableAllVEPComponentsAfterXSeconds(3));
        }
    }


    private bool CheckAllAttachmentPointsFor(AttachmentPointAction VEPAction)
    {
        int VEPListLength = 0;
        switch (VEPAction)
        {
            case AttachmentPointAction.Scrubbed:
                VEPListLength = ScrubPoints.Length;
                break;
            case AttachmentPointAction.Wired:
                VEPListLength = WirePoints.Length;
                break;
            case AttachmentPointAction.Marked:
                VEPListLength = MarkerPoints.Length;
                break;
            default:
                Debug.LogError($"Couldn't find attachmentPointAction list in {gameObject.name}");
                break;
        }

        for (int i = 0; i < VEPListLength; i++)
        {
            switch (VEPAction)
            {
                case AttachmentPointAction.Scrubbed:
                    if (!ScrubPoints[i].isScrubbed) return false;
                    break;
                case AttachmentPointAction.Wired:
                    if (!WirePoints[i].isWired) return false;
                    break;
                case AttachmentPointAction.Marked:
                    if (!MarkerPoints[i].isMarked) return false;
                    break;
                default:
                    break;
            }
        }
        Debug.Log(gameObject.name + "All " + VEPAction + " steps have been completed!" );
        return true;
    }

    private void ActivateWires()
    {
        foreach (WireAttachmentController controller in attachmentControllers)
        {
            controller.SetLightToActive();
        }
    }


    IEnumerator DisableAllVEPComponentsAfterXSeconds(float secs)
    {
        yield return new WaitForSeconds(secs);
        DisableAllVEPComponents();
    }
}
