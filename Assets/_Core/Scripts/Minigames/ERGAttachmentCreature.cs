﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(Alien))]
public class ERGAttachmentCreature : MonoBehaviour
{
    [SerializeField] private ScrubBehaviour[] ScrubPoints;
    [SerializeField] private EyeStickerBehaviour[] EyeStickerPoints;
    [SerializeField] private WireBehaviour[] WirePoints;
    [SerializeField] private AudioClip pointsScrubbedAudioClip;
    [SerializeField] private AudioClip wiresAttachedAudioClip;
    [SerializeField] private AudioClip stickersAttachedAudioClip;

    private Alien thisAlien;
    private List<WireAttachmentController> attachmentControllers;
    private AudioSource audioSource;

    private enum AttachmentPointAction
    {
        Scrubbed,
        Wired,
        Stickered
    }
    AttachmentPointAction currentStep;

    private void OnEnable()
    {
        attachmentControllers = new List<WireAttachmentController>();
        audioSource = gameObject.GetComponent<AudioSource>();
        currentStep = AttachmentPointAction.Scrubbed;
        thisAlien = GetComponent<Alien>();

        TouchScreenBehaviour.OnLayoutTransition += TouchScreenBehaviour_OnLayoutTransition;
    }

    private void OnDisable()
    {
        TouchScreenBehaviour.OnLayoutTransition -= TouchScreenBehaviour_OnLayoutTransition;
    }

    private void TouchScreenBehaviour_OnLayoutTransition(TouchScreenBehaviour.Layout newLayout)
    {
        if (thisAlien.GetMinigameManager().GetCurrentTestingAlien() != thisAlien) return;

        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.ERGScrubInstructions:
                for (int i = 0; i < ScrubPoints.Length; i++)
                {
                    ScrubPoints[i].gameObject.SetActive(true);
                }
                break;
            case TouchScreenBehaviour.Layout.ERGWireInstructions:
                for (int i = 0; i < ScrubPoints.Length; i++)
                {
                    ScrubPoints[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < WirePoints.Length; i++)
                {
                    WirePoints[i].gameObject.SetActive(true);
                }
                break;
            case TouchScreenBehaviour.Layout.ERGElectrolyteInstructions:
                for (int i = 0; i < WirePoints.Length; i++)
                {
                    WirePoints[i].gameObject.SetActive(true);
                }
                for (int i = 0; i < EyeStickerPoints.Length; i++)
                {
                    EyeStickerPoints[i].gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
    }

    public void DisableAllERGComponents()
    {
        if (ScrubPoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < ScrubPoints.Length; i++)
            {
                ScrubPoints[i].gameObject.SetActive(false);
            }
        }

        if (WirePoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < WirePoints.Length; i++)
            {
                WirePoints[i].gameObject.SetActive(false);
            }
        }

        if (EyeStickerPoints[0].gameObject.activeSelf)
        {
            for (int i = 0; i < EyeStickerPoints.Length; i++)
            {
                EyeStickerPoints[i].gameObject.SetActive(false);
            }
        }
    }

    public void AddAttachmentControllerToList(WireAttachmentController controller)
    {
        attachmentControllers.Add(controller);
    }

    public void CheckAllAttachmentPointsScrubbed()
    {
        if (currentStep != AttachmentPointAction.Scrubbed) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Scrubbed);
        if (stepComplete)
        {
            audioSource.PlayOneShot(pointsScrubbedAudioClip);
            currentStep = AttachmentPointAction.Wired;
            thisAlien.CompleteTask(MinigameManager.Minigames.PlaceERGWires);
        }
    }

    public void CheckAllAttachmentPointsWired()
    {
        if (currentStep != AttachmentPointAction.Wired) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Wired);
        if (stepComplete)
        {
            audioSource.PlayOneShot(wiresAttachedAudioClip, 0.2f);
            ActivateWires();
            currentStep = AttachmentPointAction.Stickered;
            thisAlien.CompleteTask(MinigameManager.Minigames.PlaceERGStickers);
        }
    }

    public void CheckAllEyeStickerPointsStickered()
    {
        if (currentStep != AttachmentPointAction.Stickered) return;

        bool stepComplete = CheckAllAttachmentPointsFor(AttachmentPointAction.Stickered);
        if (stepComplete)
        {
            audioSource.PlayOneShot(stickersAttachedAudioClip);
            thisAlien.CompleteTask(MinigameManager.Minigames.None);
            StartCoroutine(DisableAllERGComponentsAfterXSeconds(3));
        }
    }


    private bool CheckAllAttachmentPointsFor(AttachmentPointAction ERGAction)
    {
        int ERGListLength = 0;
        switch (ERGAction)
        {
            case AttachmentPointAction.Scrubbed:
                ERGListLength = ScrubPoints.Length;
                break;
            case AttachmentPointAction.Wired:
                ERGListLength = WirePoints.Length;
                break;
            case AttachmentPointAction.Stickered:
                ERGListLength = EyeStickerPoints.Length;
                break;
            default:
                Debug.LogError($"Couldn't find attachementPointAction list in {gameObject.name}");
                break;
        }

        for (int i = 0; i < ERGListLength; i++)
        {
            switch (ERGAction)
            {
                case AttachmentPointAction.Scrubbed:
                    if (!ScrubPoints[i].isScrubbed) return false;
                    break;
                case AttachmentPointAction.Wired:
                    if (!WirePoints[i].isWired) return false;
                    break;
                case AttachmentPointAction.Stickered:
                    if (!EyeStickerPoints[i].isStickered) return false;
                    break;
                default:
                    break;
            }
        }
        Debug.Log(gameObject.name + "All " + ERGAction + " steps have been completed!" );
        return true;
    }

    private void ActivateWires()
    {
        foreach (WireAttachmentController controller in attachmentControllers)
        {
            controller.SetLightToActive();
        }
    }

    IEnumerator DisableAllERGComponentsAfterXSeconds(float secs)
    {
        yield return new WaitForSeconds(secs);
        DisableAllERGComponents();
    }
}
