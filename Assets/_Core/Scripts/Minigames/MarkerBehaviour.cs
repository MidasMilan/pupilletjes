﻿using UnityEngine;
using Valve.VR;

public class MarkerBehaviour : MonoBehaviour
{
    [SerializeField] private RandomSound randomSound;
    public bool isMarked = false;

    private VEPAttachmentCreature parentCreature;
    private MeshRenderer markerMeshRenderer;
    private void Start()
    {
        if (GetComponentInParent<VEPAttachmentCreature>() == null)
        {
            Debug.LogError(gameObject.name + " couldn't find VEPAttachmentCreature script in parent! Attachment point will not contribute to minigame completion!");
        }
        randomSound = GetComponent<RandomSound>();
        parentCreature = GetComponentInParent<VEPAttachmentCreature>();
        markerMeshRenderer = gameObject.GetComponent<MeshRenderer>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MarkerTool"))
        {
            if (!isMarked)
            {
                SetMarkerToActive(true);
                isMarked = true;
                parentCreature.CheckAllAttachmentPointsMarked();
                randomSound.PlayRandomSound();
            }
        }
    }

    private void SetMarkerToActive(bool active = true)
    {
        markerMeshRenderer.material.SetInt("_IsActive", active ? 1 : 0);
    }
}
