﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(WireAttachmentController))]
public class WireBehaviour : MonoBehaviour
{
    [SerializeField] private AudioClip attachSound;
    [SerializeField] private WireAttachmentController wireAttachmentController;
    [SerializeField] private MeshRenderer wireMeshRenderer;
    [SerializeField] private ERGWireBehaviour wireLinePointer;
    [SerializeField] private Material disableHighlightMaterial;
    public bool isWired = false;

    private ERGAttachmentCreature ERGCreaturePointer;
    private VEPAttachmentCreature VEPCreaturePointer;
    private AudioSource audioSource;

    private void Start()
    {
        if (GetComponentInParent<ERGAttachmentCreature>() == null)
        {
            Debug.LogError(gameObject.name + " couldn't find ERGAttachmentCreature script in parent! Attachment point will not contribute to minigame completion!");
        }
        ERGCreaturePointer = GetComponentInParent<ERGAttachmentCreature>();
        VEPCreaturePointer = GetComponentInParent<VEPAttachmentCreature>();
        audioSource = ERGCreaturePointer.GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WireTool"))
        {
            if (!isWired)
            {
                isWired = true;
                audioSource.PlayOneShot(attachSound);
                SetWireToActive(false);
                if (VEPCreaturePointer.enabled)
                {
                    VEPCreaturePointer.AddAttachmentControllerToList(wireAttachmentController);
                    VEPCreaturePointer.CheckAllAttachmentPointsWired();
                }
                if (ERGCreaturePointer.enabled)
                {
                    ERGCreaturePointer.AddAttachmentControllerToList(wireAttachmentController);
                    ERGCreaturePointer.CheckAllAttachmentPointsWired();
                }

                WireData wireData = other.GetComponent<WireToolBehaviour>().PlaceWire();
                wireLinePointer.SetEndConnection(wireData.WireColor, wireData.WireBaseTransform);
                wireLinePointer.gameObject.SetActive(true);
            }
        }
    }

    private void SetWireToActive(bool active = true)
    {
        List<Material> m = new List<Material>();
        wireMeshRenderer.GetMaterials(m);
        m.RemoveAt(0);
        wireMeshRenderer.materials = m.ToArray();

        wireMeshRenderer.material.SetInt("_IsActive", active ? 1 : 0);
    }
}
