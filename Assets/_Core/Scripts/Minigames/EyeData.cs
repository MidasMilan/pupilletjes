﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class EyeData : MonoBehaviour
{
    [NonSerialized] public bool isEyeDropApplied;
    [SerializeField] private float blinkDuration;
    [SerializeField] private Animator creatureAnimator;
    [SerializeField] private string blinkAnimTriggerName;

    private EyeDropCreature parentCreature;

    private void Start()
    {
        GetComponent<Rigidbody>().isKinematic = true;

        if(GetComponentInParent<EyeDropCreature>() == null)
        {
            Debug.LogError(gameObject.name + " couldn't find EyeDropCreature script in parent! Eye will not contribute to minigame completion!");
        }
        parentCreature = GetComponentInParent<EyeDropCreature>();

        bool animatorHasParamName = false;
        foreach (AnimatorControllerParameter animParam in creatureAnimator.parameters)
        {
            if(animParam.name == blinkAnimTriggerName)
            {
                animatorHasParamName = true;
                break;
            }
        }
        if(!animatorHasParamName)
        {
            Debug.LogError(gameObject.name + " couldn't find blinkAnimTriggerName in creatureAnimator parameters!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("EyeDrop"))
        {
            if (parentCreature == null) return;

            isEyeDropApplied = true;
            other.GetComponent<EyeDropBehaviour>().LandEyeDrop();
            parentCreature.checkAllEyesApplied();
            creatureAnimator.SetTrigger(blinkAnimTriggerName);
        }
    }

    private void Update()
    {
        //TODO: find fix for eyeballs going out of sockets when passing through terrain
        if (transform.localPosition.x != 0.0f) transform.localPosition = Vector3.zero;
    }
}
