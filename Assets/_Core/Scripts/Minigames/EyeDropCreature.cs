﻿using UnityEngine;

[RequireComponent(typeof(Alien))]
public class EyeDropCreature : MonoBehaviour
{
    [SerializeField] private EyeData[] eyes;
    private Alien thisAlien;
    private bool AllEyesApplied;

    private void Start()
    {
        thisAlien = GetComponent<Alien>();
        AllEyesApplied = false;
    }

    public void checkAllEyesApplied()
    {
        if (AllEyesApplied) return;

        for (int i = 0; i < eyes.Length; i++)
        {
            if (eyes[i] == null)
            {
                Debug.LogWarning("EyeData is not applied. Ignore for minigame completion. Is list to long? Or is eye not applied?");
                continue;
            }

            if (!eyes[i].isEyeDropApplied)
            {
                return;
            }
        }
        AllEyesApplied = true;
        if(enabled) thisAlien.CompleteTask(MinigameManager.Minigames.None);
    }

    public void GetTicklish()
    {
        //print("haha that tickles!!!!");
    }
}
