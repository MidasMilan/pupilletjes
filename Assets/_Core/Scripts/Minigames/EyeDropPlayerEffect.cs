﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EyeDropPlayerEffect : MonoBehaviour
{
    [SerializeField] private float effectDuration = 3.0f;
    [SerializeField] private float blinkDuration = 0.12f;
    [SerializeField] private RectTransform canvasRectPointer;
    [SerializeField] private Image lowerEyeLid;
    [SerializeField] private Image upperEyeLid;
    [SerializeField] private Material blurFeatureMat;
    [SerializeField] [Range(0, 8)] private int startBlurPasses;

    [SerializeField] private GameManager gameManager;

    private float effectCounter;
    private float blurPassReduceInterval;
    private float canvasInvertedHeight;

    void Start()
    {
        canvasInvertedHeight = -canvasRectPointer.GetComponent<RectTransform>().rect.height;
        lowerEyeLid.rectTransform.anchoredPosition = new Vector3(0.0f, canvasInvertedHeight, 0.0f);
        upperEyeLid.rectTransform.anchoredPosition = new Vector3(0.0f, 0.0f, 0.0f);
        effectCounter = 0.0f;
        if (blurFeatureMat.GetInt("_BlurPasses") != 0) blurFeatureMat.SetInt("_BlurPasses", 0);
    }

    void Update()
    {
        if (effectCounter > 0.0f)
        {
            effectCounter -= Time.deltaTime;
            if (effectCounter < 0.0f) effectCounter = 0.0f;
            int currentBlurPasses = blurFeatureMat.GetInt("_BlurPasses");
            if (effectCounter + Time.deltaTime > (blinkDuration * 0.5f) &&
                effectCounter <= (blinkDuration * 0.5f))
            {
                StartCoroutine(blink(blinkDuration, true));
            }
            else if (effectCounter <= (currentBlurPasses - 1) * blurPassReduceInterval)
            {
                blurFeatureMat.SetInt("_BlurPasses", currentBlurPasses - 1);
            }
        }
    }

    public void StartEffect()
    {
        effectCounter = effectDuration;
        blurPassReduceInterval = effectDuration / startBlurPasses;
        blurFeatureMat.SetInt("_BlurPasses", startBlurPasses);

        StartCoroutine(blink(blinkDuration, false));
    }
    IEnumerator blink(float duration, bool isEndEffectBlink, float holdCloseDuration = 0)
    {
        float time = 0;
        float lowerCurrentPos = 0.0f;
        float upperCurrentPos = 0.0f;
        float halfDuration = duration * 0.5f;
        bool lidsHaveClosed = false;

        while (time < duration + holdCloseDuration)
        {
            // Move eyelids to close gradually
            if (time < halfDuration)
            {
                lowerCurrentPos = Mathf.Lerp(canvasInvertedHeight, 0.0f, time / halfDuration);
                upperCurrentPos = Mathf.Lerp(0.0f, canvasInvertedHeight, time / halfDuration);
                lowerEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, lowerCurrentPos);
                upperEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, upperCurrentPos);
            }
            // Close the eyelids for the holdCloseDuration
            else if (time > halfDuration && time < halfDuration + holdCloseDuration)
            {
                lowerCurrentPos = Mathf.Lerp(canvasInvertedHeight, 0.0f, 0.0f);
                upperCurrentPos = Mathf.Lerp(0.0f, canvasInvertedHeight, 0.0f);
            }
            // Open the eyelids again
            else
            {
                if (!lidsHaveClosed && isEndEffectBlink)
                {
                    lidsHaveClosed = true;
                    effectCounter = 0.0f;
                    blurFeatureMat.SetInt("_BlurPasses", 0);
                }
                lowerCurrentPos = Mathf.Lerp(0.0f, canvasInvertedHeight, (time - holdCloseDuration - halfDuration) / halfDuration);
                upperCurrentPos = Mathf.Lerp(canvasInvertedHeight, 0.0f, (time - holdCloseDuration - halfDuration) / halfDuration);
                lowerEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, lowerCurrentPos);
                upperEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, upperCurrentPos);
            }

            time += Time.deltaTime;
            yield return null;
        }

        lowerEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, canvasInvertedHeight);
        upperEyeLid.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
    }

    public void startBlink(float holdCloseDuration = 0)
    {
        StartCoroutine(blink(1.0f, false, holdCloseDuration));
    }
}
