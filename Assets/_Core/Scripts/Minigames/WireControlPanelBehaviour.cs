﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireControlPanelBehaviour : MonoBehaviour
{    
    [SerializeField] private Color[] wireColors;
    [Header("PUT THESE IN SAME ORDER AS LIST ABOVE")]
    [SerializeField] private Transform[] wireBases;
    private WireData[] wires;
    private List<WireData> notYetWired;

    private void OnEnable()
    {
        MinigameManager.OnMinigameCompleted += OnMinigameComplete;
    }
    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= OnMinigameComplete;
    }

    private void OnMinigameComplete(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        if (nextTask == MinigameManager.Minigames.PlaceERGWires || nextTask == MinigameManager.Minigames.PlaceVEPWires)
        {
            if (notYetWired == null)
            {
                InitializeLists();
            }
        }
    }

    private void InitializeLists()
    {
        notYetWired = new List<WireData>();
        wires = new WireData[wireBases.Length];
        for (int i = 0; i < wireBases.Length; i++)
        {
            wires[i] = new WireData(wireBases[i], wireColors[i] != null ? wireColors[i] : Color.magenta);
        }
        RefreshNotYetWiredList();
    }

    private void Start()
    {
        if (notYetWired == null)
        {
            InitializeLists();
        }
    }

    public WireData GetRandomWire()
    {
        if (notYetWired.Count <= 0) { Debug.LogError("No wire attachement points left"); return wires[wires.Length-1]; }

        int randomColorID = Random.Range(0, notYetWired.Count);
        if (randomColorID >= notYetWired.Count) randomColorID = notYetWired.Count - 1;
        WireData assignedWireData = notYetWired[randomColorID];
        return assignedWireData;
    }

    public void TryAttachWire(WireData toRemove)
    {
        int removeID = notYetWired.Count;
        
        for (int i = 0; i < notYetWired.Count; i++)
        {
            if (notYetWired[i].WireColor == toRemove.WireColor) removeID = i;
        }

        if(removeID != notYetWired.Count) notYetWired.RemoveAt(removeID);
    }

    public void TryAddWire(WireData value)
    {
        if(!notYetWired.Contains(value)) notYetWired.Add(value);
    }

    private void RefreshNotYetWiredList()
    {
        notYetWired.Clear();
        notYetWired.AddRange(wires);
    }
}
public struct WireData
{
    public Transform WireBaseTransform;
    public Color WireColor;

    public WireData(Transform WireBaseTransform, Color WireColor)
    {
        this.WireBaseTransform = WireBaseTransform;
        this.WireColor = WireColor;
    }
}

