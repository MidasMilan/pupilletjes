﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ScrubTubBehaviour : MonoBehaviour
{
    private AudioSource thisAudioSource;

    private void Start()
    {
        thisAudioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ScrubTool")
        {
            ScrubToolBehaviour scrubToolBehaviour = other.GetComponent<ScrubToolBehaviour>();
            if (!scrubToolBehaviour.GetScrubApplied())
            {
                thisAudioSource.Play();
                scrubToolBehaviour.SetScrubApplied(true);
            }
        }
    }
}
