﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
public class EyeStickerBehaviour : MonoBehaviour
{
    [SerializeField] private RandomSound randomSound;
    [SerializeField] private Material disableHighlightMaterial;
    public bool isStickered = false;

    private ERGAttachmentCreature parentCreature;
    private MeshRenderer stickerMeshRenderer;
    private void Start()
    {
        if (GetComponentInParent<ERGAttachmentCreature>() == null)
        {
            Debug.LogError(gameObject.name + " couldn't find ERGAttachmentCreature script in parent! Attachment point will not contribute to minigame completion!");
        }
        randomSound = GetComponent<RandomSound>();
        parentCreature = GetComponentInParent<ERGAttachmentCreature>();
        stickerMeshRenderer = gameObject.GetComponent<MeshRenderer>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("StickerTool"))
        {
            if (!isStickered)
            {
                randomSound.PlayRandomSound();
                SetStickerToActive(false);
                isStickered = true;
                parentCreature.CheckAllEyeStickerPointsStickered();
            }
        }
    }

    private void SetStickerToActive(bool active = true)
    {
        List<Material> m = new List<Material>();
        stickerMeshRenderer.GetMaterials(m);
        m.RemoveAt(0);
        stickerMeshRenderer.materials = m.ToArray();

        stickerMeshRenderer.material = disableHighlightMaterial;
        stickerMeshRenderer.material.SetInt("_IsActive", active ? 1 : 0);
    }
}
