﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.Newtonsoft.Json.Bson;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(AudioSource))]
public class EyeDropBehaviour : MonoBehaviour
{
    [SerializeField] private MeshRenderer thisMeshRenderer;

    private ParticleSystem thisParticleSys;
    private Rigidbody thisRB;
    private Collider thisCollider;
    private AudioSource thisAudioSource;


    private void Start()
    {
        thisParticleSys = GetComponent<ParticleSystem>();
        thisRB = GetComponent<Rigidbody>();
        thisCollider = GetComponent<Collider>();
        thisAudioSource = GetComponent<AudioSource>();

        thisAudioSource.pitch = Random.Range(0.9f, 1.1f);

        if (thisMeshRenderer == null)
        {
            Debug.LogError(gameObject.name + " meshRenderer not set in EyeDropBehaviour script! Will cause visual bugs!");
            if(GetComponentInChildren<MeshRenderer>() != null)
            {
                thisMeshRenderer = GetComponentInChildren<MeshRenderer>();
                Debug.LogError("found alternative MeshRenderer on: " + thisMeshRenderer.gameObject.name);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Creature"))
        {
            collision.gameObject.GetComponent<EyeDropCreature>().GetTicklish();
        }
        else if (collision.gameObject.CompareTag("VRHead"))
        {
            float dot = Vector3.Dot(thisRB.velocity.normalized, collision.transform.forward);
            if (dot < -0.5f)
            {
                collision.gameObject.GetComponent<EyeDropPlayerEffect>().StartEffect();
            }
        }

        transform.position = collision.GetContact(0).point;
        LandEyeDrop();
    }

    public void LandEyeDrop()
    {
        thisAudioSource.Play();
        thisCollider.enabled = false;
        thisMeshRenderer.enabled = false;
        thisRB.isKinematic = true;
        //gameobject should be destroyed after particle system is done (is in stopaction of particlesystem component)
        thisParticleSys.Play();
    }
}
