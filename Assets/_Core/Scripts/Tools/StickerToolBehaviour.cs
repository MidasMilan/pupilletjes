﻿using UnityEngine;

public class StickerToolBehaviour : MonoBehaviour
{
    public void ActivateTool(bool value)
    {
        gameObject.SetActive(value);
    }
}
