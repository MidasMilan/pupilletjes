﻿using UnityEngine;
using Valve.VR;

public class RetinomaxBehaviour : MonoBehaviour
{
    [SerializeField] private SteamVR_Action_Boolean triggerAction;
    [SerializeField] private SteamVR_Input_Sources handType;

    public void ActivateTool(bool value)
    {
        if (value)
        {
            triggerAction.AddOnStateDownListener(triggerDown, handType);
            triggerAction.AddOnStateUpListener(triggerUp, handType);
            gameObject.SetActive(true);
        }
        else
        {
            triggerAction.RemoveOnStateDownListener(triggerDown, handType);
            triggerAction.RemoveOnStateUpListener(triggerUp, handType);
            gameObject.SetActive(false);
        }
    }

    private void triggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        TryStartScanning();
    }

    private void triggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        StopScanning();
    }

    private void TryStartScanning()
    {
        gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.red;
    }

    private void StopScanning()
    {
        gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
    }
}
