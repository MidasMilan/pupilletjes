﻿using System.Collections;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(InteractableHoverEvents))]
[RequireComponent(typeof(TextMeshPro))]
public class MenuToolBehaviour : MonoBehaviour
{
    [Range(1.0f, 3.0f)][SerializeField] private float hoverScale = 1.3f;
    [SerializeField] private float onHoverAnimDuration = 0.25f;
    [SerializeField] private float onExitHoverAnimDuration = 0.25f;
    [SerializeField] private Color baseBackgroundColor;
    [SerializeField] private Color highlightPulseColor;
    [SerializeField] private Color onHoverColor;
    [SerializeField] private float highlightPulseSpeed;
    [SerializeField] private ToolWheelController.Tools thisToolType = ToolWheelController.Tools.Hand;
    [SerializeField] private AudioSource toolWheelAudioSource;
    [SerializeField] private RandomSound randomSound;
    [SerializeField] private float toolNameSoundVolume = 1.0f;
    [SerializeField] private SpriteRenderer toolBackground;
    [SerializeField] private RectTransform nameBackground;

    [SerializeField] private bool debugMode = false;

    private Vector3 nameEndPos;
    private Vector3 nameEndScale;
    private bool highlighted;
    private float highlightCounter;
    private bool hovered;

    public ToolWheelController.Tools GetToolType()
    {
        return thisToolType;
    }

    private void OnEnable()
    {
        ToolWheelController.CloseToolWheelEvent += OnToolWheelClose;
    }

    private void OnDisable()
    {
        ToolWheelController.CloseToolWheelEvent -= OnToolWheelClose;
    }

    private void OnToolWheelClose(ToolWheelController TWController)
    {
        if (debugMode) print($"{gameObject.name} on toolwheel close");
        SetEquipped(false, true);
    }

    void Start()
    {
        if (nameBackground == null)
        {
            Debug.LogError("nameBackground is not assigned!" + gameObject.name);
        }

        toolBackground.color = baseBackgroundColor;
        highlighted = false;
        hovered = false;
        nameEndPos = nameBackground.localPosition;
        nameEndScale = nameBackground.localScale;
        nameBackground.localPosition = new Vector3();
        nameBackground.localScale = new Vector3();
        randomSound = GetComponent<RandomSound>();
    }

    private void Update()
    {
        if(highlighted)
        {
            highlightCounter += highlightPulseSpeed * Time.deltaTime;
            if(highlightCounter >= 1 || highlightCounter <= 0)
            {
                highlightPulseSpeed *= -1;
                highlightCounter = Mathf.Clamp(highlightCounter, 0.0f, 1.0f);
            }
            toolBackground.color = Color.Lerp(baseBackgroundColor, highlightPulseColor, highlightCounter);
        }
    }

    public void SetHighlighted(bool value)
    {
        highlighted = value;
        if (!highlighted)
        {
            toolBackground.color = baseBackgroundColor;
        }
    }

    public void SetEquipped(bool value, bool instantTransition)
    {
        if (debugMode) print($"{gameObject.name} setting equipped: {value}");
        if (value)
        {
            if (instantTransition)
            {
                toolBackground.color = onHoverColor;
                nameBackground.localScale = nameEndScale;
                nameBackground.localPosition = nameEndPos;
                transform.localScale = new Vector3(hoverScale, hoverScale, hoverScale);
            }
            else
            {
                StartCoroutine(onHoverAnim(onHoverAnimDuration));
            }
        }
        else if (gameObject.activeSelf)
        {
            if (instantTransition)
            {
                toolBackground.color = baseBackgroundColor;
                nameBackground.localScale = new Vector3();
                nameBackground.localPosition = new Vector3();
                transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
            else
            {
                StartCoroutine(exitHoverAnim(onExitHoverAnimDuration));
            }
        }
    }

    public void OnFingerEnter()
    {
        if (debugMode) print($"OnFingerEnter: {gameObject.name}");
        if (!hovered)
        {
            StartCoroutine(onHoverAnim(onHoverAnimDuration));
            toolWheelAudioSource.Play();
            randomSound.PlayRandomSound(toolNameSoundVolume);
            hovered = true;
        }
    }

    public void OnFingerExit()
    {
        if (debugMode) print($"OnFingerExit: {gameObject.name}");
        if (hovered)
        {
            StartCoroutine(exitHoverAnim(onExitHoverAnimDuration));
            hovered = false;
        }
    }

    private IEnumerator onHoverAnim(float duration)
    {
        float time = 0.0f;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = new Vector3(hoverScale, hoverScale, hoverScale);
        Vector3 currentScale = startScale;

        while (time < duration)
        {
            float lerpValue = Mathf.Sin((time / duration) * Mathf.PI * 0.5f);
            currentScale = Vector3.Lerp(startScale, endScale, lerpValue);
            transform.localScale = currentScale;
            nameBackground.localScale = Vector3.Lerp(new Vector3(), nameEndScale, lerpValue);
            nameBackground.localPosition = Vector3.Lerp(new Vector3(), nameEndPos, lerpValue);
            toolBackground.color = Color.Lerp(baseBackgroundColor, onHoverColor, lerpValue);

            time += Time.deltaTime;
            yield return null;
        }

        toolBackground.color = onHoverColor;
        nameBackground.localScale = nameEndScale;
        nameBackground.localPosition = nameEndPos;
        transform.localScale = endScale;
    }
    private IEnumerator exitHoverAnim(float duration)
    {
        float time = 0.0f;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = new Vector3(1.0f, 1.0f, 1.0f);
        Vector3 currentScale = startScale;
        Color startColor = toolBackground.color;

        while (time < duration)
        {
            float lerpValue = Mathf.Sin((time / duration) * Mathf.PI * 0.5f);
            currentScale = Vector3.Lerp(startScale, endScale, lerpValue);
            transform.localScale = currentScale;
            nameBackground.localScale = Vector3.Lerp(nameEndScale, new Vector3(), lerpValue);
            nameBackground.localPosition = Vector3.Lerp(nameEndPos, new Vector3(), lerpValue);
            toolBackground.color = Color.Lerp(startColor, baseBackgroundColor, lerpValue);

            time += Time.deltaTime;
            yield return null;
        }

        toolBackground.color = baseBackgroundColor;
        nameBackground.localScale = new Vector3();
        nameBackground.localPosition = new Vector3();
        transform.localScale = endScale;
    }
}
