﻿using System.Collections;
using System.Linq;
using UnityEngine;

//MAKE SURE DOBBER IS TAGGED WITH:"Dobber"!
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(AudioSource))]
public class DobberBehaviour : MonoBehaviour
{
    [SerializeField] private Transform DobberOnRodTarget;
    [SerializeField] private Transform EndPointBendyRod;
    [SerializeField] private Transform[] BendyRodLinePoints;
    [SerializeField] private float dobbingSpeed;
    [SerializeField] private float dobbingAmplitude;
    [SerializeField] private float lerpToRodDuration;
    [SerializeField] private AudioClip castDobberSound;
    [SerializeField] private AudioClip landDobberSound;
    [SerializeField] private AudioClip retrieveDobberSound;
    [SerializeField] private Collider dobberOnRodCollider;

    [SerializeField] private IRISVoiceBehaviour IRISVoicePointer;
    [SerializeField] private int maxTutorials;

    [SerializeField] private Dialogue[] inWaterDialogues;
    [SerializeField] private Dialogue[] onHookDialogues;
    [SerializeField] private Dialogue[] placeOnTableDialogues;
    private int completeTutorialCounter;

    private float dobbingTargetY;

    private Rigidbody thisRB;
    private Collider collisionCollider;
    private LineRenderer thisLineRenderer;
    private AudioSource thisAudioSource;

    public delegate void OnHookedHandler();
    public static event OnHookedHandler OnHooked;
    public static event OnHookedHandler OnUnhooked;

    [SerializeField] private bool debugMode = false;

    private Alien targetedBy;
    public void SetDobberTarget(Alien target) { 
        targetedBy = target;

        if (target == null)
        {
            OnUnhooked.Invoke();
        } else
        {
            OnHooked.Invoke();
            IRISVoicePointer.AddRandomVoiceLineToQueue(onHookDialogues);
        }
    }

    public Alien GetDobberTarget() { return targetedBy; }
    public Collider GetDobberOnRodCollider() { return dobberOnRodCollider; }
    public Rigidbody GetRigidbody() { return thisRB; }

    public enum DobberState
    {
        OnRod,
        BeingThrown,
        InWater,
        BeingPulledIn,
        Idle
    }
    private DobberState currentState = new DobberState();
    public DobberState GetDobberState()
    {
        return currentState;
    }

    void Start()
    {
        thisRB = GetComponent<Rigidbody>();
        collisionCollider = GetComponent<CapsuleCollider>();
        thisLineRenderer = GetComponent<LineRenderer>();
        thisAudioSource = GetComponent<AudioSource>();

        currentState = DobberState.OnRod;
        thisRB.isKinematic = true;
        collisionCollider.enabled = false;
        targetedBy = null;
        completeTutorialCounter = 0;

        gameObject.SetActive(false);// added quickly before deadline
    }

    void Update()
    {
        //For testing purposes
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(currentState == DobberState.OnRod)
            {
                CastDobber(new Vector3());
            }
            else
            {
                thisRB.isKinematic = true;
                collisionCollider.enabled = false;
                StartCoroutine(LerpToRod(lerpToRodDuration));
            }
        }

        switch (currentState)
        {
            case DobberState.OnRod:
                transform.position = DobberOnRodTarget.position;
                transform.LookAt(EndPointBendyRod, Vector3.up);
                transform.Rotate(new Vector3(1, 0, 0), 90);
                break;
            case DobberState.InWater:
                transform.position = new Vector3(transform.position.x,
                                                 dobbingTargetY + (Mathf.Sin(Time.time * dobbingSpeed) * dobbingAmplitude),
                                                 transform.position.z);
                break;
            case DobberState.BeingPulledIn:
                DobberOnRodTarget.position = EndPointBendyRod.position;
                transform.LookAt(EndPointBendyRod, Vector3.up);
                transform.Rotate(new Vector3(1, 0, 0), 90);
                break;
            default:
                break;
        }
    }

    public void OnEquip()
    {
        gameObject.SetActive(true);
        DobberOnRodTarget.position = EndPointBendyRod.position;
        transform.position = DobberOnRodTarget.position;
        thisRB.velocity = Vector3.zero;
        if (debugMode) print("DobberState changed to: OnRod");
        currentState = DobberState.OnRod;
        gameObject.tag = "Dobber";
    }

    public void Despawn()
    {
        StopAllCoroutines();
        if (targetedBy != null)
        {
            //targetedBy.LetGoOfHook(); //TODO: implement in alien script
            targetedBy = null;
        }
        thisRB.isKinematic = true;
        collisionCollider.enabled = false;
        transform.position = DobberOnRodTarget.position;
        currentState = DobberState.OnRod;
        gameObject.SetActive(false);
    }

    private void LateUpdate()
    {
        Vector3[] linePoints = new Vector3[BendyRodLinePoints.Length + 2];
        for (int i = 0; i < BendyRodLinePoints.Length; i++)
        {
            linePoints[i] = BendyRodLinePoints[i].position;
        }
        linePoints[BendyRodLinePoints.Length] = EndPointBendyRod.position;
        linePoints[BendyRodLinePoints.Length + 1] = transform.position;

        thisLineRenderer.SetPositions(linePoints);
    }

    public void CastDobber(Vector3 force)
    {
        if (debugMode) print("DobberState changed to: BeingThrown");
        currentState = DobberState.BeingThrown;
        if(targetedBy != null)
        {
            //targetedBy.LetGoOfHook(); //TODO: implement in alien script
            targetedBy = null;
        }
        transform.position = EndPointBendyRod.position;
        thisRB.isKinematic = false;
        thisRB.AddForce(force);
        collisionCollider.enabled = true;
        thisAudioSource.PlayOneShot(castDobberSound);
    }
    public void CastDobber(Vector3 direction, float force)
    {
        CastDobber(direction * force);
    }

    public void LandDobber(float landPos)
    {
        if (debugMode) print("DobberState changed to: InWater");
        dobbingTargetY = landPos;
        currentState = DobberState.InWater;
        thisRB.velocity = new Vector3();
        thisRB.isKinematic = true;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        thisAudioSource.PlayOneShot(landDobberSound);
        IRISVoicePointer.AddRandomVoiceLineToQueue(inWaterDialogues);
    }

    public void RetrieveDobber(float retrieveArcHeight, float retrieveDuration, Transform target = null)
    {
        if (debugMode) print("DobberState changed to: BeingPulledIn");
        currentState = DobberState.BeingPulledIn;
        if (target == null) target = DobberOnRodTarget;
        StartCoroutine(LerpToTargetWithArcPos(target, retrieveArcHeight, retrieveDuration));
        thisAudioSource.PlayOneShot(retrieveDobberSound);
    }

    //Used to move the bobber back to the player in an arc
    IEnumerator LerpToTargetWithArcPos(Transform target, float arcHeight, float duration)
    {
        float time = 0;
        Vector3 startPos = transform.position;
        Vector3 currentPos = transform.position;
        float currentYPos = transform.position.y;
        float halfDuration = duration * 0.5f;

        while (time < duration)
        {
            currentPos = Vector3.Lerp(startPos, target.position, time / duration);
            if (time < halfDuration)
            {
                currentYPos = Mathf.Lerp(startPos.y, startPos.y + arcHeight, Mathf.Sin(time / halfDuration * Mathf.PI * 0.5f));
            }
            else
            {
                currentYPos = Mathf.Lerp(target.position.y, startPos.y + arcHeight, Mathf.Cos((time - halfDuration) / halfDuration * Mathf.PI * 0.5f));
            }
            transform.position = new Vector3(currentPos.x, currentYPos, currentPos.z);
            time += Time.deltaTime;
            yield return null;
        }

        transform.position = target.position;
        if (debugMode) print("DobberState changed to: Idle");
        currentState = DobberState.Idle;

        thisRB.isKinematic = true;
        collisionCollider.enabled = false;
        StartCoroutine(LerpToRod(lerpToRodDuration));
    }

    IEnumerator LerpToRod(float duration)
    {
        currentState = DobberState.Idle;
        float time = 0;
        Vector3 startPos = transform.position;
        Vector3 currentPos = transform.position;
        float currentYPos = transform.position.y;
        float halfDuration = duration * 0.5f;

        while (time < duration)
        {
            currentPos = Vector3.Lerp(startPos, DobberOnRodTarget.position, time / duration);
            transform.position = currentPos;
            time += Time.deltaTime;
            yield return null;
        }

        transform.position = DobberOnRodTarget.position;
        if (debugMode) print("DobberState changed to: OnRod");
        currentState = DobberState.OnRod;
        if (targetedBy != null && completeTutorialCounter < maxTutorials)
        {
            completeTutorialCounter++;
            IRISVoicePointer.AddRandomVoiceLineToQueue(placeOnTableDialogues);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Water") || 
            collision.gameObject.CompareTag("Tool") || 
            collision.gameObject.CompareTag("Creature")) return;

        if (currentState != DobberState.InWater) currentState = DobberState.Idle;
    }
}
