﻿using UnityEngine;

public class Floater : MonoBehaviour
{
    [SerializeField] private bool floating = true;
    [SerializeField] private float floatSpeed = 1.0f;
    [SerializeField] private Vector3 floatAmplitude = new Vector3(0.0f, 0.0f, 0.0f);
    [SerializeField] private Transform floatingTarget;
    [SerializeField] private Vector3 floatOffset = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 startPoint;
    private Vector3 destination;
    private float floatCounter;
    private float halfFloatSpeed;

    private void Start()
    {
        startPoint = transform.position;
        destination = startPoint + floatAmplitude;
        floatCounter = 0.0f;
        halfFloatSpeed = floatSpeed / 2;
    }


    void Update()
    {
        if (floating)
        {
            floatCounter += Time.deltaTime;
            if(floatCounter < halfFloatSpeed)
            {
                transform.position = Vector3.Lerp(floatingTarget.position + floatOffset, floatingTarget.position + floatOffset + floatAmplitude, floatCounter / halfFloatSpeed);
            }
            else
            {
                transform.position = Vector3.Lerp(floatingTarget.position + floatOffset + floatAmplitude, floatingTarget.position + floatOffset, (floatCounter - halfFloatSpeed) / halfFloatSpeed);
            }

            if (floatCounter >= floatSpeed) floatCounter = 0.0f;
        }
    }
}
