﻿using UnityEngine;

public class ERGWireBehaviour : MonoBehaviour
{
    [SerializeField] private Transform[] LineTransforms;
    [SerializeField] private LineRenderer thisLR;

    public void SetEndConnection(Color color, Transform ConnectionPoint)
    {
        thisLR.material.color = color;
        AddLineTransform(ConnectionPoint);
    }

    private void Update()
    {
        for (int i = 0; i < LineTransforms.Length; i++)
        {
            thisLR.SetPosition(i, LineTransforms[i].position);
        }
    }

    private void AddLineTransform(Transform LinePoint)
    {
        thisLR.positionCount = LineTransforms.Length + 1;
        Transform[] newLines = new Transform[LineTransforms.Length + 1];
        for (int i = 0; i < newLines.Length; i++)
        {
            newLines[i] = (i+1 == newLines.Length)?LinePoint:LineTransforms[i];
        }
        LineTransforms = newLines;
    }
}
