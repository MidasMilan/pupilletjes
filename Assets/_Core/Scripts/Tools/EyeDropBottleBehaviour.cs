﻿using System;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(Animator))]
public class EyeDropBottleBehaviour : MonoBehaviour
{ 
    [SerializeField] private SteamVR_Action_Boolean triggerAction;
    [SerializeField] private SteamVR_Input_Sources handType;
    [SerializeField] private GameObject eyeDropPrefab;

    [SerializeField] private Transform eyeDropSpawnLocation;
    [SerializeField] private Animator animController;
    [SerializeField] private SkinnedMeshRenderer liquidRendererPointer;
    [SerializeField] private float wobbleAmplifier = 100.0f;
    [SerializeField] [Range(0.0f, 10.0f)] private float maxWobble = 2.0f;

    private Material liquidMat;
    private Vector3 previousPos;

    public void Start()
    {
        liquidMat = liquidRendererPointer.materials[0].name.StartsWith("Liquid") ? liquidRendererPointer.materials[0] : liquidRendererPointer.materials[1];
        if(!liquidMat.name.StartsWith("Liquid"))
        {
            Debug.LogError("Couldn't find liquid material in: " + gameObject.name);
        }
    }

    private void OnEnable()
    {
        triggerAction.AddOnStateDownListener(TriggerDown, handType);
        triggerAction.AddOnStateUpListener(TriggerUp, handType);
    }

    private void OnDisable()
    {
        triggerAction.RemoveOnStateDownListener(TriggerDown, handType);
        triggerAction.RemoveOnStateUpListener(TriggerUp, handType);
    }

    public void ActivateBottle(bool value)
    {
        if (value)
        {
            previousPos = transform.position;
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        squeezeBottle();
    }

    private void TriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        animController.SetBool("PinchingBottle", false);
    }

    private void squeezeBottle()
    {
        Instantiate(eyeDropPrefab, eyeDropSpawnLocation.position, new Quaternion());
        animController.SetBool("PinchingBottle", true);
    }

    private void Update()
    {
        Vector3 moveDir = (transform.position - previousPos) * wobbleAmplifier * Time.deltaTime;
        moveDir = new Vector3(Mathf.Clamp(moveDir.x, -maxWobble, maxWobble),
                              0.0f,
                              Mathf.Clamp(moveDir.z, -maxWobble, maxWobble));
        liquidMat.SetFloat("_WobbleX", moveDir.x);
        liquidMat.SetFloat("_WobbleZ", moveDir.z);

        previousPos = transform.position;
    }
}
