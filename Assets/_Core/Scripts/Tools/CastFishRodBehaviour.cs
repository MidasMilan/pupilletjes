﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastFishRodBehaviour : MonoBehaviour
{
    [SerializeField] private float minFlingSpeedToCast;
    [SerializeField] private float minThrowSpeed;
    [SerializeField] private float maxThrowSpeed;
    [SerializeField] private float minFlingSpeedToRetrieve;
    [SerializeField] private float retrieveArcHeight;
    [SerializeField] private float retrieveDuration;
    [SerializeField] private int minHeadAndVelAngle;

    [SerializeField] private Transform playerHead;
    [SerializeField] private DobberBehaviour dobberBehaviour;
    
    [SerializeField] private Rigidbody bendyRodRB;

    [SerializeField] private bool debugMode = false;

    void Update()
    {
        DobberBehaviour.DobberState currentDobberState = dobberBehaviour.GetDobberState();

        bool swingingFront = Vector3.Angle(playerHead.forward, bendyRodRB.velocity) <= minHeadAndVelAngle;
        if (swingingFront)
        {
            if (currentDobberState == DobberBehaviour.DobberState.OnRod &&
                bendyRodRB.velocity.magnitude >= minFlingSpeedToCast)
            {
                Vector3 throwVelocity = bendyRodRB.velocity;
                if (throwVelocity.magnitude < minThrowSpeed)
                {
                    throwVelocity = throwVelocity.normalized * minThrowSpeed;
                }
                else if (throwVelocity.magnitude > maxThrowSpeed)
                {
                    throwVelocity = throwVelocity.normalized * maxThrowSpeed;
                }

                if (debugMode) 
                {
                    print("player forward: " + playerHead.forward + ", vel angle: " + Vector3.Angle(playerHead.forward, bendyRodRB.velocity));
                    print("bendy rod vel: " + bendyRodRB.velocity.magnitude + ", minFling: " + minFlingSpeedToCast); 
                }
                dobberBehaviour.CastDobber(throwVelocity); 
            }
        }
        else
        {
            if (currentDobberState != DobberBehaviour.DobberState.BeingThrown &&
                currentDobberState != DobberBehaviour.DobberState.BeingPulledIn &&
                currentDobberState != DobberBehaviour.DobberState.OnRod &&
                bendyRodRB.velocity.magnitude >= minFlingSpeedToRetrieve)
            {
                if (debugMode)
                {
                    print("Dobber state on pull in rod: " + currentDobberState);
                    print("player forward: " + playerHead.forward + ", vel angle: " + Vector3.Angle(playerHead.forward, bendyRodRB.velocity));
                    print("bendy rod vel: " + bendyRodRB.velocity.magnitude + ", minFling: " + minFlingSpeedToCast);
                }
                dobberBehaviour.RetrieveDobber(retrieveArcHeight, retrieveDuration);
            }
        }
    }
}
