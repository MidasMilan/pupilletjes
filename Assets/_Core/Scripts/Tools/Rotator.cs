﻿﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    [SerializeField] private bool rotating = true;
    [SerializeField] private Vector3 rotationSpeeds = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 startRotation;

    private void Start()
    {
        startRotation = rotationSpeeds;
    }


    void Update()
    {
        if (rotating)
        {
            transform.Rotate(rotationSpeeds * Time.deltaTime);
        }
	}
    public void SetRotating(bool active)
    {
        rotating = active;
    }

    public void SetRotation(float x, float y, float z, float timeTillTargetRotation = 0.0f)
    {
        Vector3 targetRot = new Vector3(x, y, z);
        if (timeTillTargetRotation == 0.0f)
        {
            rotationSpeeds = targetRot;
        }
        else
        {
            StartCoroutine(LerpToRotation(targetRot, timeTillTargetRotation));
        }
    }

    public void ResetToStartRotation(float timeTillTargetRotation = 0.0f)
    {
        SetRotation(startRotation.x, startRotation.y, startRotation.z, timeTillTargetRotation);
    }

    IEnumerator LerpToRotation(Vector3 targetRotation, float duration)
    {
        float time = 0;
        Vector3 startRotation = rotationSpeeds;

        while (time < duration)
        {
            rotationSpeeds = Vector3.Lerp(startRotation, targetRotation, Mathf.Sin(time / duration * Mathf.PI * 0.5f));
            time += Time.deltaTime;
            yield return null;
        }
        rotationSpeeds = targetRotation;
    }
}
