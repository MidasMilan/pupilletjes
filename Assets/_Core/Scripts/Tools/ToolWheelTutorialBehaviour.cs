﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolWheelTutorialBehaviour : MonoBehaviour
{
    [Header("Select this tool dialogues")]
    [SerializeField] private Dialogue eyeStickerSelect;
    [SerializeField] private Dialogue scrubSelect;
    [SerializeField] private Dialogue markerSelect;
    [SerializeField] private Dialogue eyeDropSelect;
    [SerializeField] private Dialogue wireSelect;

    [Header("tool explanation dialogues")]
    [SerializeField] private Dialogue eyeStickerExplanation;
    [SerializeField] private Dialogue scrubExplanation;
    [SerializeField] private Dialogue markerExplanation;
    [SerializeField] private Dialogue eyeDropExplanation;
    [SerializeField] private Dialogue wireExplanation;
    [SerializeField] private Dialogue fishingRodExplanation;

    [Header("Pointers")]
    [SerializeField] private IRISVoiceBehaviour IRISVoicePointer;

    private List<ToolWheelController.Tools> explainedTools;

    private void OnEnable()
    {
        explainedTools = new List<ToolWheelController.Tools>();

        TouchScreenBehaviour.OnLayoutTransition += TouchScreenBehaviour_OnLayoutTransition;
        ToolWheelController.CloseToolWheelEvent += ToolWheelController_CloseToolWheelEvent;
    }

    private void OnDisable()
    {
        TouchScreenBehaviour.OnLayoutTransition -= TouchScreenBehaviour_OnLayoutTransition;
        ToolWheelController.CloseToolWheelEvent -= ToolWheelController_CloseToolWheelEvent;
    }

    private void TouchScreenBehaviour_OnLayoutTransition(TouchScreenBehaviour.Layout newLayout)
    {
        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.EyeDropInstructions:
                IRISVoicePointer.AddVoicelineToQueue(eyeDropSelect);
                break;
            case TouchScreenBehaviour.Layout.ERGScrubInstructions:
                IRISVoicePointer.AddVoicelineToQueue(scrubSelect);
                break;
            case TouchScreenBehaviour.Layout.ERGWireInstructions:
                IRISVoicePointer.AddVoicelineToQueue(wireSelect);
                break;
            case TouchScreenBehaviour.Layout.ERGElectrolyteInstructions:
                IRISVoicePointer.AddVoicelineToQueue(eyeStickerSelect);
                break;
            case TouchScreenBehaviour.Layout.VEPMarkerInstructions:
                IRISVoicePointer.AddVoicelineToQueue(markerSelect);
                break;
            case TouchScreenBehaviour.Layout.VEPScrubInstructions:
                IRISVoicePointer.AddVoicelineToQueue(scrubSelect);
                break;
            case TouchScreenBehaviour.Layout.VEPWireInstructions:
                IRISVoicePointer.AddVoicelineToQueue(wireSelect);
                break;
            default:
                Debug.LogError("Don't recognize layout, can't add a equip tool voice line to queue");
                break;
        }
    }

    private void ToolWheelController_CloseToolWheelEvent(ToolWheelController TWController)
    {
        ToolWheelController.Tools currentTool = TWController.GetCurrentToolType();
        if (!explainedTools.Contains(currentTool))
        {
            print("giving tip from toolwheel tutorial");
            switch (currentTool)
            {
                case ToolWheelController.Tools.Hand:
                    break;
                case ToolWheelController.Tools.FishingRod:
                    IRISVoicePointer.AddVoicelineToQueue(fishingRodExplanation);
                    break;
                case ToolWheelController.Tools.EyeDropBottle:
                    IRISVoicePointer.AddVoicelineToQueue(eyeDropExplanation);
                    break;
                case ToolWheelController.Tools.ScrubTool:
                    IRISVoicePointer.AddVoicelineToQueue(scrubExplanation);
                    break;
                case ToolWheelController.Tools.WireTool:
                    IRISVoicePointer.AddVoicelineToQueue(wireExplanation);
                    break;
                case ToolWheelController.Tools.StickerTool:
                    IRISVoicePointer.AddVoicelineToQueue(eyeStickerExplanation);
                    break;
                case ToolWheelController.Tools.MarkerTool:
                    IRISVoicePointer.AddVoicelineToQueue(markerExplanation);
                    break;
                default:
                    Debug.LogError("Don't recognize tool, can't add a tool explanation voice line to queue");
                    break;
            }
            explainedTools.Add(currentTool);
        }
    }
}
