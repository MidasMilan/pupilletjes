﻿using UnityEngine;
using System.Collections.Generic;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Collider))]
public class ToolWheelBehaviour : MonoBehaviour
{
    [SerializeField] private MenuToolBehaviour[] toolIconBehaviourPointers;
    private Dictionary<ToolWheelController.Tools, MenuToolBehaviour> toolIcons;


    [SerializeField] private ToolWheelController toolWheelControllerPointer;

    [SerializeField] private float toolToFingerTriggerDist;

    [SerializeField] private bool debugMode = false;

    private Collider thisCol;
    private ToolWheelController.Tools highlightedTool;
    private ToolWheelController.Tools hoveringTool;
    private ToolWheelController.Tools previousHoverTool;
    private string fingerColliderTag;

    // Start is called before the first frame update
    void Start()
    {
        highlightedTool = ToolWheelController.Tools.None;
        hoveringTool = ToolWheelController.Tools.None;
        previousHoverTool = ToolWheelController.Tools.None;
        toolIcons = new Dictionary<ToolWheelController.Tools, MenuToolBehaviour>();
        thisCol = GetComponent<Collider>();
        thisCol.enabled = false;

        fingerColliderTag = toolWheelControllerPointer.GetHandPointer().handType == Valve.VR.SteamVR_Input_Sources.LeftHand ? "IndexFingerR" : "IndexFingerL";
        if (debugMode) print($"{gameObject.name}, finger collider tag: {fingerColliderTag}");

        if (toolWheelControllerPointer == null)
        {
            Debug.LogError(gameObject.name + " toolWheelControllerPointer not set in MenuToolBehaviour script! Will cause major bugs!!!!!");
        }

        for (int i = 0; i < toolIconBehaviourPointers.Length; i++)
        {
            if (toolIcons.ContainsKey(toolIconBehaviourPointers[i].GetToolType()))
            {
                Debug.LogError($"{gameObject.name} contains multiple tools of tooltype: {toolIconBehaviourPointers[i].GetToolType()}");
            }
            else
            {
                toolIcons.Add(toolIconBehaviourPointers[i].GetToolType(), toolIconBehaviourPointers[i]);
            }
        }

        foreach (KeyValuePair<ToolWheelController.Tools, MenuToolBehaviour> toolIcon in toolIcons)
        {
            if (toolIcon.Key == ToolWheelController.Tools.FishingRod ||
                toolIcon.Key == ToolWheelController.Tools.Hand)
            {
                continue;
            }
            toolIcon.Value.gameObject.SetActive(false);
        }

        if (debugMode)
        {
            print($"{gameObject.name} spawned and initialized icon dict of size: {toolIcons.Count}");
            foreach (ToolWheelController.Tools toolType in toolIcons.Keys)
            {
                print($"{toolType}, on gameObject: {toolIcons[toolType].gameObject.name}");
            }
        }

        HighlightTool(ToolWheelController.Tools.FishingRod);

        transform.localScale = new Vector3();
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        ToolWheelController.OpenToolWheelEvent += OnOpenToolWheel;
        ToolWheelController.ActivateToolWheelEvent += OnToolWheelActive;
        ToolWheelController.CloseToolWheelEvent += OnToolWheelClose;
    }

    private void OnDisable()
    {
        ToolWheelController.OpenToolWheelEvent -= OnOpenToolWheel;
        ToolWheelController.ActivateToolWheelEvent -= OnToolWheelActive;
        ToolWheelController.CloseToolWheelEvent -= OnToolWheelClose;
    }

    private void ForceRemoveAllVisualHovers(ToolWheelController.Tools equipped, ToolWheelController.Tools exception = ToolWheelController.Tools.None)
    {
        if (debugMode) print($"starting force remove EXCEPT: {exception}");

        foreach (ToolWheelController.Tools toolType in toolIcons.Keys)
        {
            MenuToolBehaviour toolI = toolIcons[toolType];
            if (exception != toolType)
            {
                toolI.SetEquipped(false, toolI == toolIcons[equipped] ? false : true);
            }
        }
    }

    private void OnOpenToolWheel(ToolWheelController TWController)
    {
        if (TWController == toolWheelControllerPointer) return;

        toolIcons[TWController.GetCurrentToolType()].SetEquipped(true, false);

        if (debugMode) print($"ToolWheelBehaviour opened. equippped tool: {TWController.GetCurrentToolType()}");
        hoveringTool = ToolWheelController.Tools.None;
        previousHoverTool = ToolWheelController.Tools.None;
    }

    private void OnToolWheelActive(ToolWheelController TWController)
    {
        if (TWController != toolWheelControllerPointer) return;
        thisCol.enabled = true;
    }

    private void OnToolWheelClose(ToolWheelController TWController)
    {
        if (TWController != toolWheelControllerPointer) return;
        thisCol.enabled = false;
    }

    public void UpdateHoveringTool(ToolWheelController.Tools hoverTool)
    {
        if (debugMode) print($"UPDATING HOVERING TOOL! previous: {previousHoverTool} from: {hoveringTool}, to: {hoverTool}");
        
        if (hoveringTool != toolWheelControllerPointer.GetCurrentToolType() && previousHoverTool == ToolWheelController.Tools.None)
        {
            ForceRemoveAllVisualHovers(toolWheelControllerPointer.GetCurrentToolType(), hoverTool);
        }

        if (hoveringTool != hoverTool)
        {
            toolWheelControllerPointer.SetHoverHandTool(hoverTool);
            previousHoverTool = hoveringTool;
            hoveringTool = hoverTool;
        }
    }

    public void CheckExitHover(ToolWheelController.Tools exitHoverTool)
    {
        if (debugMode) print($"EXITING HOVER TOOL! previous: {previousHoverTool} current: {hoveringTool}, exiting: {exitHoverTool}");
        if (exitHoverTool == previousHoverTool)
        {
            previousHoverTool = ToolWheelController.Tools.None;
        }
        else if (exitHoverTool == hoveringTool)
        {
            UpdateHoveringTool(previousHoverTool);
        }
    }

    public void HighlightTool(ToolWheelController.Tools toolToLight)
    {
        if (debugMode) print($"highlighted tool: {highlightedTool}, tool to light: {toolToLight}");

        if (highlightedTool != ToolWheelController.Tools.None)
        {
            if (debugMode) print($"highlighted tool is not none");
            MenuToolBehaviour highlightedBehaviour = toolIcons[highlightedTool];
            highlightedBehaviour.SetHighlighted(false);
        }

        MenuToolBehaviour toolBehaviour = toolIcons[toolToLight];
        toolBehaviour.gameObject.SetActive(true);
        toolBehaviour.SetHighlighted(true);
        highlightedTool = toolToLight;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag(fingerColliderTag))
        {
            //if (debugMode) Debug.LogWarning($"{gameObject.name}, START onTriggerStay. hoveringTool: {hoveringTool}, fingerColliderTag: {fingerColliderTag}");

            MenuToolBehaviour closestTool = null;
            float closestToolDist = 100.0f;
            for (int i = 0; i < toolIconBehaviourPointers.Length; i++)
            {
                MenuToolBehaviour toolI = toolIconBehaviourPointers[i];
                if (!toolI.gameObject.activeSelf) continue;

                float toolDist = (toolI.transform.position - other.transform.position).magnitude;
                if (toolDist < closestToolDist) 
                { 
                    closestTool = toolI;
                    closestToolDist = toolDist;
                }
            }

            //if (debugMode) print($"{gameObject.name}, Closest tool: {closestTool.gameObject.name}, distance to finger: {closestToolDist}, hand name: {other.GetComponentInParent<HandCollider>().gameObject.name}");

            if (closestTool.GetToolType() == hoveringTool && closestToolDist > toolToFingerTriggerDist)
            {
                CheckExitHover(closestTool.GetToolType());
                closestTool.OnFingerExit();
            }
            else if (closestTool.GetToolType() != hoveringTool && closestToolDist <= toolToFingerTriggerDist)
            {
                UpdateHoveringTool(closestTool.GetToolType());
                closestTool.OnFingerEnter();
            }

            //if (debugMode) Debug.LogWarning($"{gameObject.name}, END onTriggerStay. hoveringTool: {hoveringTool}");
        }
    }
}
