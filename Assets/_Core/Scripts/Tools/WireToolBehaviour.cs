﻿using UnityEngine;

public class WireToolBehaviour : MonoBehaviour
{
    [SerializeField] private WireControlPanelBehaviour wirePanelPointer;

    [SerializeField] private LineRenderer wireRenderer;
    [SerializeField] private Transform wireConnectionTrans;
    private Vector3 wireBasePos;
    private WireData currentWire;

    private void OnEnable()
    {
        MinigameManager.OnMinigameCompleted += OnWireComplete;
    }

    private void OnDisable()
    {
        MinigameManager.OnMinigameCompleted -= OnWireComplete;
    }

    private void OnWireComplete(Alien helpedAlien, MinigameManager.Minigames nextTask)
    {
        EnableToolWireRenderer(nextTask == MinigameManager.Minigames.PlaceERGWires || nextTask == MinigameManager.Minigames.PlaceVEPWires);
    }

    public void ActivateTool(bool value)
    {
        gameObject.SetActive(value);
        EnableToolWireRenderer(wirePanelPointer.gameObject.activeSelf);
    }

    private void EnableToolWireRenderer(bool value)
    {
        wireRenderer.enabled = value;
        wireRenderer.positionCount = 2;
        if(value)
        {
            SetNewWire();
            wirePanelPointer.TryAddWire(currentWire);
        } 
    }

    private void Update()
    {
        wireRenderer.SetPosition(0, wireConnectionTrans.position);
        wireRenderer.SetPosition(1, wireBasePos);
    }

    public WireData PlaceWire()
    {
        wirePanelPointer.TryAttachWire(currentWire);
        WireData value = currentWire;
        SetNewWire();
        return value;
    }

    private void SetNewWire()
    {
        currentWire = wirePanelPointer.GetRandomWire();
        wireBasePos = currentWire.WireBaseTransform.position;
        wireRenderer.material.color = currentWire.WireColor;
    }
}
