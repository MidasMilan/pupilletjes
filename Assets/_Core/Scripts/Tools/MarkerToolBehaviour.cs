﻿using UnityEngine;

public class MarkerToolBehaviour : MonoBehaviour
{
    public void ActivateTool(bool value)
    {
        gameObject.SetActive(value);
    }
}
