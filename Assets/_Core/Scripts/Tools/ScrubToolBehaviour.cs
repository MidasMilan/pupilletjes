﻿using UnityEngine;
using Valve.VR;

public class ScrubToolBehaviour : MonoBehaviour
{
    [SerializeField] GameObject scrubSap;
    private bool hasScrubApplied;

    public void ActivateTool(bool value)
    {
        hasScrubApplied = false;
        SetScrubSap();
        gameObject.SetActive(value);
    }

    public void SetScrubApplied(bool value)
    {
        hasScrubApplied = value;
        SetScrubSap();
    }

    public bool GetScrubApplied()
    {
        return hasScrubApplied;
    }

    private void SetScrubSap()
    {
        if (scrubSap) scrubSap.SetActive(hasScrubApplied);
    }
}
