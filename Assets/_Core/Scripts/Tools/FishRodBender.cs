﻿using UnityEngine;

public class FishRodBender : MonoBehaviour
{
    [SerializeField] private Transform[] bendyRodBones;
    [SerializeField] private Transform PivotPointBendyRod;
    [SerializeField] private Transform EndPointBendyRod;
    [SerializeField] private Transform TargetEndPointBendyRod;
    [SerializeField] private Transform[] LineTransforms;


    [SerializeField] private bool DebugMode = false;

    private void OnDrawGizmosSelected()
    {
        if (!DebugMode) return;

        //calulates all points between pivot and target point, then smooths these points slowly towards the bendy end point
        for (int i = 0; i < bendyRodBones.Length+1; i++)
        {
            float lerp = (float)i / bendyRodBones.Length;
            Vector3 forwardPos = Vector3.Lerp(PivotPointBendyRod.position, TargetEndPointBendyRod.position, lerp);
            Vector3 forwardBendyPos = Vector3.Lerp(PivotPointBendyRod.position, EndPointBendyRod.position, lerp);
            Vector3 targetToBendy = Vector3.Lerp(forwardBendyPos, forwardPos, Mathf.Cos(lerp * Mathf.PI * 0.5f));
            print("point: " + i + ", lerp: "  + lerp + ", pos: " + targetToBendy);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(targetToBendy, 0.1f);
        }
    }

    private void Update()
    {
        //Force handle rotation because it keeps pointing towards bendy end instead of target end
        Quaternion handleRotation = PivotPointBendyRod.rotation * Quaternion.Euler(-90.0f, 180.0f, 0.0f);
        bendyRodBones[0].SetPositionAndRotation(PivotPointBendyRod.position, handleRotation);

        //calulates all points after handle between pivot and target, then smooths these points slowly towards the bendy end point
        for (int i = 1; i < bendyRodBones.Length; i++)
        {
            float lerp = (float)i / (bendyRodBones.Length - 1);
            Vector3 forwardPos = Vector3.Lerp(PivotPointBendyRod.position, TargetEndPointBendyRod.position, lerp);
            Vector3 forwardBendyPos = Vector3.Lerp(PivotPointBendyRod.position, EndPointBendyRod.position, lerp);
            bendyRodBones[i].position = Vector3.Lerp(forwardBendyPos, forwardPos, Mathf.Cos(lerp * Mathf.PI * 0.5f));   
        }

        SetLineTransforms();
    }

    //Hardcoded, I am sorry Vera
    private void SetLineTransforms()
    {
        LineTransforms[0].position = bendyRodBones[3].position + bendyRodBones[3].forward * -0.045f;
        LineTransforms[1].position = bendyRodBones[5].position + bendyRodBones[5].forward * -0.035f;
        LineTransforms[2].position = bendyRodBones[7].position + bendyRodBones[7].forward * -0.027f;
    }
}
