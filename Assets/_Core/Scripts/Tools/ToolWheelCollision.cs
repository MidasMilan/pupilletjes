﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ToolWheelCollision : MonoBehaviour
{
    [SerializeField] private float tempCollOffDuration = 0.06f;
    [SerializeField] private bool isLeftTW;

    [SerializeField] private bool debugMode = false;
    private Collider thisColl;

    private void OnEnable()
    {
        if (thisColl == null) thisColl = GetComponent<Collider>();
        thisColl.enabled = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isLeftTW ? collision.gameObject.CompareTag("LeftHand") : collision.gameObject.CompareTag("RightHand"))
        {
            return;
        }
        
        Vector3 toSource = collision.transform.position - transform.position;
        float angle = Vector3.Angle(toSource, transform.forward);
        if (angle < 90)
        {
            if (thisColl.enabled)
            {
                thisColl.enabled = false;
                StartCoroutine(tempCollOff());
            }
        }

        if (debugMode) print($"{gameObject.name}, on collision enter tag: {collision.gameObject.tag}, forward:{transform.forward}, normal: {collision.GetContact(0).normal}, angle: {angle}");
    }

    IEnumerator tempCollOff()
    {
        float time = 0.0f;

        while (time < tempCollOffDuration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        thisColl.enabled = true;
    }
}
