﻿using System.Collections;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Hand = Valve.VR.InteractionSystem.Hand;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class ToolWheelController : MonoBehaviour
{
    [Header("ATTRIBUTES")]
    [SerializeField] private bool canBeOpened;
    [SerializeField] private float totalTimeOpenWheelLength;
    [SerializeField] private float openAnimDuration;
    [SerializeField] private float overshootScale = 1.3f;
    [Range(1, 99)][SerializeField] private int scaleOvershootDurationPercentage = 10;
    [SerializeField] private float closeAnimDuration;
    [SerializeField] private SteamVR_Action_Boolean touchPadClickAction;
    [SerializeField] private SteamVR_Action_Boolean selectToolAction;
    [SerializeField] private Tools startEquippedTool;
    [SerializeField] private float toolWheelHandOffset = 0.2f;
    [SerializeField] private bool debugMode = false;

    [Header("ASSIGN FROM SCENE")]
    [SerializeField] private DobberBehaviour dobberPointer;

    [Header("ASSIGN FROM PREFAB")]
    [SerializeField] private AudioClip openToolWheelSound;
    [SerializeField] private AudioClip closeToolWheelSound;
    [SerializeField] private AudioClip selectToolSound;
    [SerializeField] private AudioSource thisAudioSource;

    [SerializeField] private ToolWheelBehaviour thisTWPointer;
    [SerializeField] private ToolWheelController otherTWControllerPointer;
    [SerializeField] private Transform VRCameraPointer;

    [SerializeField] private Transform toolWheelSpawnPoint;
    [SerializeField] private Image toolWheelOpenBar;

    [SerializeField] private GameObject fishingRodPointer;
    [SerializeField] private GameObject bendyRodPointer;
    [SerializeField] private ConfigurableJoint bendyRodJoint1Pointer;
    [SerializeField] private ConfigurableJoint bendyRodJoint2Pointer;
    [SerializeField] private Rigidbody bendyRodRBPointer;
    [SerializeField] private SpringJoint dobberOnRodJointPointer;
    [SerializeField] private Rigidbody rigidRodRBPointer;
    [SerializeField] private EyeDropBottleBehaviour eyeDropBottlePointer;
    [SerializeField] private RetinomaxBehaviour retinomaxPointer;
    [SerializeField] private ScrubToolBehaviour scrubToolPointer;
    [SerializeField] private WireToolBehaviour wireToolPointer;
    [SerializeField] private StickerToolBehaviour stickerToolPointer;
    [SerializeField] private MarkerToolBehaviour markerToolPointer;
    [SerializeField] private Hand handPointer;

    private HandCollider handCollider;
    private bool toolWheelButtonDown;
    private float openWheelCounter;
    private float openMenuOvershootDuration;

    public delegate void OpenToolWheelHandler(ToolWheelController TWController);
    public static event OpenToolWheelHandler OpenToolWheelEvent;
    public static event OpenToolWheelHandler CloseToolWheelEvent;
    public static event OpenToolWheelHandler ActivateToolWheelEvent;

    public enum Tools
    {
        Hand,
        Retinomax,
        FishingRod,
        EyeDropBottle,
        None,
        ScrubTool,
        WireTool,
        StickerTool,
        MarkerTool
    }
    private Tools currentlyEquippedTool;
    private Tools currentlyHoveredTool;

    public Tools GetCurrentToolType() { return currentlyEquippedTool; }

    private void OnEnable()
    {
        touchPadClickAction.AddOnStateDownListener(onTouchpadDown, handPointer.handType);
        touchPadClickAction.AddOnStateUpListener(onTouchpadUp, handPointer.handType);
        selectToolAction.AddOnStateDownListener(trySelectTool, handPointer.handType == SteamVR_Input_Sources.LeftHand?SteamVR_Input_Sources.RightHand:SteamVR_Input_Sources.LeftHand);

        OpenToolWheelEvent += CheckOpenTW;

        TouchScreenBehaviour.OnLayoutTransition += TouchScreenBehaviour_OnLayoutTransition;
    }

    private void OnDisable()
    {
        touchPadClickAction.RemoveOnStateDownListener(onTouchpadDown, handPointer.handType);
        touchPadClickAction.RemoveOnStateUpListener(onTouchpadUp, handPointer.handType);
        selectToolAction.RemoveOnStateDownListener(trySelectTool, handPointer.handType == SteamVR_Input_Sources.LeftHand ? SteamVR_Input_Sources.RightHand : SteamVR_Input_Sources.LeftHand);

        OpenToolWheelEvent -= CheckOpenTW;

        TouchScreenBehaviour.OnLayoutTransition -= TouchScreenBehaviour_OnLayoutTransition;
    }

    private void CheckOpenTW(ToolWheelController TWController)
    {
        EquipToolToHand(Tools.Hand, false, true);
    }

    private void TouchScreenBehaviour_OnLayoutTransition(TouchScreenBehaviour.Layout newLayout)
    {
        if (debugMode) print($"onLayout change: {newLayout}");
        switch (newLayout)
        {
            case TouchScreenBehaviour.Layout.MainMenu:
                thisTWPointer.HighlightTool(Tools.FishingRod);
                break;
            case TouchScreenBehaviour.Layout.EyeDropInstructions:
                thisTWPointer.HighlightTool(Tools.EyeDropBottle);
                break;
            case TouchScreenBehaviour.Layout.ERGScrubInstructions:
                thisTWPointer.HighlightTool(Tools.ScrubTool);
                break;
            case TouchScreenBehaviour.Layout.ERGWireInstructions:
                thisTWPointer.HighlightTool(Tools.WireTool);
                break;
            case TouchScreenBehaviour.Layout.ERGElectrolyteInstructions:
                thisTWPointer.HighlightTool(Tools.StickerTool);
                break;
            case TouchScreenBehaviour.Layout.VEPMarkerInstructions:
                thisTWPointer.HighlightTool(Tools.MarkerTool);
                break;
            case TouchScreenBehaviour.Layout.VEPScrubInstructions:
                thisTWPointer.HighlightTool(Tools.ScrubTool);
                break;
            case TouchScreenBehaviour.Layout.VEPWireInstructions:
                thisTWPointer.HighlightTool(Tools.WireTool);
                break;
            case TouchScreenBehaviour.Layout.MinigameComplete:
                thisTWPointer.HighlightTool(Tools.Hand);
                break;
            default:
                break;
        }
    }

    private void Start()
    {
        openMenuOvershootDuration = openAnimDuration / 100.0f * scaleOvershootDurationPercentage;
        toolWheelButtonDown = false;
        openWheelCounter = 0.0f;
        toolWheelOpenBar.enabled = false;
        
        EquipToolToHand(startEquippedTool);
    }

    public void setCanBeOpened(bool enabled) => canBeOpened = enabled;

    private void Update()
    {
        if (toolWheelButtonDown)
        {
            if (openWheelCounter < totalTimeOpenWheelLength)
            {
                openWheelCounter += Time.deltaTime;
                if (openWheelCounter >= totalTimeOpenWheelLength)
                {
                    toolWheelButtonDown = false;
                    toolWheelOpenBar.enabled = false;
                    openWheelCounter = 0.0f;
                    toolWheelOpenBar.fillAmount = 0.0f;
                    OpenWheel(toolWheelSpawnPoint.position);
                }
                else
                {
                    toolWheelOpenBar.fillAmount = openWheelCounter / totalTimeOpenWheelLength;
                }
            }
        }
        else if (openWheelCounter > 0.0f)
        {
            openWheelCounter -= Time.deltaTime;
            if (openWheelCounter <= 0.0f)
            {
                toolWheelOpenBar.enabled = false;
                openWheelCounter = 0.0f;
            }
            toolWheelOpenBar.fillAmount = openWheelCounter / totalTimeOpenWheelLength;
        }

        if (thisTWPointer.gameObject.activeSelf)
        {
            float rotateBy = handPointer.handType == SteamVR_Input_Sources.LeftHand ? -90.0f : 90.0f;
            thisTWPointer.transform.SetPositionAndRotation(transform.position + transform.right * toolWheelHandOffset, transform.rotation * Quaternion.AngleAxis(rotateBy, Vector3.forward));
        }

        if (debugMode)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                currentlyHoveredTool = Tools.FishingRod;
                CloseWheel();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                currentlyHoveredTool = Tools.FishingRod;
                CloseWheel();
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                currentlyHoveredTool = Tools.EyeDropBottle;
                CloseWheel();
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                currentlyHoveredTool = Tools.Hand;
                CloseWheel();
            }
        }
    }

    private void onTouchpadDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (!canBeOpened) return;

        if (totalTimeOpenWheelLength > 0.0f)
        {
            toolWheelButtonDown = true;
            toolWheelOpenBar.enabled = true;
        }
        else
        {
            OpenWheel(toolWheelSpawnPoint.position);
        }
    }

    private void onTouchpadUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (!canBeOpened) return;

        toolWheelButtonDown = false;
        if(thisTWPointer.gameObject.activeSelf) CloseWheel();
    }

    private void trySelectTool(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (!canBeOpened) return;

        if (fromSource != handPointer.handType && currentlyHoveredTool != Tools.None)
        {
            toolWheelButtonDown = false;
            CloseWheel();
        }
    }

    private void OpenWheel(Vector3 spawnLocation)
    {
        OpenToolWheelEvent?.Invoke(this);

        thisTWPointer.gameObject.SetActive(true);
        float rotateBy = handPointer.handType == SteamVR_Input_Sources.LeftHand ? -90.0f : 90.0f;
        thisTWPointer.transform.SetPositionAndRotation(transform.position - transform.up * toolWheelHandOffset, transform.rotation * Quaternion.AngleAxis(rotateBy, Vector3.forward));

        StopAllCoroutines();
        StartCoroutine(openMenuAnim(openAnimDuration));

        thisAudioSource.PlayOneShot(openToolWheelSound);

        handPointer.Hide();
    }

    private void CloseWheel()
    {
        Tools otherEquip = otherTWControllerPointer.CheckEquippingRequirements();
        //since there can only be 1 rod, equip rod in other hand should de-equip in current
        if (currentlyHoveredTool == Tools.FishingRod && otherEquip == Tools.FishingRod) otherTWControllerPointer.EquipToolToHand(Tools.Hand);
        Tools thisEquip = CheckEquippingRequirements();
        EnableGrab(thisEquip == Tools.Hand);

        StopAllCoroutines();
        StartCoroutine(closeMenuAnim(closeAnimDuration));
        thisAudioSource.PlayOneShot(closeToolWheelSound);

        CloseToolWheelEvent?.Invoke(this);

        handPointer.Show();
    }

    public Tools CheckEquippingRequirements()
    {
        if (currentlyHoveredTool != Tools.None)
        {
            EquipToolToHand(currentlyHoveredTool);
            thisAudioSource.PlayOneShot(selectToolSound);
        }
        else
        {
            EquipToolToHand(currentlyEquippedTool);
        }

        return currentlyEquippedTool;
    }

    public void SetHoverHandTool(Tools currentlyHoveringOver)
    {
        currentlyHoveredTool = currentlyHoveringOver;
        if (debugMode) print($"{gameObject.name}, Setting hover hand tool: {currentlyHoveredTool}");
    }

    public Tools GetHoverHandTool()
    {
        return currentlyHoveredTool;
    }

    public Hand GetHandPointer()
    {
        return handPointer;
    }

    private void EnableGrab(bool value)
    {
        if (value)
        {
            handPointer.grabGripAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabGrip");
        }
        else
        {
            handPointer.grabGripAction = null;
        }

        if (debugMode) print($"Enable grab {handPointer.handType}: {value}, grab grib action: {handPointer.grabGripAction}, other grib action: {otherTWControllerPointer.handPointer.grabGripAction}");
    }

    private void EquipToolToHand(Tools toolToEquip, bool saveAsCurrentTool = true, bool overrideGrabDisable = false)
    {
        if (debugMode) print($"{gameObject.name}, currently equipped tool: {currentlyEquippedTool}, equipping tool to hand: {toolToEquip}, saving as current tool: {saveAsCurrentTool}");
        //TODO: fishingrod equiping is a mess and probably causes lots of bugs (found and hidden). Either refactor entire fishingrod structure or make a fishingrod equiping script...
        if(toolToEquip != currentlyEquippedTool)
        {
            switch (currentlyEquippedTool)
            {
                case Tools.FishingRod:
                    fishingRodPointer.SetActive(false);
                    if (bendyRodJoint1Pointer.connectedBody == rigidRodRBPointer)
                    {
                        dobberOnRodJointPointer.gameObject.SetActive(true);
                        bendyRodPointer.SetActive(false);
                        dobberPointer.Despawn();
                    }
                    handPointer.DetachObject(fishingRodPointer);
                    break;
                case Tools.EyeDropBottle:
                    eyeDropBottlePointer.ActivateBottle(false);
                    handPointer.DetachObject(eyeDropBottlePointer.gameObject);
                    break;
                case Tools.Retinomax:
                    retinomaxPointer.ActivateTool(false);
                    handPointer.DetachObject(retinomaxPointer.gameObject);
                    break;
                case Tools.ScrubTool:
                    scrubToolPointer.ActivateTool(false);
                    handPointer.DetachObject(scrubToolPointer.gameObject);
                    break;
                case Tools.WireTool:
                    wireToolPointer.ActivateTool(false);
                    handPointer.DetachObject(wireToolPointer.gameObject);
                    break;
                case Tools.StickerTool:
                    stickerToolPointer.ActivateTool(false);
                    handPointer.DetachObject(stickerToolPointer.gameObject);
                    break;
                case Tools.MarkerTool:
                    markerToolPointer.ActivateTool(false);
                    handPointer.DetachObject(markerToolPointer.gameObject);
                    break;
                case Tools.Hand:
                    EnableGrab(false);
                    break;
                default:
                    break;
            }
        }
        switch (toolToEquip)
        {
            case Tools.FishingRod:
                //Can't assign at Start() because steamvr disables player gameobject.
                bendyRodPointer.SetActive(true);
                fishingRodPointer.SetActive(true);
                bendyRodJoint1Pointer.connectedBody = rigidRodRBPointer;
                bendyRodJoint2Pointer.connectedBody = rigidRodRBPointer;
                dobberOnRodJointPointer.connectedBody = bendyRodRBPointer;
                dobberOnRodJointPointer.transform.position = bendyRodJoint1Pointer.transform.position;
                dobberOnRodJointPointer.GetComponent<Rigidbody>().velocity = Vector3.zero;
                dobberOnRodJointPointer.gameObject.SetActive(true);
                bendyRodPointer.transform.SetPositionAndRotation(rigidRodRBPointer.transform.position, rigidRodRBPointer.transform.rotation);
                rigidRodRBPointer.velocity = Vector3.zero;
                dobberPointer.OnEquip();

                handPointer.AttachObject(fishingRodPointer, GrabTypes.Scripted);
                break;
            case Tools.EyeDropBottle:
                eyeDropBottlePointer.ActivateBottle(true);
                handPointer.AttachObject(eyeDropBottlePointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.Retinomax:
                retinomaxPointer.ActivateTool(true);
                handPointer.AttachObject(retinomaxPointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.ScrubTool:
                scrubToolPointer.ActivateTool(true);
                handPointer.AttachObject(scrubToolPointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.WireTool:
                wireToolPointer.ActivateTool(true);
                handPointer.AttachObject(wireToolPointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.StickerTool:
                stickerToolPointer.ActivateTool(true);
                handPointer.AttachObject(stickerToolPointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.MarkerTool:
                markerToolPointer.ActivateTool(true);
                handPointer.AttachObject(markerToolPointer.gameObject, GrabTypes.Scripted);
                break;
            case Tools.Hand:
                if (handPointer.skeleton != null) handPointer.skeleton.BlendToSkeleton();
                EnableGrab(overrideGrabDisable?false:true);
                if (handCollider == null) handCollider = handPointer.GetComponent<HandPhysics>().handCollider;
                handCollider.SetCollisionDetectionEnabled(true);
                break;
            default:
                break;
        }
        
        currentlyEquippedTool = saveAsCurrentTool?toolToEquip:currentlyEquippedTool;
        currentlyHoveredTool = Tools.None;
    }

    IEnumerator openMenuAnim(float totalDuration)
    {
        float time = 0.0f;
        Vector3 menuScale = new Vector3();
        Vector3 overshoot = new Vector3(overshootScale, overshootScale, overshootScale);
        float openTillOvershootDuration = totalDuration - openMenuOvershootDuration;
        float toOvershootDuration = openMenuOvershootDuration / 2;

        while (time < totalDuration)
        {
            if(time < openTillOvershootDuration)//linear till start overshoot
            {
                menuScale = Vector3.Lerp(new Vector3(), new Vector3(1.0f, 1.0f, 1.0f), time / openTillOvershootDuration);
            }
            else if(time - openTillOvershootDuration < toOvershootDuration)//to overshoot scale
            {
                menuScale = Vector3.Lerp(new Vector3(1.0f, 1.0f, 1.0f), overshoot, Mathf.Sin((time - openTillOvershootDuration) / toOvershootDuration * Mathf.PI * 0.5f));
            }
            else if(time < totalDuration)//return from overshoot to normal scale
            {
                menuScale = Vector3.Lerp(new Vector3(1.0f, 1.0f, 1.0f), overshoot, Mathf.Cos((time - (openTillOvershootDuration + toOvershootDuration)) / toOvershootDuration * Mathf.PI * 0.5f));
            }
            thisTWPointer.transform.localScale = menuScale;
            time += Time.deltaTime;
            yield return null;
        }

        thisTWPointer.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        ActivateToolWheelEvent?.Invoke(this);
    }

    IEnumerator closeMenuAnim(float duration)
    {
        float time = 0.0f;
        Vector3 menuScale = new Vector3();

        while (time < duration)
        {
            menuScale = Vector3.Lerp(new Vector3(1.0f, 1.0f, 1.0f), new Vector3(), Mathf.Sin((time / duration) * Mathf.PI * 0.5f));
            thisTWPointer.transform.localScale = menuScale;
            time += Time.deltaTime;
            yield return null;
        }

        thisTWPointer.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        thisTWPointer.gameObject.SetActive(false);
    }
}
