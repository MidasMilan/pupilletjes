﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class IgnoreCollision : MonoBehaviour
{
    private Collider thisCollider;

    private void Start()
    {
        thisCollider.GetComponent<Collider>();
    }

    public void AddColliderToIgnore(Collider collider)
    {
        Physics.IgnoreCollision(GetComponent<Collider>(), collider, true);
    }
}
