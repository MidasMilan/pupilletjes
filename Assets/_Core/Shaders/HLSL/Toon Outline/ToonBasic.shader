// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Toon/Basic" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
		[Space10] [Toggle] _ContrastMode ("Contrast Mode", Float) = 0  
		[KeywordEnum(Green, Yellow, Red, Grey)] _ColourContrast("Colour Contrast Mode", Float) = 0
	}


	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass {
			Name "BASE"
			Cull Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			sampler2D _MainTex;
			samplerCUBE _ToonShade;
			float4 _MainTex_ST;
			float4 _ContrastColor;
			float4 _Color;
			uniform float _ContrastMode;
			float _ColourContrast;		

			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float3 cubenormal : TEXCOORD1;
				UNITY_FOG_COORDS(2)
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.cubenormal = mul (UNITY_MATRIX_MV, float4(v.normal,0));
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 returnValue;  

				if(_ContrastMode == 1)
                {
                   switch(_ColourContrast)
                   {
                       case 0:
                            _ContrastColor = float4(0, .75, 0, 1);
                        break;
                        case 1:
                            _ContrastColor = float4(1, 1, 0, 1);
                        break; 
                        case 2: 
                            _ContrastColor = float4(1.5, 0, 0, 1);
                        break;
                        case 3:
                            _ContrastColor = float4(0.1, 0.1, 0.1, 1);
                        break;
                   }                  

                   	fixed4 col = _ContrastColor;
					fixed4 cube = texCUBE(_ToonShade, i.cubenormal);
					fixed4 c = fixed4(1 * cube.rgb * col.rgb, col.a);
					UNITY_APPLY_FOG(i.fogCoord, c);
					return c;  

                } else 
				{
					fixed4 col = _Color * tex2D(_MainTex, i.texcoord);
					fixed4 cube = texCUBE(_ToonShade, i.cubenormal);
					fixed4 c = fixed4(2.0f * cube.rgb * col.rgb, col.a);
					UNITY_APPLY_FOG(i.fogCoord, c);
					return c;
				}				
			}
			ENDCG			
		}
	} 

	Fallback "VertexLit"
}
