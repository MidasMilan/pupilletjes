﻿Shader "Unlit/Contrast Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Space10] [Toggle] _ContrastMode ("Contrast Mode", Float) = 0  
        [KeywordEnum(Green, Yellow, Red, Grey)] _ColourContrast("Colour Contrast Mode", Float) = 0
        _ContrastTex ("Texture", 2D) = "white" {} 
        _OppacityBlend ("Oppacity Blend", Range(0.5, 2)) = 1           
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag          

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;              
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _ContrastTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _ContrastMode;
            float _ColourContrast;
            float _OppacityBlend;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);              
                return o;
            }

            void Unity_Blend_Overlay_float4(float4 Base, float4 Blend, float Opacity, out float4 Out)
            {
                float4 result1 = 1.0 - 2.0 * (1.0 - Base) * (1.0 - Blend);
                float4 result2 = 2.0 * Base * Blend;
                float4 zeroOrOne = step(Base, 0.5);
                Out = result2 * zeroOrOne + (1 - zeroOrOne) * result1;
                Out = lerp(Base, Out, Opacity);
            }

            fixed4 frag (v2f i) : SV_Target
            {           
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 contrastTexture = tex2D(_ContrastTex, i.uv);
                fixed4 returnValue;                       
                
                if(_ContrastMode == 1)
                {
                   switch(_ColourContrast)
                   {
                       case 0:
                            _Color = float4(0, .75, 0, 1);
                            break;
                        case 1:
                            _Color = float4(1, 1, 0, 1);
                            break; 
                        case 2: 
                            _Color = float4(1.5, 0, 0, 1);
                            break;
                        case 3:
                            _Color - float4(0, 0, 0, 1);
                            break;
                   }   

                    Unity_Blend_Overlay_float4(contrastTexture, _Color, _OppacityBlend,  returnValue);

                   return returnValue;           
                } 
                else {  return col; }
            }
            ENDCG
        }
    }
}
