﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class BlurFeature : ScriptableRendererFeature
{
    //All the variables that can be adjusted from outside of the shader
    [System.Serializable]
    public class BlurFeatureSettings
    {
        public RenderPassEvent Event = RenderPassEvent.BeforeRenderingTransparents;
        public Material blitMat = null;
        [Range(0, 8)] public int blurPasses = 0;
    }

    public BlurFeatureSettings settings = new BlurFeatureSettings();

    public class BlurFeaturePass : ScriptableRenderPass
    {
        //Custom variable declaration
        Material blitMat;
        int blurPasses;

        //You need this to render stuff
        ProfilingSampler profilingSampler; //Allows to see this pass in the frame debug menu
        RenderStateBlock renderStateBlock; //What to write to when rendering
        List<ShaderTagId> shaderTagIdList = new List<ShaderTagId>(); //specify which passes to call in shaders

        //Static references so they don't need to calculate every time frame draw gets executed
        static int cameraID = Shader.PropertyToID("_CameraColorTexture");
        static int tmpId1 = Shader.PropertyToID("tmpBlurRT1");
        static int tmpId2 = Shader.PropertyToID("tmpBlurRT2");

        public BlurFeaturePass(RenderPassEvent renderEvent, Material blitM, int blurP)
        {
            profilingSampler = new ProfilingSampler("Blur feature");
            renderPassEvent = renderEvent; //Assigning when to execute
            blitMat = blitM;
            blurPasses = blitMat.GetInt("_BlurPasses");
            //blitMat.SetInt("_BlurPasses", blurPasses);

            shaderTagIdList.Add(new ShaderTagId("UniversalForward"));
            shaderTagIdList.Add(new ShaderTagId("LightweightForward"));
            shaderTagIdList.Add(new ShaderTagId("SRPDefaultUnlit"));

            renderStateBlock = new RenderStateBlock(RenderStateMask.Nothing);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            blurPasses = blitMat.GetInt("_BlurPasses");
            if (blurPasses == 0) return;

            SortingCriteria sortingCriteria = SortingCriteria.CommonTransparent;

            DrawingSettings drawingSettings = CreateDrawingSettings(shaderTagIdList, ref renderingData, sortingCriteria);
            ref CameraData cameraData = ref renderingData.cameraData;
            Camera camera = cameraData.camera;
            CommandBuffer cmd = CommandBufferPool.Get("BasicFeature");
            using (new ProfilingScope(cmd, profilingSampler))
            {
                //Get the final render from previous render feature
                cmd.GetTemporaryRT(cameraID, camera.pixelWidth, camera.pixelHeight, 32, FilterMode.Bilinear, RenderTextureFormat.Default);

                //get ready for the blurry effect
                cmd.GetTemporaryRT(tmpId2, renderingData.cameraData.cameraTargetDescriptor, FilterMode.Bilinear);
                cmd.GetTemporaryRT(tmpId1, renderingData.cameraData.cameraTargetDescriptor, FilterMode.Bilinear);
                RenderTargetIdentifier cameraIdentifier = new RenderTargetIdentifier(cameraID);
                RenderTargetIdentifier tempTex1 = new RenderTargetIdentifier(tmpId1);
                RenderTargetIdentifier tempTex2 = new RenderTargetIdentifier(tmpId2);

                //Go do the blurry stuff! 
                //cmd.SetGlobalFloat("_Offset", 1.5f);
                cmd.Blit(cameraIdentifier, tempTex1, blitMat);
                for (var i = 0; i < blurPasses; i++)
                {
                    //cmd.SetGlobalFloat("_Offset", 0.5f + i);
                    cmd.Blit(tempTex1, tempTex2, blitMat);

                    // pingpong
                    var rttmp = tempTex1;
                    tempTex1 = tempTex2;
                    tempTex2 = rttmp;
                }
                //cmd.SetGlobalFloat("_Offset", 0.5f + blurPasses - 1f);
                cmd.SetRenderTarget(cameraID, RenderBufferLoadAction.DontCare, RenderBufferStoreAction.Store);
                cmd.Blit(tempTex1, BuiltinRenderTextureType.CurrentActive, blitMat);
                //cmd.SetGlobalTexture("_MainTex", tempTex1);

                cmd.ReleaseTemporaryRT(tmpId1);
                cmd.ReleaseTemporaryRT(tmpId2);

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }
            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }
    }

    BlurFeaturePass pass;

    public override void Create()
    {
        pass = new BlurFeaturePass(settings.Event, settings.blitMat, settings.blurPasses);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        renderer.EnqueuePass(pass);
    }
}
