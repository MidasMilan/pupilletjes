﻿Shader "Custom/BasicBlurEffectShader"
{
    Properties
    {
        [HideInInspector] _MainTex("Blur Texture", 2D) = "white" {}
        [HideInInspector] _BlurPasses("blur passes", int) = 0
        _Offset("offset", Float) = 0.5
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        Pass
        {
            ZWrite On

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            half4 _MainTex_ST;
            int _BlurPasses;
            float _Offset;

            fixed4 frag(v2f i) : SV_Target
            {
                float2 res = _MainTex_TexelSize.xy;
                fixed4 col;
                fixed4 myTex = tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(i.uv, _MainTex_ST));
                float iOffset = _Offset;

                col = myTex;

                col.rgb = myTex.rgb;

                col.rgb += tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(i.uv, _MainTex_ST) + float2(iOffset, iOffset) * res ).rgb;
                col.rgb += tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(i.uv, _MainTex_ST) + float2(iOffset, -iOffset) * res ).rgb;
                col.rgb += tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(i.uv, _MainTex_ST) + float2(-iOffset, iOffset) * res ).rgb;
                col.rgb += tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(i.uv, _MainTex_ST) + float2(-iOffset, -iOffset) * res ).rgb;

                col.rgb /= 5.0f;

                return col;
            }
            ENDCG
        }
    }
}
