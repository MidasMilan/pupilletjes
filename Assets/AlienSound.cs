﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Alien))]
public class AlienSound : MonoBehaviour
{

    [Header("Random Pitch")]
    [Tooltip("Use min and max random pitch levels when playing sounds.")]
    [SerializeField] private bool _UseRandomPitch = true;
    [Tooltip("Minimum pitch that will be used when randomly set.")]
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float _PitchMin = 0.75f;
    [Tooltip("Maximum pitch that will be used when randomly set.")]
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float _PitchMax = 1.25f;

    [Header("Volume")]
    [SerializeField] private bool useRandomVolume = true;
    [Tooltip("Minimum volume that will be used when randomly set.")]
    [Range(0.0f, 1.0f)]
    [SerializeField] private float _VolMin = 0.75f;
    [Tooltip("Maximum volume that will be used when randomly set.")]
    [Range(0.0f, 1.0f)]
    [SerializeField] private float _VolMax = 1.0f;

    [SerializeField] private AudioClip[] _InWaterAudioClips;
    [SerializeField] private AudioClip[] _OnHookedAudioClips;
    [SerializeField] private AudioClip[] _OnTableAudioClips;
    [SerializeField] private AudioClip[] _WalkingAudioClips;
    [SerializeField] private AudioClip[] _BuildingAudioClips;
    [SerializeField] private AudioClip[] _PartyingAudioClips;


    public enum SoundType
    {
        InWater,
        OnHooked,
        OnTable,
        Walking,
        Building,
        Partying
    }

    public void Play(AudioSource _AudioSource, SoundType _SoundType)
    {

        switch (_SoundType)
        {
            case SoundType.InWater:
                PlayRandomAudioClip(_AudioSource, _InWaterAudioClips);
                break;
            case SoundType.OnHooked:
                PlayRandomAudioClip(_AudioSource, _OnHookedAudioClips);
                break;
            case SoundType.OnTable:
                PlayRandomAudioClip(_AudioSource, _OnTableAudioClips);
                break;
            case SoundType.Walking:
                PlayRandomAudioClip(_AudioSource, _WalkingAudioClips);
                break;
            case SoundType.Building:
                PlayRandomAudioClip(_AudioSource, _BuildingAudioClips);
                break;
            case SoundType.Partying:
                PlayRandomAudioClip(_AudioSource, _PartyingAudioClips);
                break;
            default:
                break;
        }
    }

    private void PlayRandomAudioClip(AudioSource _AudioSource, AudioClip[] _AudioClips)
    {
        if (this._UseRandomPitch)
        {
            // Randomly apply a pitch between the pitch min max
            _AudioSource.pitch = Random.Range(this._PitchMin, this._PitchMax);
        }

        if (this.useRandomVolume)
        {
            //randomly apply a volume between the volume min max
            _AudioSource.volume = Random.Range(this._VolMin, this._VolMax);
        }

        if (_AudioClips.Length > 0)
        {
            // Play a random sound
            _AudioSource.PlayOneShot(_AudioClips[Random.Range(0, _AudioClips.Length)]);
        }
    }
}
